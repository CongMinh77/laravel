function deleteOrder(id) {
    const swalWithBootstrapButtons = Swal.mixin({
        customClass: {
            confirmButton: 'btn btn-success',
            cancelButton: 'btn btn-danger mr-3'
        },
        buttonsStyling: false
    })
    swalWithBootstrapButtons.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, delete it!',
        cancelButtonText: 'No, cancel!',
        reverseButtons: true,
    }).then((result) => {
        if (result.isConfirmed) {
            event.preventDefault();
            document.getElementById('form-' + id).submit();
            swalWithBootstrapButtons.fire(
                'Deleted!',
                'Your file has been deleted.',
                'success',
            )
        } else if (
            result.dismiss === Swal.DismissReason.cancel
        ) {
            swalWithBootstrapButtons.fire(
                'Cancelled',
                'Your order file is safe :)',
                'error'
            )
        }
    })
}

$(document).on('change', '#select-status', function (e){
    e.preventDefault();
    let id = $(this).data('order');
    let url = $(this).data('action');
    let status = this.value;
    let data = {
        id: id,
        status: status
    }
    User.callApi(url, data, METHOD_PUT)
        .done(function (res) {
            $.toast({
                heading: 'Notification',
                text: 'Update status success',
                position: 'top-right',
                icon: 'success',
            })
        })
        .fail(function (res) {
            $.each(res.responseJSON.errors, function () {
                console.log(res.responseJSON.errors)
            })
        })
})
