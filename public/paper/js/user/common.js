const METHOD_GET = "GET";
const METHOD_POST = "POST";
const METHOD_PUT = "PUT";
const METHOD_DELETE = "DELETE";
const METHOD_PATCH = "PATCH";
const btnToTop = $("#scrollToTop");

const User = (function () {
    let modules = {};

    modules.showMessage = function (res)
    {
        $('#success_message').show().addClass('alert alert-success').text(res.message).fadeOut(5000);
    }

    modules.callApi = function (url, data = {}, method = METHOD_GET)
    {
        return $.ajax({
            url: url,
            data: data,
            type: method,
        })
    }

    return modules;
}(window.jQuery, window, document));

btnToTop.hide();

$(window).scroll(function () {
    $(this).scrollTop() > 200 ? btnToTop.fadeIn() : btnToTop.fadeOut();
});

btnToTop.on('click', function () {
    $("body,html").animate({
        scrollTop: 0
    }, 1000);

    return false;
});

$(document).ready(function () {
    if (userId !== 0) {
        User.callApi(urlGetWishList)
            .done(function (res) {
                if (res.success.length > 0) {
                    $("#amount-wish-list").append(JSON.stringify(res.success.length));
                }
            })
            .fail(function (res) {
                $.each(res.responseJSON.errors, function () {
                    console.log(res.responseJSON.errors)
                })
            });
    }
    // User.callApi(urlGetCart)
    //     .done( function (res) {
    //         console.log(res.data)
    //         $("#amount-cart").append(JSON.stringify(res.data.cart));
    //     });
})
