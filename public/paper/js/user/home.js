$(document).ready(function(){
    $('.slide-product-hot').slick({
        centerMode: true,
        centerPadding: '60px',
        slidesToShow: 3,
        slidesToScroll: 1,
        prevArrow: '<div class="product-prev"><i class="fa fa-angle-left fa-lg text-dark"></i></div>',
        nextArrow: '<div class="product-next"><i class="fa fa-angle-right fa-lg text-dark"></i></div>',
        autoplay: true,
        autoplaySpeed: 5000,
        responsive: [
            {
                breakpoint: 768,
                settings: {
                    arrows: false,
                    centerMode: true,
                    centerPadding: '40px',
                    slidesToShow: 1
                }
            },
            {
                breakpoint: 480,
                settings: {
                    arrows: false,
                    centerMode: true,
                    centerPadding: '40px',
                    slidesToShow: 1
                }
            }
        ]
    });

    $(document).on('click', '#btn-like-product', function (e) {
        e.preventDefault();
        $('#error').show();
        let id = $(this).data('product');
        let url = $(this).data('action');

        let data = {
            id: id,
        };
        let btnWishlist = $(`[data-product=${id}]`);

        User.callApi(url, data, METHOD_POST)
            .done(function (res) {
                if (btnWishlist.data('like') === 1) {
                    btnWishlist.data('like', 0).removeClass('btn-danger')
                        .addClass('btn-light').children().addClass('icon');
                }
                else {
                    btnWishlist.data('like', 1).removeClass('btn-light')
                        .addClass('btn-danger').children().removeClass('icon');
                }
                User.callApi(urlGetWishList)
                    .done(function (res) {
                        $("#amount-wish-list").html(JSON.stringify(res.success.length));
                    })
                    .fail(function (res) {
                        console.log(res)
                    });
                $('#like-product').load(`/wishlist/listProduct`);
            })
            .fail(function (res) {
                $.each(res.responseJSON.errors, function () {
                    console.log(res.responseJSON.errors)
                })
            })
    });

    $(document).on('click', '#btn-register-email', function (e) {
        e.preventDefault();
        $('#error').show();
        let email = $('#email-register').val();
        let status = 1;
        let url = $(this).data('action');

        let data = {
            email: email,
            status: status,
        };
        if (validateEmail(email)) {
            User.callApi(url, data, METHOD_POST)
                .done(function (res) {
                    $('#email-register').val('');
                    $.toast({
                        heading: 'Notification',
                        text: 'Register success!',
                        position: 'top-right',
                        icon: 'success',
                    })
                })
                .fail(function (res) {
                    $.each(res.responseJSON.errors, function () {
                        console.log(res.responseJSON.errors)
                    })
                })
        }
        else {
            $.toast({
                heading: 'Notification',
                text: 'Wrong format email',
                position: 'top-right',
                icon: 'error',
            })
        }
    });

    $(document).on('click', '#btn-add-cart', function (e) {
        e.preventDefault();
        $(this).siblings('loader');
        let id = $(this).data('product-cart');
        let type = $(this).data('type');
        let user_id = userId;
        let url = $(this).data('action');

        let data = {
            product_id: id,
            user_id: user_id,
            type: type,
        };

        // User.callApi(url, data, METHOD_POST)
        //     .done(function (res) {
        //         User.callApi(urlGetWishList)
        //             .done(function (res) {
        //                 $("#amount-cart").html(JSON.stringify(res.data.cart));
        //             });
        //     })
        //     .fail(function (res) {
        //         $.each(res.responseJSON.errors, function () {
        //             console.log(res.responseJSON.errors)
        //         })
        //     })
    });

    $(document).on('click', '#btn-category', function (){
        let url = $(this).data('action');
        let urlAfterJoin = url.split(' ').join('%20');
        $('.element-nav-product').removeClass('active');
        $(this).parent().addClass('active');
        $('#category-list').load(`${urlAfterJoin}`);
    });
});

const validateEmail = (email) => {
    return String(email)
        .toLowerCase()
        .match(
            /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
        );
};

