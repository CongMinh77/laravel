const Cart = (function () {
    let modules = {};

    modules.success = function (text = ''){
        $.toast({
            heading: 'Notification',
            text: text,
            position: 'top-right',
            icon: 'success',
        })
    }

    modules.deleteCart = function (url, data = {}, method = METHOD_DELETE)
    {
        User.callApi(url, data, METHOD_DELETE)
            .done(function (res) {
                if (userId !== 0) {
                    $('#table-cart').load(`/cart/list`);
                }
                else {
                    location.reload();
                }
            })
            .fail(function (res) {
                $.each(res.responseJSON.errors, function () {
                    console.log(res.responseJSON.errors)
                })
            })
    }

    return modules;
}(window.jQuery, window, document));

$(document).ready(function (){
    $(document).on('change', '#quantity-product', function (e){
        e.preventDefault();
        let id = $(this).data('product-cart');
        let url = $(this).data('action');
        let urlDelete = $(this).data('action-delete');
        let quantity = $(this).val();
        let data = {
            id: id,
            quantity: quantity,
        };

        User.callApi(url, data, METHOD_PATCH)
            .done(function (res) {
                if (res.remove) {
                    $(`#row-${id}`).remove();
                    Cart.success('Remove success!');
                    Cart.deleteCart(urlDelete, {id: id})
                }
                else {
                    Cart.success('Add to cart success!')
                    location.reload();
                }
                // let amountCartOld = $('#amount-cart').text();
                // let quantityProduct = parseInt(amountCartOld) + parseInt(res.data.quantity);
                // $("#amount-cart").html(JSON.stringify(quantityProduct));
                // console.log(parseInt(a), quantity);
            })
            .fail(function (res) {
                $.each(res.responseJSON.errors, function () {
                    console.log(res.responseJSON.errors)
                })
            })
    })
})

$(document).on('click', '#btn-remove-from-cart', function (e) {
    e.preventDefault();
    let id = $(this).data('product-cart');
    let url = $(this).data('action');
    let data = {
        id: id,
    };
    Cart.deleteCart(url, data)
})
