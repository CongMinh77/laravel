const dashboard = (function () {
    let modules = {};
    const color = [
        '#F23041',
        '#F59201',
        '#8EC441',
        '#94E7EF',
        '#D5D731',
        '#558ED9',
        '#A44cd3',
        '#1E0478',
        '#011C26',
        '#6F7CCD',
        '#BF5517',
        '#F2DC9B',
        '#ffc606'
    ];

    /**
     * Setup data LineChart
     */
    // modules.dataSetLineChartCategory = function () {
    //     let dataSetLine = [];
    //     for (let i = 0; i < allCategory.length; i++) {
    //         dataSetLine.push({
    //             label: allCategory[i],
    //             backgroundColor: color[i],
    //             borderColor: color[i],
    //             data: dataLineAllCategory[i],
    //         });
    //     }
    //     dataSetLine.push({
    //         label: 'Trang chủ',
    //         backgroundColor: color[color.length - 1],
    //         borderColor: color[color.length - 1],
    //         data: dataHomePage,
    //     });
    //     return dataSetLine;
    // };

    modules.dataSetLineChartTotal = function () {
        let dataSetLine = [];
        dataSetLine.push({
            label: 'Order',
            backgroundColor: color[3],
            borderColor: color[3],
            data: [3,2,1,4,8,6,5,12,15,12,18,22],
        });
        return dataSetLine;
    };

    const labels = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    modules.dataLineChart = (dataSetLineChart) => ({
        labels: labels,
        datasets: dataSetLineChart,
    });

    modules.configLineChart = (dataLineChart) => ({
        type: 'line',
        data: dataLineChart,
        options: {
            responsive: true,
            interaction: {
                mode: 'index',
                intersect: false,
            },
            stacked: false,
            plugins: {
                title: {
                    display: false,
                    text: 'Chart.js Line Chart - Multi Axis'
                },
            },
            scales: {
                y: {
                    type: 'linear',
                    display: true,
                    position: 'left',
                    ticks: {
                        min: 0,
                        beginAtZero: true,
                        stepSize: 0.5
                    }
                },
            }
        },
    });

    /**
     * Setup data PieChart
     */
    modules.labelPieChart = function () {
        return [
            'User',
            'Guest',
        ]
    };

    modules.dataPieChart = () => ({
        labels: dashboard.labelPieChart(),
        datasets: [
            {
                data: [userViews, guestViews],
                backgroundColor: [
                    color[0],
                    color[3]
                ],
                hoverOffset: 2
            }
        ]
    });

    modules.configPieChart = (dataPieChart) => ({
        type: 'pie',
        data: dataPieChart,
        options: {
            plugins: {
                legend: {
                    display: true
                },
                title: {
                    display: true,
                    text: 'Tổng lượt truy cập'
                },
            }
        },
    });

    /**
     * Render Chart to canvas by ID
     */
    modules.renderChart = function (chartContainerNameTag = '', configChart) {
        return new Chart(
            document.getElementById(chartContainerNameTag),
            configChart
        );
    };

    return modules;
}(window.jQuery, window, document));

$(document).ready(function () {
    // dashboard.renderChart(
    //     'pie-chart',
    //     dashboard.configPieChart(dashboard.dataPieChart())
    // );
    dashboard.renderChart(
        'line-chart-total',
        dashboard.configLineChart(dashboard.dataLineChart(dashboard.dataSetLineChartTotal()))
    );
    // dashboard.renderChart(
    //     'line-chart-category',
    //     dashboard.configLineChart(dashboard.dataLineChart(dashboard.dataSetLineChartCategory()))
    // );
});

// $(function () {
//     var start = moment();
//     var end = moment();
//     if (startDate != null && endDate != null) {
//         start = moment(startDate);
//         end = moment(endDate);
//     }
//
//
//     function cb(start, end) {
//         $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
//     }
//     $('#reportrange').daterangepicker({
//         startDate: start,
//         endDate: end,
//         maxDate: moment(),
//         "maxSpan": {
//             "days": 30
//         },
//         ranges: {
//             'Today': [moment(), moment()],
//             'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
//             'Last 7 Days': [moment().subtract(6, 'days'), moment()],
//             'Last 30 Days': [moment().subtract(29, 'days'), moment()],
//             'This Month': [moment().startOf('month'), moment().endOf('month')],
//             'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1,
//                 'month').endOf('month')]
//         }
//     }, cb);
//     cb(start, end);
//
//     $('#reportrange').on('apply.daterangepicker', function (ev, picker) {
//         $('#start-date').val(picker.startDate.format('YYYY/MM/DD'));
//         $('#end-date').val(picker.endDate.format('YYYY/MM/DD'));
//     });
// });
