const METHOD_GET = "GET";
const METHOD_POST = "POST";
const METHOD_PUT = "PUT";
const METHOD_DELETE = "DELETE";

$(document).ready(function () {
    $('#notification').fadeOut(3000, "linear");
    $('div.alert').fadeOut(3000, "linear");
});

demo = {
    checkFullPageBackgroundImage: function() {
        $page = $('.full-page');
        image_src = $page.data('image');

        if (image_src !== undefined) {
          image_container = '<div class="full-page-background" style="background-image: url(' + image_src + ') "/>';
          $page.append(image_container);
        }
    },

    showNotification: function(from, align) {
        color = 'primary';

        $.notify({
          icon: "nc-icon nc-bell-55",
          message: "Welcome to <b>Paper Dashboard</b> - a beautiful bootstrap dashboard for every web developer."

        }, {
          type: color,
          timer: 8000,
          placement: {
            from: from,
            align: align
          }
        });
    },
}

const Admin = (function () {
    let modules = {};

    modules.showMessage = function (res)
    {
        $('#success_message').show().addClass('alert alert-success').text(res.message).fadeOut(5000);
    }

    modules.callApi = function (url, data = {}, method = METHOD_GET)
    {
        return $.ajax({
            url: url,
            data: data,
            type: method,
        })
    }

    return modules;
}(window.jQuery, window, document));
