const CREATE_CATEGORY_MODAL = $('#CreateCategory');
const EDIT_CATEGORY_MODAL = $('#EditCategory');

const Category = (function () {
    let modules = {};

    modules.showMessage = function (res)
    {
        $('#success_message').show().addClass('alert alert-success').text(res.message).fadeOut(5000);
    }

    modules.fillData = function (tag, value = '')
    {
        $(tag).val(value);
    }

    modules.showError = function (name, type = '', message)
    {
        $('span.' + name + '_error_' + type).text(message[0]);
    }

    modules.callCategoryApi = function (url, data = {}, method = METHOD_GET)
    {
        return $.ajax({
            url: url,
            data: data,
            type: method,
        })
    }

    modules.getListBy = function (url)
    {
        let name = $('#name').val();
        let parentId = $('#parent_id').val();
        let search = $('#search').val();
        let data = {
            name: name,
            parent_id: parentId,
            search: search,
        }

        Category.callCategoryApi(url, data)
            .then(function (res) {
                $('#list').replaceWith(res);
            })
    }

    modules.getList = function ()
    {
        let url = $('#list').data('action');
        Category.getListBy(url);
    }

    return modules;
}(window.jQuery, window, document));

$(document).ready(function () {
    Category.getList();
});

$(document).on('click', '.page-link', function (e) {
    e.preventDefault();
    let url = $(this).attr('href');
    Category.getListBy(url);
})

$(document).on('click', '#btn-create', function (e) {
    e.preventDefault();
    $('#error').show();
    let url = $(this).data('action');
    let data = {
        name: $('#nameCategory').val(),
        parent_id: $('#parentIdCategory').val(),
    };

    Category.callCategoryApi(url, data, METHOD_POST)
        .done(function (res) {
            Category.getList();
            Category.showMessage(res);
            CREATE_CATEGORY_MODAL.modal('hide').find('input').val("");
        })
        .fail(function (res) {
            $.each(res.responseJSON.errors, function (k, v) {
                Category.showError(k, 'create' , v);
                CREATE_CATEGORY_MODAL.on('mousemove', function (){
                    $('#error').fadeOut(3000);
                })
            })
        })
})

let urlUpdate = '';

$(document).on('click', '#btn-edit', function (e) {
    e.preventDefault();
    let url = $(this).data('action');
    urlUpdate = $(this).data('update');
    let data = {};

    Category.callCategoryApi(url, data, METHOD_GET)
        .then(function (res) {
            Category.fillData('#idUpdate', res.category.id);
            Category.fillData('#nameUpdate', res.category.name);
            Category.fillData('#parentIdUpdate', res.category.parent_id);
        })
})

$(document).on('click', '#btn-update', function (e) {
    e.preventDefault();
    $('#error').show();
    let data = {
        name: $('#nameUpdate').val(),
        parent_id: $('#parentIdUpdate').val(),
    };

    Category.callCategoryApi(urlUpdate, data, METHOD_PUT)
        .done(function (res) {
            Category.getList();
            Category.showMessage(res);
            EDIT_CATEGORY_MODAL.modal('hide').find('input').val("");
        })
        .fail(function (res) {
            $.each(res.responseJSON.errors, function (k, v) {
                Category.showError(k, 'update', v);
                EDIT_CATEGORY_MODAL.on('mousemove', function (){
                    $('#error').fadeOut(3000);
                })
            })
        })
})

$(document).on('click', '#btn-show', function (e) {
    e.preventDefault();
    let url = $(this).data('action');
    let data = {};

    Category.callCategoryApi(url, data, METHOD_GET)
        .then(function (res) {
            Category.fillData('#idShow', res.category.id);
            $('#nameShow').text(res.category.name);
            Category.fillData('#parentIdShow', res.category.parent_id);
        })
})

$(document).on('click', '.btn-delete-category', function (e){
    e.preventDefault();
    const swalWithBootstrapButtons = Swal.mixin({
        customClass: {
            confirmButton: 'btn btn-success',
            cancelButton: 'btn btn-danger mr-3'
        },
        buttonsStyling: false
    })
    swalWithBootstrapButtons.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, delete it!',
        cancelButtonText: 'No, cancel!',
        reverseButtons: true,
    }).then((result) => {
        if (result.isConfirmed) {
            let url = $(this).data('action');
            let data = {};
            console.log(url)
            Category.callCategoryApi(url, data, METHOD_DELETE)
                .then(function () {
                    Category.getList();
                })
            Swal.fire(
                'Deleted!',
                'Your category has been deleted.',
                'success'
            )
        }
        else if (result.dismiss === Swal.DismissReason.cancel)
        {
            swalWithBootstrapButtons.fire(
                'Cancelled',
                'Your category file is safe :)',
                'error'
            )
        }
    })
})

$('#search').on('keyup', function () {
    Category.getList();
})
