<?php

namespace Tests\Feature\Categories;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class CreateCategoryTest extends TestCase
{
    public function getStoreRoute()
    {
        return route('categories.store');
    }

    /** @test */
    public function authenticated_and_user_has_permission_user_can_create_new_category()
    {
        $this->loginAsUserWithPermission('category_create');
        $createData = [
            'name' => $this->faker->unique->word(),
            'parent_id' => 1,
        ];
        $response = $this->postJson($this->getStoreRoute(), $createData);
        $response->assertStatus(Response::HTTP_OK);
        $response->assertJson(fn(AssertableJson $json) =>
            $json->where('status', Response::HTTP_OK)
                ->where('message', 'Create category successfully!')
                ->etc()
        );
    }

    /** @test */
    public function authenticated_super_admin_can_create_new_category()
    {
        $this->loginAsSuperAdmin();
        $createData = [
            'name' => $this->faker->unique->word(),
            'parent_id' => 1,
        ];
        $response = $this->postJson($this->getStoreRoute(), $createData);
        $response->assertStatus(Response::HTTP_OK);
        $response->assertJson(fn(AssertableJson $json) =>
            $json->where('status', Response::HTTP_OK)
                ->where('message', 'Create category successfully!')
                ->etc()
        );
    }

    /** @test */
    public function unauthenticated_user_can_not_create_new_category()
    {
        $createData = [
            'name' => $this->faker->unique->word(),
            'parent_id' => 1,
        ];
        $response = $this->postJson($this->getStoreRoute(), $createData);
        $response->assertStatus(Response::HTTP_UNAUTHORIZED);
    }

    /** @test */
    public function user_can_not_create_new_user_if_name_field_is_null()
    {
        $this->loginAsUserWithPermission('category_create');
        $createData = [
            'parent_id' => 1,
        ];
        $response = $this->postJson($this->getStoreRoute(), $createData);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    /** @test */
    public function super_admin_can_not_create_new_user_if_name_field_is_null()
    {
        $this->loginAsSuperAdmin();
        $createData = [
            'parent_id' => 1,
        ];
        $response = $this->postJson($this->getStoreRoute(), $createData);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }
}
