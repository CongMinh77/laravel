<?php

namespace Tests\Feature\Categories;

use App\Models\Category;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class DeleteCategoryTest extends TestCase
{
    public function getDeleteRoute($id)
    {
        return route('categories.destroy', $id);
    }
    /** @test */
    public function authenticated_and_user_has_permission_user_can_delete_category()
    {
        $this->loginAsUserWithPermission('category_delete');
        $category = Category::factory()->create();
        $response = $this->json('DELETE', $this->getDeleteRoute($category->id));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertJson(fn(AssertableJson $json) =>
                $json->where('status', Response::HTTP_OK)
                ->where('message', 'Delete category successfully!')
                ->etc()
            );
    }

    /** @test */
    public function authenticated_super_admin_can_delete_category()
    {
        $category = Category::factory()->create();
        $this->loginAsUserWithPermission('category_delete');
        $response = $this->json('DELETE', $this->getDeleteRoute($category->id));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertJson(fn(AssertableJson $json) =>
                $json->where('status', Response::HTTP_OK)
                ->where('message', 'Delete category successfully!')
                ->etc()
            );
    }

    /** @test */
    public function unauthenticated_user_can_not_delete_category()
    {
        $category = Category::factory()->create();
        $response = $this->json('DELETE', $this->getDeleteRoute($category->id));
        $response->assertStatus(Response::HTTP_UNAUTHORIZED);
    }

    /** @test */
    public function unauthenticated_super_admin_can_not_delete_category()
    {
        $category = Category::factory()->create();
        $response = $this->json('DELETE', $this->getDeleteRoute($category->id));
        $response->assertStatus(Response::HTTP_UNAUTHORIZED);
    }
}
