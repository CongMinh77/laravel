<?php

namespace Tests\Feature\Categories;

use App\Models\Category;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class UpdateCategoryTest extends TestCase
{
    public function getUpdateRoute($id)
    {
        return route('categories.update', $id);
    }

    /** @test */
    public function authenticated_user_has_permission_can_update_category()
    {
        $this->withoutExceptionHandling();
        $category = Category::factory()->create();
        $this->loginAsUserWithPermission('category_edit');
        $updateData = [
            'name' => $this->faker->unique->word(),
            'parent_id' => '1',
        ];
        $response = $this->json('PUT', $this->getUpdateRoute($category->id), $updateData);
        $response->assertStatus(Response::HTTP_OK);
        $response
            ->assertJson(fn(AssertableJson $json) =>
                $json->where('status', Response::HTTP_OK)
                    ->where('message', 'Update category successful!')
                    ->etc()
            );
    }

    /** @test */
    public function authenticated_super_admin_can_update_category()
    {
        $this->loginAsSuperAdmin();
        $category = Category::factory()->create();
        $updateData = [
            'name' => $this->faker->unique->word(),
            'parent_id' => '1',
        ];
        $response = $this->json('PUT', $this->getUpdateRoute($category->id), $updateData);
        $response->assertStatus(Response::HTTP_OK);
        $response
            ->assertJson(fn(AssertableJson $json) =>
                $json->where('status', Response::HTTP_OK)
                    ->where('message', 'Update category successful!')
                    ->etc()
            );
    }

    /** @test */
    public function unauthenticated_user_can_not_update_category()
    {
        $category = Category::factory()->create();
        $updateData = [
            'name' => $this->faker->unique->word(),
            'parent_id' => '1',
        ];
        $response = $this->json('PUT', $this->getUpdateRoute($category->id), $updateData);
        $response->assertStatus(Response::HTTP_UNAUTHORIZED);
    }

    /** @test */
    public function unauthenticated_super_admin_can_not_update_category()
    {
        $category = Category::factory()->create();
        $updateData = [
            'name' => $this->faker->unique->word(),
            'parent_id' => '1',
        ];
        $response = $this->json('PUT', $this->getUpdateRoute($category->id), $updateData);
        $response->assertStatus(Response::HTTP_UNAUTHORIZED);
    }

    /** @test */
    public function user_has_permission_can_not_update_user_account_if_name_is_null()
    {
        $this->loginAsUserWithPermission('category_edit');
        $category = Category::factory()->create();
        $updateData = [
            'parent_id' => '1',
        ];
        $response = $this->json('PUT', $this->getUpdateRoute($category->id), $updateData);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    /** @test */
    public function super_admin_can_not_update_user_account_if_name_is_null()
    {
        $this->loginAsSuperAdmin();
        $category = Category::factory()->create();
        $updateData = [
            'parent_id' => '1',
        ];
        $response = $this->json('PUT', $this->getUpdateRoute($category->id), $updateData);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }
}
