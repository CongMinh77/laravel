<?php

namespace Tests\Feature\Users;

use App\Models\User;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;
use Tests\TestCase;

class CreateUserTest extends TestCase
{
    public function getStoreRoute()
    {
        return route('users.store');
    }
    public function getCreateRoute()
    {
        return route('users.create');
    }

    /** @test */
    public function super_admin_can_not_create_new_user_if_name_or_email_or_password_or_address_are_null()
    {
        $this->loginAsSuperAdmin();
        $user = User::factory([
            'name' => null,
            'email' => null,
            'password' => null,
            'address' => null,
        ])->make()->toArray();
        $response = $this->post($this->getStoreRoute(), $user);
        $response->assertSessionHasErrors(['name', 'email', 'password', 'address']);
    }

    /** @test */
    public function authenticated_user_without_permission_can_not_see_create_user_form()
    {
        $this->loginAsUser();
        $response = $this->get($this->getCreateRoute());
        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

    /** @test */
    public function authenticated_user_has_permission_can_see_create_user_form()
    {
        $this->loginAsUserWithPermission('user_create');
        $response = $this->get($this->getCreateRoute());
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('users.create');
        $response->assertSee('name')->assertSee('email');
    }

    /** @test */
    public function unauthenticated_user_can_not_create_new_user()
    {
        $user = User::factory()->make();
        $response = $this->post($this->getStoreRoute(), $user->toArray());
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');
    }

    /** @test */
    public function unauthenticated_user_can_not_see_create_new_user_form()
    {
        $response = $this->get($this->getCreateRoute());
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');
    }
}
