<?php

namespace Tests\Feature\Products;

use App\Models\Category;
use App\Models\Product;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class UpdateProductTest extends TestCase
{
    public function getEditRoute($id)
    {
        return route('products.edit', $id);
    }

    public function getUpdateRoute($id)
    {
        return route('products.update', $id);
    }

    public function getProductRoute()
    {
        return route('products.index');
    }

    public function setDataCreate($data = [])
    {
        return array_merge([
            'name' => $this->faker->name,
            'price' => '10000',
            'description' => $this->faker->text,
            'category_id' => Category::factory()->create()->id,
        ], $data);
    }

    /** @test */
    public function authenticated_super_admin_can_see_edit_product_form()
    {
        $this->loginAsSuperAdmin();
        $product = Product::factory()->create();
        $response = $this->get($this->getEditRoute($product->id));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('products.edit');
        $response->assertSee($product->name, $product->price);
    }

    /** @test */
    public function unauthenticated_user_can_not_see_edit_product_view()
    {
        $product = Product::factory()->create();
        $response = $this->get($this->getEditRoute($product->id));
        $response->assertRedirect('/login');
        $response->assertStatus(Response::HTTP_FOUND);
    }

    /** @test */
    public function authenticated_super_admin_can_update_product()
    {
        $this->loginAsSuperAdmin();
        $product = Product::factory()->create();
        $data = $this->setDataCreate();
        $response = $this->put($this->getUpdateRoute($product->id), $data);
        $this->assertDatabaseHas('products', ['name' => $data['name']]);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect($this->getProductRoute());
    }

    /** @test */
    public function authenticated_super_admin_can_not_update_product_name_is_null()
    {
        $this->loginAsSuperAdmin();
        $product = Product::factory()->create();
        $data = [
            'name' => null,
            'price' => null
        ];
        $response = $this->put($this->getUpdateRoute($product->id), $data);
        $response->assertSessionHasErrors(['name', 'price']);
    }

    /** @test */
    public function authenticated_user_has_permission_can_see_edit_product_form()
    {
        $this->loginAsUserWithPermission('product_edit');
        $product = Product::factory()->create();
        $response = $this->get($this->getEditRoute($product->id));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('products.edit');
        $response->assertSee(['name', 'price']);
    }

    /** @test */
    public function authenticated_user_has_permission_can_not_update_name_if_price_are_null()
    {
        $this->loginAsUserWithPermission('product_edit');
        $product = Product::factory()->create();
        $data = [
            'name' => null,
            'price' => null
        ];
        $response = $this->put($this->getUpdateRoute($product->id), $data);
        $response->assertSessionHasErrors(['name', 'price']);
        $this->assertDatabaseMissing('products', $data);
    }
}
