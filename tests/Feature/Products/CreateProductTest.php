<?php

namespace Tests\Feature\Products;

use App\Models\Category;
use App\Models\Product;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class CreateProductTest extends TestCase
{
    public function getCreateRoute()
    {
        return route('products.create');
    }

    public function getStoreRoute()
    {
        return route('products.store');
    }

    public function getProductRoute()
    {
        return route('products.index');
    }

    public function setDataCreate($data = [])
    {
        return array_merge([
            'name' => $this->faker->name,
            'price' => '10000',
            'description' => $this->faker->text,
            'category_id' => Category::factory()->create()->id,
        ], $data);
    }

    /** @test */
    public function authenticated_super_admin_can_see_create_product_view()
    {
        $this->loginAsSuperAdmin();
        $response = $this->get($this->getCreateRoute());
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('products.create');
        $response->assertSee(['name', 'price']);
    }

    /** @test */
    public function authenticates_user_without_permission_can_not_create_new_product()
    {
        $this->loginAsUser();
        $data = $this->setDataCreate();
        $response = $this->post($this->getStoreRoute(), $data);
        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

    /** @test */
    public function unauthenticated_user_can_not_see_create_product_view()
    {
        $response = $this->get($this->getCreateRoute());
        $response->assertRedirect('/login');
        $response->assertStatus(Response::HTTP_FOUND);
    }

    /** @test */
    public function authenticated_super_admin_can_create_new_product()
    {
        $this->loginAsSuperAdmin();
        $data = $this->setDataCreate();
        $productCountBefore = Product::count();
        $response = $this->post($this->getStoreRoute(), $data);
        $productCountAfter = Product::count();
        $this->assertEquals($productCountBefore + 1, $productCountAfter);
        $this->assertDatabaseHas('products', ['name' => $data['name']]);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect($this->getProductRoute());
    }

    /** @test */
    public function unauthenticated_user_can_not_create_product()
    {
        $data = $this->setDataCreate();
        $response = $this->post($this->getStoreRoute(), $data);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');
    }

    /** @test */
    public function product_belong_category()
    {
        $product = Product::factory()->create();
        $category = Category::factory()->create();
        $product->categories()->sync($category->pluck('id'));
        $this->assertDatabaseHas('category_product', ['category_id' => $category->id, 'product_id' => $product->id]);
    }

    /** @test */
    public function authenticated_super_admin_can_not_create_new_product_if_name_and_price_is_null()
    {
        $this->loginAsSuperAdmin();
        $product = Product::factory()->make(['name' => null, 'price' => null])->toArray();
        $response = $this->post($this->getStoreRoute(), $product);
        $response->assertSessionHasErrors(['name', 'price']);
    }

    /** @test */
    public function authenticated_user_has_permission_can_see_create_product_form()
    {
        $this->loginAsUserWithPermission('product_create');
        $response = $this->get($this->getCreateRoute());
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('products.create');
        $response->assertSee(['name', 'price']);
    }

    /** @test */
    public function authenticated_user_has_permission_can_create_new_product()
    {
        $this->loginAsUserWithPermission('product_store');
        $data = $this->setDataCreate();
        $response = $this->post($this->getStoreRoute(), $data);
        $this->assertDatabaseHas('products', ['name' => $data['name']]);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect(route('products.index'));
    }

    /** @test */
    public function authenticated_user_has_permission_can_not_create_new_product_if_name_and_price_are_null()
    {
        $this->loginAsUserWithPermission('product_store');
        $product = Product::factory()->make(['name' => null, 'price' => null])->toArray();
        $response = $this->post($this->getStoreRoute(), $product);
        $response->assertSessionHasErrors(['name', 'price']);
    }
}
