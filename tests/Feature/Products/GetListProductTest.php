<?php

namespace Tests\Feature\Products;

use App\Models\Product;
use Illuminate\Http\Response;
use Tests\TestCase;

class GetListProductTest extends TestCase
{
    public function getProductRoute()
    {
        return route('products.index');
    }

    /** @test */
    public function super_admin_can_get_all_products()
    {
        $this->loginAsSuperAdmin();
        $product = Product::factory()->create();
        $response = $this->get($this->getProductRoute());
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('products.index');
        $response->assertSee($product->name);
    }

    /** @test */
    public function authenticated_user_without_permission_can_not_get_all_products()
    {
        $this->loginAsUser();
        $response = $this->get(route('users.create'));
        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

    /** @test */
    public function authenticated_user_has_permission_can_get_all_products()
    {
        $this->loginAsUserWithPermission('product_view');
        $product = Product::factory()->create();
        $response = $this->get($this->getProductRoute());
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('products.index');
        $response->assertSee($product->name);
    }

    /** @test */
    public function unauthenticated_user_can_not_get_all_products()
    {
        $product = Product::factory()->create();
        $response = $this->get($this->getProductRoute());
        $response->assertRedirect('/login');
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertDontSee($product->name);
    }
}
