<?php

namespace Tests\Feature\Products;

use App\Models\Product;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class DeleteProductTest extends TestCase
{
    public function getDeleteRoute($id)
    {
        return route('products.destroy', $id);
    }

    /** @test */
    public function super_admin_can_delete_product()
    {
        $this->loginAsSuperAdmin();
        $product = Product::factory()->create();
        $response = $this->delete($this->getDeleteRoute($product->id));
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect(route('products.index'));
    }

    /** @test */
    public function authenticated_user_have_permission_can_delete_product()
    {
        $this->loginAsUserWithPermission('product_delete');
        $product = Product::factory()->create();
        $response = $this->delete($this->getDeleteRoute($product->id));
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect(route('products.index'));
    }

    /** @test */
    public function unauthenticated_super_admin_can_not_delete_product()
    {
        $product = Product::factory()->create();
        $response = $this->delete($this->getDeleteRoute($product->id));
        $response->assertRedirect('/login');
        $response->assertStatus(Response::HTTP_FOUND);
    }
}
