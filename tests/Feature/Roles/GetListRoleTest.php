<?php

namespace Tests\Feature\Roles;

use App\Models\Role;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class GetListRoleTest extends TestCase
{
    public function getRoleRoute()
    {
        return route('roles.index');
    }
    /** @test */
    public function super_admin_can_get_all_roles()
    {
        $this->loginAsSuperAdmin();
        $role = Role::factory()->create();
        $response = $this->get($this->getRoleRoute());
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('roles.index');
        $response->assertSee($role->display_name);
    }

    /** @test */
    public function authenticated_user_has_permission_can_get_all_roles()
    {
        $this->loginAsUserWithPermission('role_view');
        $role = Role::factory()->create();
        $response = $this->get($this->getRoleRoute());
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('roles.index');
        $response->assertSee($role->display_name);
    }

    /** @test */
    public function unauthenticated_user_can_not_get_all_roles()
    {
        $role = Role::factory()->create();
        $response = $this->get($this->getRoleRoute());
        $response->assertRedirect('/login');
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertDontSee($role->display_name);
    }
}
