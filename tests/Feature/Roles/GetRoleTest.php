<?php

namespace Tests\Feature\Roles;

use App\Models\Role;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class GetRoleTest extends TestCase
{
    public function getShowRoleRoute($id)
    {
        return route('roles.show', $id);
    }
    /** @test */
    public function super_admin_can_get_role()
    {
        $this->loginAsSuperAdmin();
        $role = Role::factory()->create();
        $response = $this->get($this->getShowRoleRoute($role->id));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('roles.show');
        $response->assertSee($role->display_name);
    }

    /** @test */
    public function authenticated_user_have_permission_can_get_role()
    {
        $this->loginAsUserWithPermission('role_view');
        $role = Role::factory()->create();
        $response = $this->get($this->getShowRoleRoute($role->id));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('roles.show');
        $response->assertSee($role->display_name);
    }

    /** @test */
    public function unauthenticated_user_can_not_get_role()
    {
        $role = Role::factory()->create();
        $response = $this->get($this->getShowRoleRoute($role->id));
        $response->assertRedirect('/login');
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertDontSee($role->display_name);
    }
}
