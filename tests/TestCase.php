<?php

namespace Tests;

use App\Models\Permission;
use App\Models\Role;
use App\Models\User;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Illuminate\Foundation\Testing\WithFaker;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;
    use WithFaker;

    protected function loginAsSuperAdmin(): TestCase
    {
        $user = User::factory()->create();
        $role = Role::where('name', 'super-admin')->pluck('id');
        $user->roles()->attach($role);
        return $this->actingAs($user);
    }

    protected function loginAsUser(): TestCase
    {
        $user = User::factory()->create();
        return $this->actingAs($user);
    }

    protected function loginAsUserWithPermission($permission): TestCase
    {
        $user = User::factory()->create();
        $role = Role::factory()->create();
        $user->roles()->attach($role->pluck('id'));
        $permission = Permission::where('name', $permission)->first();
        $role->permissions()->attach($permission->pluck('id'));
        return $this->actingAs($user);
    }
}
