<?php

namespace App\Repositories;

use App\Models\User;
use App\Repositories\BaseRepository;

class UserRepository extends BaseRepository
{
    public function model() : string
    {
        return User::class;
    }

    public function search($dataSearch)
    {
        return $this->model->withName($dataSearch['search'])->withEmail($dataSearch['search'])
            ->withRoleName($dataSearch['role'])->latest('id')
            ->paginate(config('constants.admin.pagination'));
    }

    public function getRoleId($id)
    {
        return $this->model->find($id)->roles()->where('user_id', $id)->pluck('role_id');
    }
}
