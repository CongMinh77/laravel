<?php

namespace App\Repositories;

use App\Models\Cart;
use App\Repositories\BaseRepository;

class CartRepository extends BaseRepository
{
    public function model() : string
    {
        return Cart::class;
    }

    public function findProductOfUser($user, $product)
    {
        return $this->model->findProductOfUser($user, $product)->get()->first();
    }

    public function getCartWithRelationship($user)
    {
        return $this->model->where('user_id', $user)->with('products')->paginate('5')->all();
    }

    public function countCarts($user)
    {
        return $this->model->where('user_id', $user)->pluck('quantity');
    }
}
