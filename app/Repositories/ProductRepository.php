<?php

namespace App\Repositories;

use App\Models\Product;
use App\Repositories\BaseRepository;

class ProductRepository extends BaseRepository
{
    public function model() : string
    {
        return Product::class;
    }

    public function search($dataSearch)
    {
        return $this->model->withName($dataSearch['search'])
            ->withPrice($dataSearch['search'])
            ->withCategoryName($dataSearch['category'])->latest('id')
            ->paginate(config('constants.admin.pagination'));
    }

    public function getLimitedProduct()
    {
        return $this->model->select('*')->paginate(config('constants.user.pagination'));
    }

    public function getLimitedNumberProduct($number)
    {
        return $this->model->select('*')->latest('created_at')
            ->take($number)->get();
    }

    public function getListLikeByUser()
    {
        return auth()->user()->likes()->with('likeable')->get();
    }

    public function getProductByCategory($category)
    {
        return $this->model->withCategoryName($category)->latest('id')
            ->paginate(config('constants.user.pagination'));
    }
}
