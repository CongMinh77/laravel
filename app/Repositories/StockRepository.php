<?php

namespace App\Repositories;

use App\Models\Stock;
use App\Repositories\BaseRepository;

class StockRepository extends BaseRepository
{
    public function model() : string
    {
        return Stock::class;
    }

    public function getAllWithRelationship()
    {
        return $this->model->getAllWithRelationship();
    }

    public function findWithRelationship($id)
    {
        return $this->model->where('id', $id)->with('products')->get();
    }

    public function search($request)
    {
        return $this->model->withProductName($request['search'])->latest('id')
            ->paginate(config('constants.admin.pagination'));
    }

    public function getProductImport($id)
    {
        return $this->model->productImport($id);
    }

    public function getProductExport($id)
    {
        return $this->model->productExport($id);
    }
}
