<?php

namespace App\Repositories;

use App\Models\Order;
use App\Repositories\BaseRepository;

class OrderRepository extends BaseRepository
{
    public function model() : string
    {
        return Order::class;
    }

    public function search($dataSearch)
    {
        return $this->model->withName($dataSearch['search'])
            ->withEmail($dataSearch['search'])
            ->withPhone($dataSearch['search'])
            ->withAddress($dataSearch['search'])
            ->latest('id')
            ->paginate(config('constants.admin.pagination'));
    }

    public function getOrderByUserId($userId)
    {
        return $this->model->withUserId($userId)->latest('id')
            ->paginate(config('constants.admin.pagination'));
    }
}
