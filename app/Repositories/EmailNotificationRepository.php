<?php

namespace App\Repositories;

use App\Models\EmailNotification;
use App\Repositories\BaseRepository;

class EmailNotificationRepository extends BaseRepository
{
    public function model() : string
    {
        return EmailNotification::class;
    }

    public function search($dataSearch)
    {
        return $this->model->withName($dataSearch['search'])->latest('id')
            ->paginate(config('constants.admin.pagination'));
    }
}
