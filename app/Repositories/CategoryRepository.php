<?php

namespace App\Repositories;

use App\Models\Category;
use App\Repositories\BaseRepository;

class CategoryRepository extends BaseRepository
{
    public function model() : string
    {
        return Category::class;
    }

    public function deleteParent($id)
    {
        $category = $this->model->where('parent_id', $id);
        return $category->delete();
    }

    public function search($dataSearch)
    {
        return $this->model->withName($dataSearch['search'])->latest('id')
            ->paginate(config('constants.admin.pagination'));
    }

    public function getCategoryIsNullParent()
    {
        return $this->model->categoryIsNullParent()->get();
    }
}
