<?php

namespace App\Repositories;

use App\Models\OrderDetail;
use App\Repositories\BaseRepository;

class OrderDetailRepository extends BaseRepository
{
    public function model() : string
    {
        return OrderDetail::class;
    }

    public function getOrderDetailByOrder($id)
    {
        return $this->model->orderDetailByOrder($id)->with('products')->get();
    }

    public function getOrderByUserId($userId)
    {
        return $this->model->withUserId($userId)->with('products')->latest()
            ->paginate(config('constants.admin.pagination'));
    }
}
