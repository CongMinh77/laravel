<?php

namespace App\Repositories;

use App\Models\Coupon;
use App\Repositories\BaseRepository;

class CouponRepository extends BaseRepository
{
    public function model() : string
    {
        return Coupon::class;
    }

    public function search($dataSearch)
    {
        return $this->model->withName($dataSearch['search'])->latest('id')
            ->paginate(config('constants.admin.pagination'));
    }
}
