<?php

namespace App\Providers;

use App\Composers\CartComposer;
use App\Composers\CategoryComposer;
use App\Composers\PermissionComposer;
use App\Composers\ProductComposer;
use App\Composers\ProductUserComposer;
use App\Composers\RoleComposer;
use App\Composers\UserComposer;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;


class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer([
            'admin.pages.dashboard',
            'admin.stocks.create',
            'admin.stocks.edit',
        ], ProductComposer::class);

        View::composer([
            'admin.pages.dashboard',
            'admin.categories.*',
            'admin.products.*',
            'user.*',
            'layouts.user.*'
        ], CategoryComposer::class);

        View::composer([
            'admin.pages.dashboard',
            'layouts.user.*'
        ], UserComposer::class);

        View::composer([
            'admin.pages.dashboard',
            'admin.users.*'
        ], RoleComposer::class);

        View::composer([
            'admin.roles.*'
        ], PermissionComposer::class);

        View::composer([
            'layouts.user.*',
            'user.cart.*',
            'user.order.*'
        ], CartComposer::class);
    }
}
