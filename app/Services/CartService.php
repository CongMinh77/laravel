<?php

namespace App\Services;

use App\Repositories\CartRepository;
use App\Repositories\CouponRepository;

class CartService
{
    protected CartRepository $cartRepository;

    /**
     * @param CartRepository $cartRepository
     */
    public function __construct(CartRepository $cartRepository)
    {
        $this->cartRepository = $cartRepository;
    }

    public function create($request, $id)
    {
        $dataCreate = $request->all();
        $dataCreate['product_id'] = $id;
        $dataCreate['user_id'] = auth()->check() ? auth()->user()->id : null;
        return $this->cartRepository->create($dataCreate);
    }

    public function all()
    {
        $user = auth()->check() ? auth()->user()->id : null;
        return $this->cartRepository->getCartWithRelationship($user);
    }

    public function update($request, $id)
    {
        $cart = $this->cartRepository->find($id);
        $dataUpdate['quantity'] = $request->quantity + $cart->quantity;
        $cart->update($dataUpdate);
        return $cart;
    }

    public function updateCart($request, $id)
    {
        $cart = $this->cartRepository->find($id);
        $dataUpdate['quantity'] = $request->quantity;
        $cart->update($dataUpdate);
        return $cart;
    }

    public function findProductOfUser($product)
    {
        $user = auth()->check() ? auth()->user()->id : null;
        return $this->cartRepository->findProductOfUser($user, $product);
    }

    public function findById($id)
    {
        return $this->cartRepository->findWithoutRedirect($id);
    }

    public function softDelete($id): int
    {
        return $this->cartRepository->delete($id);
    }

    public function totalPayment()
    {
        $total = 0;
        $user = auth()->check() ? auth()->user()->id : null;
        $carts = $this->cartRepository->getCartWithRelationship($user);
        foreach ($carts as $cart) {
            $total = $total + ($cart->quantity * $cart->products->price);
        }

        return $total;
    }

    public function multipleDelete($id)
    {
        return $this->cartRepository->multipleDelete($id);
    }

    public function countCarts()
    {
        $user = auth()->check() ? auth()->user()->id : null;
        return $this->cartRepository->countCarts($user);
    }
}
