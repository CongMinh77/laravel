<?php

namespace App\Services;

use App\Repositories\EmailNotificationRepository;
use App\Repositories\ProductRepository;
use App\Repositories\UserRepository;

class EmailNotificationService
{
    protected UserRepository $userRepository;
    protected ProductRepository $productRepository;
    protected EmailNotificationRepository $emailNotificationRepository;

    public function __construct(UserRepository $userRepository,
        ProductRepository $productRepository,
        EmailNotificationRepository $emailNotificationRepository)
    {
        $this->userRepository = $userRepository;
        $this->productRepository = $productRepository;
        $this->emailNotificationRepository = $emailNotificationRepository;
    }

    public function all()
    {
        return $this->emailNotificationRepository->all();
    }

    public function search($request)
    {
        $dataSearch = $request->all();
        $dataSearch['search'] = $request->search;
        $dataSearch = $this->emailNotificationRepository->removeSpecialCharacter($dataSearch);

        return $this->emailNotificationRepository->search($dataSearch);
    }

    public function create($request)
    {
        $dataCreate = $request->all();
        return $this->emailNotificationRepository->create($dataCreate);
    }

    public function update($request, $id)
    {
        $dataUpdate = $request->all();
        return $this->emailNotificationRepository->update($dataUpdate, $id);
    }

    public function findById($id)
    {
        return $this->emailNotificationRepository->findWithoutRedirect($id);
    }

    public function delete($id) : int
    {
        return $this->emailNotificationRepository->delete($id);
    }

    public function count()
    {
        return $this->emailNotificationRepository->count();
    }
}
