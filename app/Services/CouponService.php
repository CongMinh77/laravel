<?php

namespace App\Services;

use App\Repositories\CouponRepository;

class CouponService
{
    protected CouponRepository $couponRepository;

    /**
     * @param CouponRepository $couponRepository
     */
    public function __construct(CouponRepository $couponRepository)
    {
        $this->couponRepository = $couponRepository;
    }

    public function create($request)
    {
        $dataCreate = $request->all();
        $dataCreate['name'] = strtoupper($request->name);
        return $this->couponRepository->create($dataCreate);
    }

    public function all()
    {
        return $this->couponRepository->all();
    }

    public function update($request, $id)
    {
        $dataUpdate = $request->all();
        $dataUpdate['name'] = strtoupper($request->name);
        $stock = $this->couponRepository->find($id);
        $stock->update($dataUpdate);
        return $stock;
    }

    public function findById($id)
    {
        return $this->couponRepository->findWithoutRedirect($id);
    }

    public function softDelete($id): int
    {
        return $this->couponRepository->delete($id);
    }

    public function search($request)
    {
        $dataSearch['search'] = $request->search;
        $dataSearch = $this->couponRepository->removeSpecialCharacter($dataSearch);

        return $this->couponRepository->search($dataSearch);
    }
}
