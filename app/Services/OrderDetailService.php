<?php

namespace App\Services;

use App\Repositories\CartRepository;
use App\Repositories\CouponRepository;
use App\Repositories\OrderDetailRepository;
use App\Repositories\OrderRepository;
use Illuminate\Support\Facades\DB;

class OrderDetailService
{
    protected OrderDetailRepository $orderDetailRepository;

    /**
     * @param OrderDetailRepository $orderDetailRepository
     */
    public function __construct(OrderDetailRepository $orderDetailRepository)
    {
        $this->orderDetailRepository = $orderDetailRepository;
    }

    public function create($request, $order)
    {
        foreach ($request->product_id as $key => $value) {
            $dataCreate['product_id'] = $request->product_id[$key];
            $dataCreate['quantity'] = $request->quantity[$key];
            $dataCreate['order_id'] = $order->id;
            $this->orderDetailRepository->create($dataCreate);
        }
        return true;
    }

    public function all()
    {
        return $this->orderDetailRepository->all();
    }

    public function update($request, $id)
    {
        $cart = $this->orderDetailRepository->find($id);
        $dataUpdate['quantity'] = $request->quantity + $cart->quantity;
        $cart->update($dataUpdate);
        return $cart;
    }

    public function findById($id)
    {
        return $this->orderDetailRepository->findWithoutRedirect($id);
    }

    public function softDelete($id): int
    {
        return $this->orderDetailRepository->delete($id);
    }

    public function getOrderDetailByOrder($id)
    {
        return $this->orderDetailRepository->getOrderDetailByOrder($id);
    }

    public function getOrderByUserId()
    {
        $userId = auth()->check() ? auth()->user()->id : null;
        return $this->orderDetailRepository->getOrderByUserId($userId);
    }
}
