<?php

namespace App\Services;

use App\Repositories\CategoryRepository;

class CategoryService
{
    protected CategoryRepository $categoryRepository;

    public function __construct(CategoryRepository $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }

    public function search($request)
    {
        $dataSearch = $request->all();
        $dataSearch['search'] = $request->search;
        $dataSearch = $this->categoryRepository->removeSpecialCharacter($dataSearch);

        return $this->categoryRepository->search($dataSearch);
    }

    public function all()
    {
        return $this->categoryRepository->all();
    }

    public function create($request)
    {
        $dataCreate = $request->all();
        return $this->categoryRepository->create($dataCreate);
    }

    public function update($request, $id)
    {
        $dataUpdate = $request->all();
        return $this->categoryRepository->update($dataUpdate, $id);
    }

    public function findById($id)
    {
        return $this->categoryRepository->findWithoutRedirect($id);
    }

    public function delete($id) : int
    {
        return $this->categoryRepository->delete($id);
    }

    public function deleteParent($id)
    {
        return $this->categoryRepository->deleteParent($id);
    }

    public function count()
    {
        return $this->categoryRepository->count();
    }

    public function getCategoryIsNullParent()
    {
        return $this->categoryRepository->getCategoryIsNullParent();
    }
}
