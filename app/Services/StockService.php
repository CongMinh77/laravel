<?php

namespace App\Services;

use App\Repositories\StockRepository;

class StockService
{
    protected StockRepository $stockRepository;

    /**
     * @param StockRepository $stockRepository
     */
    public function __construct(StockRepository $stockRepository)
    {
        $this->stockRepository = $stockRepository;
    }

    public function create($request)
    {
        $dataCreate = $request->all();

        return $this->stockRepository->create($dataCreate);
    }

    public function all()
    {
        return $this->stockRepository->getAllWithRelationship();
    }

    public function update($request, $id)
    {
        $dataUpdate = $request->all();
        $stock = $this->stockRepository->find($id);
        $stock->update($dataUpdate);
        return $stock;
    }

    public function findById($id)
    {
        return $this->stockRepository->findWithoutRedirect($id);
    }

    public function findWithRelationship($id)
    {
        return $this->stockRepository->findWithRelationship($id);
    }

    public function softDelete($id): int
    {
        return $this->stockRepository->delete($id);
    }

    public function search($request)
    {
        $dataSearch['search'] = $request->search;
        $dataSearch = $this->stockRepository->removeSpecialCharacter($dataSearch);

        return $this->stockRepository->search($dataSearch);
    }

    public function checkProductInStock($id)
    {
        $productImport = $this->stockRepository->getProductImport($id);
        $productExport = $this->stockRepository->getProductExport($id);

        return array_sum($productImport) - array_sum($productExport);
    }
}
