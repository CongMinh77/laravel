<?php

namespace App\Services;

use App\Repositories\CartRepository;
use App\Repositories\OrderRepository;

class OrderService
{
    protected OrderRepository $orderRepository;
    protected CartRepository $cartRepository;
    protected OrderDetailService $orderDetailService;

    /**
     * @param OrderRepository $orderRepository
     * @param CartRepository $cartRepository
     * @param OrderDetailService $orderDetailService
     */
    public function __construct(
        OrderRepository $orderRepository,
        CartRepository $cartRepository,
        OrderDetailService $orderDetailService
    ) {
        $this->orderRepository = $orderRepository;
        $this->cartRepository = $cartRepository;
        $this->orderDetailService = $orderDetailService;
    }

    public function create($request)
    {
        $dataCreate = $request->all();
        $dataCart = $request->cart_id;
        unset(
            $dataCreate['quantity'],
            $dataCreate['product_id'],
            $dataCreate['cart_id']
        );
        $dataCreate['user_id'] = auth()->check() ? auth()->user()->id : null;
        $dataCreate['status'] = config('constants.order.pending');
        $order = $this->orderRepository->create($dataCreate);
        if (session('cart')) {
            session()->forget('cart');
        }
        else {
            $this->cartRepository->multipleDelete($dataCart);
        }
        return $this->orderDetailService->create($request, $order);
    }

    public function all()
    {
        return $this->orderRepository->all();
    }

    public function update($request, $id)
    {
        $cart = $this->orderRepository->find($id);
        $dataUpdate['quantity'] = $request->quantity + $cart->quantity;
        $cart->update($dataUpdate);
        return $cart;
    }

    public function updateStatus($request, $id)
    {
        $dataUpdate = $request->all();
        $order = $this->orderRepository->find($id);
        $order->update($dataUpdate);
    }

    public function findById($id)
    {
        return $this->orderRepository->findWithoutRedirect($id);
    }

    public function softDelete($id): int
    {
        return $this->orderRepository->delete($id);
    }

    public function search($request)
    {
        $dataSearch['search'] = $request->search;
        $dataSearch = $this->orderRepository->removeSpecialCharacter($dataSearch);
        return $this->orderRepository->search($dataSearch);
    }

    public function getOrderByUserId()
    {
        $userId = auth()->check() ? auth()->user()->id : null;
        return $this->orderRepository->getOrderByUserId($userId);
    }
}
