<?php

namespace App\Traits;

use Intervention\Image\Facades\Image;

trait HandleImage
{
    protected string $imageDefault = 'paper/img/default-product-image.png';
    protected string $imagePath = 'paper/img/products/';

    public function verifyImage($request): bool
    {
        return $request->hasFile('image') && $request->file('image');
    }

    public function saveImage($request): string
    {
        if ($this->verifyImage($request)) {
            $file = $request->file('image');
            $fileName = time() . '.' . $file->getClientOriginalExtension();
            $saveLocation = $this->imagePath . $fileName;
            Image::make($file)->resize(500, 500)->save($saveLocation);
            return $saveLocation;
        }
        return $this->imageDefault;
    }

    public function updateImage($request, $currentImage)
    {
        if ($this->verifyImage($request)) {
            $this->deleteImage($currentImage);
            return $this->saveImage($request);
        }
        return $currentImage;
    }

    public function deleteImage($imageName)
    {
        if (file_exists($imageName) && $imageName != $this->imageDefault) {
            unlink($imageName);
        }
    }
}
