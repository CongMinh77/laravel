<?php

namespace App\Composers;

use App\Services\PermissionService;
use Illuminate\View\View;

class PermissionComposer
{
    /**
     * @var PermissionService
     */
    protected PermissionService $permissionService;

    /**
     * @param PermissionService $permissionService
     */
    public function __construct(PermissionService $permissionService)
    {
        $this->permissionService = $permissionService;
    }

    /**
     * Bind data to the view.
     *
     * @param View $view
     * @return void
     */
    public function compose(View $view)
    {
        $permissions = $this->permissionService->getWithGroupName('group_name');

        $view->with('permissions', $permissions);
    }

}
