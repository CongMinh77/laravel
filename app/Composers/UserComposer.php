<?php

namespace App\Composers;

use App\Services\UserService;
use Illuminate\View\View;

class UserComposer
{
    /**
     * @var UserService
     */
    protected UserService $userService;

    /**
     * @param UserService $userService
     */
    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    /**
     * Bind data to the view.
     *
     * @param View $view
     * @return void
     */
    public function compose(View $view)
    {
        $users = $this->userService->all();
        $countUser = $this->userService->count();
        $roleIds = auth()->check() ? $this->userService->getRoleId(auth()->user()->id)->toArray() : [];

        $view->with('users', $users)
            ->with('countUser', $countUser)
            ->with('roleIds', $roleIds);
    }

}
