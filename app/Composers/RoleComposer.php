<?php

namespace App\Composers;

use App\Services\RoleService;
use Illuminate\View\View;

class RoleComposer
{
    /**
     * @var RoleService
     */
    protected RoleService $roleService;

    /**
     * @param RoleService $roleService
     */
    public function __construct(RoleService $roleService)
    {
        $this->roleService = $roleService;
    }

    /**
     * Bind data to the view.
     *
     * @param View $view
     * @return void
     */
    public function compose(View $view)
    {
        $roles = $this->roleService->all();
        $countRole = $this->roleService->count();
        $rolesWithoutSuperAdmin = $this->roleService->withoutSuperAdmin();

        $view->with('roles', $roles)
            ->with('countRole', $countRole)
            ->with('rolesWithoutSuperAdmin', $rolesWithoutSuperAdmin);
    }

}
