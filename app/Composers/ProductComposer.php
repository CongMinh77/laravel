<?php

namespace App\Composers;

use App\Services\ProductService;
use Illuminate\View\View;

class ProductComposer
{
    /**
     * @var ProductService
     */
    protected ProductService $productService;

    /**
     * @param ProductService $productService
     */
    public function __construct(ProductService $productService)
    {
        $this->productService = $productService;
    }

    /**
     * Bind data to the view.
     *
     * @param View $view
     * @return void
     */
    public function compose(View $view)
    {
        $products = $this->productService->all();
        $countProduct = $this->productService->count();

        $view->with('products', $products)
            ->with('countProduct', $countProduct);
    }

}
