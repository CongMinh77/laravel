<?php

namespace App\Composers;

use App\Services\CategoryService;
use App\Services\CouponService;
use Illuminate\View\View;

class CouponComposer
{
    /**
     * @var CouponService
     */
    protected CouponService $couponService;

    /**
     * @param CouponService $couponService
     */
    public function __construct(CouponService $couponService)
    {
        $this->couponService = $couponService;
    }

    /**
     * Bind data to the view.
     *
     * @param View $view
     * @return void
     */
    public function compose(View $view)
    {
        $categories = $this->couponService->all();

        $view->with('categories', $categories);
    }

}
