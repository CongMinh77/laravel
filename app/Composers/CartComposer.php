<?php

namespace App\Composers;

use App\Services\CartService;
use App\Services\ProductService;
use Illuminate\View\View;

class CartComposer
{
    /**
     * @var CartService
     */
    protected CartService $cartService;

    /**
     * @param CartService $cartService
     */
    public function __construct(CartService $cartService)
    {
        $this->cartService = $cartService;
    }

    /**
     * Bind data to the view.
     *
     * @param View $view
     * @return void
     */
    public function compose(View $view)
    {
        $carts = $this->cartService->all();
        $countCarts = $this->cartService->countCarts();
        $totalPayment = $this->cartService->totalPayment();

        $view->with('carts', $carts)
            ->with('countCart', array_sum($countCarts->toArray()))
            ->with('totalPayment', $totalPayment);
    }

}
