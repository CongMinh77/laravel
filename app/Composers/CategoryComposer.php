<?php

namespace App\Composers;

use App\Services\CategoryService;
use Illuminate\View\View;

class CategoryComposer
{
    /**
     * @var CategoryService
     */
    protected CategoryService $categoryService;

    /**
     * @param CategoryService $categoryService
     */
    public function __construct(CategoryService $categoryService)
    {
        $this->categoryService = $categoryService;
    }

    /**
     * Bind data to the view.
     *
     * @param View $view
     * @return void
     */
    public function compose(View $view)
    {
        $categories = $this->categoryService->all();
        $countCategory = $this->categoryService->count();
        $categoryParent = $this->categoryService->getCategoryIsNullParent();

        $view->with('categories', $categories)
            ->with('countCategory', $countCategory)
            ->with('categoryParent', $categoryParent);
    }

}
