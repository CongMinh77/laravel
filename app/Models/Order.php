<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;

    protected $table = 'orders';

    protected $fillable = [
        'user_id',
        'status',
        'total',
        'ship',
        'payment',
        'customer_name',
        'customer_email',
        'customer_phone',
        'customer_address',
        'note'
    ];

    public function scopeWithName($query, $name)
    {
        return $query->where('customer_name', 'Like', '%'. $name .'%');
    }

    public function scopeWithEmail($query, $email)
    {
        return $query->orWhere('customer_email', 'Like', '%'. $email .'%');
    }

    public function scopeWithPhone($query, $phone)
    {
        return $query->orWhere('customer_phone', 'Like', '%'. $phone .'%');
    }

    public function scopeWithAddress($query, $address)
    {
        return $query->orWhere('customer_address', 'Like', '%'. $address .'%');
    }

    public function scopeWithUserId($query, $userId)
    {
        return $query->where('user_id', $userId);
    }
}
