<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Coupon extends Model
{
    use HasFactory;

    protected $table = 'coupons';

    protected $fillable = [
        'name',
        'type',
        'value',
        'expiration_date'
    ];

    public function scopeWithName($query, $name)
    {
        return $name ? $query->where('name', 'Like', '%' . $name . '%') : null;
    }
}
