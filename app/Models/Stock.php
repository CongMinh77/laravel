<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Stock extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table = 'stocks';

    protected $fillable = [
        'product_id',
        'amount',
        'status',
        'note',
    ];

    public function products()
    {
        return $this->belongsTo(
            Product::class,
            'product_id',
            'id'
        );
    }

    public function getAllWithRelationship()
    {
        return $this->with('products')->get();
    }

    public function scopeWithProductName($query, $nameProduct)
    {
        return $query->when($nameProduct, function ($e) use ($nameProduct) {
            $e->orWhereHas('products', function ($q) use ($nameProduct) {
                $q->where('name', 'like', '%' . $nameProduct . '%');
            });
        });
    }

    public function scopeProductImport($query, $productId)
    {
        return $query->where('product_id', $productId)
            ->where('status', config('constants.stock.status.import.key'))
            ->pluck('amount')->toArray();
    }

    public function scopeProductExport($query, $productId)
    {
        return $query->where('product_id', $productId)
            ->where('status', config('constants.stock.status.export.key'))
            ->pluck('amount')->toArray();
    }
}
