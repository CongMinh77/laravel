<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticate;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Overtrue\LaravelLike\Traits\Liker;

class User extends Authenticate
{
    use HasApiTokens, HasFactory, Notifiable, Liker;

    public const SUPER_ADMIN_NAME_ROLE = 'super-admin';
    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'address',
        'phone',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function roles()
    {
        return $this->belongsToMany(
            Role::class,
            'role_user',
            'user_id',
            'role_id',
        );
    }

    public function products()
    {
        return $this->belongsToMany(
            Product::class,
            'product_user',
            'user_id',
            'product_id',
        );
    }

    public function scopeWithName($query, $name)
    {
        return $name ? $query->where('name', 'Like', '%' . $name . '%') : null;
    }

    public function scopeWithEmail($query, $email)
    {
        return $email ? $query->orWhere('email', 'Like', '%' . $email . '%') : null;
    }

    /**
     * withRoleName function
     *
     * @param $query
     * @param $role
     * @return void
     */
    public function scopeWithRoleName($query, $role)
    {
        return $role ? $query->WhereHas('roles', fn ($q) => $q->where('roles.name', $role)) : null;
    }

    public function addRole($roleId)
    {
        return $this->roles()->attach($roleId);
    }

    public function syncRole($roleId): array
    {
        return $this->roles()->sync($roleId);
    }

    public function hasRole($role)
    {
        return $this->roles->contains('name', $role);
    }

    public function hasPermission($permission): bool
    {
        foreach ($this->roles as $role) {
            if ($role->permissions->contains('name', $permission)) {
                return true;
            }
        }
        return false;
    }

    /**
     * isSuperAdmin function
     * @return void
     */
    public function isSuperAdmin()
    {
        return $this->hasRole(self::SUPER_ADMIN_NAME_ROLE);
    }
}
