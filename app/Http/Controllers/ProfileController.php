<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProfileRequest;
use App\Http\Requests\PasswordRequest;
use App\Services\UserService;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Hash;
use Illuminate\View\View;

class ProfileController extends Controller
{
    protected UserService $userService;

    /**
     * @param UserService $userService
     */
    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    /**
     * Show the form for editing the profile.
     *
     * @return View
     */
    public function edit()
    {
        $roleIds = auth()->check() ? $this->userService->getRoleId(auth()->user()->id)->toArray() : [];

        if (in_array(config('constants.admin.admin_role'), $roleIds) || in_array(config('constants.admin.staff_role'), $roleIds) ||
            in_array(config('constants.admin.super_admin'), $roleIds)) {
            return view('admin.profile.edit');
        }
        else {
            return view('user.profile.edit');
        }
    }

    /**
     * Update the profile
     *
     * @param ProfileRequest $request
     * @return RedirectResponse
     */
    public function update(ProfileRequest $request)
    {
        auth()->user()->update($request->all());

        return back()->withStatus(__('Profile successfully updated.'));
    }

    /**
     * Change the password
     *
     * @param PasswordRequest $request
     * @return RedirectResponse
     */
    public function password(PasswordRequest $request)
    {
        auth()->user()->update(['password' => Hash::make($request->get('password'))]);

        return back()->withPasswordStatus(__('Password successfully updated.'));
    }
}
