<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\Product\AddToCartRequest;
use App\Services\CartService;
use App\Services\ProductService;
use App\Services\StockService;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;

class CartController extends Controller
{
    protected ProductService $productService;
    protected CartService $cartService;
    protected StockService $stockService;

    /**
     * @param ProductService $productService
     * @param CartService $cartService
     */
    public function __construct(ProductService $productService, CartService $cartService)
    {
        $this->productService = $productService;
        $this->cartService = $cartService;
    }

    public function index()
    {
        return view('user.cart.index');
    }

    public function addToCart(AddToCartRequest $request, $id)
    {
        $product = $this->productService->findById($id);
        $inCart = $this->cartService->findProductOfUser($product->id);
        if ($inCart) {
            $this->cartService->update($request, $inCart->id);
        }
        else {
            if (auth()->check() && $product) {
                $this->cartService->create($request, $product->id);
            }
            else {
                $cartSession = session()->get('cart', []);

                if(isset($cartSession[$id])) {
                    $cartSession[$id]['quantity'] = $cartSession[$id]['quantity'] + $request->quantity;
                } else {
                    $cartSession[$id] = [
                        "name" => $product->name,
                        "quantity" => $request->quantity,
                        "price" => $product->price,
                        "image" => $product->image
                    ];
                }

                session()->put('cart', $cartSession);
                $cartSession = session()->get('cart', []);
                return redirect()->route('cart.index')->with('success', 'Product added to cart successfully!');
            }
        }
        return redirect()->route('cart.index');
    }

    /**
     * Write code on Method
     *
     * @param Request $request
     * @return JsonResponse()
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function update(Request $request)
    {
        if (auth()->check()) {
            try {
                $cart = $this->cartService->updateCart($request, $request->id);

                return response()->json([
                    'status' => 'success',
                    'data' => $cart,
                    'remove' => $request->quantity < 1,
                    'message' =>  'Update success!'
                ], Response::HTTP_OK);
            } catch (Exception $e) {
                return response()->json([
                    'status' => 'failed',
                    'message' => $e->getMessage(),
                ], Response::HTTP_INTERNAL_SERVER_ERROR);
            }
        }
        else {
            if($request->id && $request->quantity){
                $cart = session()->get('cart');
                $cart[$request->id]["quantity"] = $request->quantity;
                session()->put('cart', $cart);
                session()->flash('success', 'Cart updated successfully');
            }
        }
    }

    /**
     * Write code on Method
     *
     * @param Request $request
     * @return JsonResponse()
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function remove(Request $request)
    {
        if(auth()->check()) {
            try {
                $this->cartService->softDelete($request->id);

                return response()->json([
                    'status' => 'success',
                    'message' =>  'Remove success!'
                ], Response::HTTP_OK);
            } catch (Exception $e) {
                return response()->json([
                    'status' => 'failed',
                    'message' => $e->getMessage(),
                ], Response::HTTP_INTERNAL_SERVER_ERROR);
            }
        }
        else {
            $cart = session()->get('cart');
            if(isset($cart[$request->id])) {
                unset($cart[$request->id]);
                session()->put('cart', $cart);
            }
            session()->flash('success', 'Product removed successfully');
        }
    }

    public function listCart()
    {
        return view('user.cart.list');
    }
}
