<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\Order\StoreOrderRequest;
use App\Services\OrderDetailService;
use App\Services\OrderService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Exception;
use Illuminate\Support\Facades\DB;

class OrderController extends Controller
{
    protected OrderService $orderService;
    protected OrderDetailService $orderDetailService;

    /**
     * @param OrderService $orderService
     * @param OrderDetailService $orderDetailService
     */
    public function __construct(
        OrderService $orderService,
        OrderDetailService $orderDetailService
    ) {
        $this->orderService = $orderService;
        $this->orderDetailService = $orderDetailService;
    }

    public function index()
    {
        return view('user.order.index');
    }

    public function store(StoreOrderRequest $request)
    {
        DB::beginTransaction();
            $this->orderService->create($request);
        DB::commit();
        return redirect()->route('order.show');
    }

    public function show()
    {
        $orderUser = $this->orderService->getOrderByUserId();
        return view('user.order.show', compact('orderUser'));
    }

    public function showDetail($id)
    {
        $order = $this->orderService->findById($id);
        $orderDetail = $this->orderDetailService->getOrderDetailByOrder($id);
        return view('user.order.showDetail', compact('order', 'orderDetail'));
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function updateStatus(Request $request)
    {
        try {
            $this->orderService->updateStatus($request, $request->id);

            return response()->json([
                'status' => 'success',
                'message' =>  'Update success!'
            ], Response::HTTP_OK);
        } catch (Exception $e) {
            return response()->json([
                'status' => 'failed',
                'message' => $e->getMessage(),
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function destroy($id)
    {
        $this->orderService->softDelete($id);
        return redirect()->back();
    }
}
