<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Resources\EmailNotification\EmailNotificationResource;
use App\Services\EmailNotificationService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class EmailNotificationController extends Controller
{
    /**
     * @var EmailNotificationService
     */
    protected EmailNotificationService $emailNotificationService;

    /**
     * @param EmailNotificationService $emailNotificationService
     */
    public function __construct(EmailNotificationService $emailNotificationService)
    {
        $this->emailNotificationService = $emailNotificationService;
    }

    public function store(Request $request)
    {
        try {
            $email = $this->emailNotificationService->create($request);
            $emailResource = new EmailNotificationResource($email);
            return $this->sentResponseAjax(
                $emailResource,
                'Register email successfully!',
                Response::HTTP_CREATED
            );
        }
        catch (\Exception $e) {
            return response()->json([
                'message' => $e->getMessage(),
            ], 500);
        }
    }
}
