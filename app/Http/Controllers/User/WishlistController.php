<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Services\ProductService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class WishlistController extends Controller
{
    protected ProductService $productService;

    /**
     * @param ProductService $productService
     */
    public function __construct(ProductService $productService)
    {
        $this->productService = $productService;
    }

    public function index()
    {
        if (auth()->user()) {
            try {
                $response = $this->productService->getListLikedByUser();

                return response()->json([
                    'status' => 'Get list success!',
                    'success' => $response
                ], Response::HTTP_OK);
            } catch (\Exception $e) {
                return response()->json([
                    'status' => 'failed',
                    'message' => $e->getMessage(),
                ], Response::HTTP_INTERNAL_SERVER_ERROR);
            }
        }
        else {
            return redirect()->route('login');
        }
    }

    public function listLike()
    {
        if (auth()->check()) {
            $likes = $this->productService->getListLikedByUser();
            return view('user.wishlist.index', compact('likes'));
        }
        else {
            return redirect()->route('login');
        }
    }

    public function list()
    {
        if (auth()->check()) {
            $likes = $this->productService->getListLikedByUser();
            return view('user.wishlist.list', compact('likes'));
        }
        else {
            return redirect()->route('login');
        }
    }

    public function like(Request $request)
    {
        if (auth()->check())
        {
            try {
                $response = $this->productService->likeProduct($request->id);

                return response()->json([
                    'status' => 'success',
                    'message' =>  'Like success!',
                    'success' => $response
                ], Response::HTTP_OK);
            } catch (\Exception $e) {
                return response()->json([
                    'status' => 'failed',
                    'message' => $e->getMessage(),
                ], Response::HTTP_INTERNAL_SERVER_ERROR);
            }
        }
        else {
            return redirect()->route('login');
        }
    }
}
