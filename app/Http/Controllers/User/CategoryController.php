<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Services\ProductService;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    protected ProductService $productService;

    /**
     * @param ProductService $productService
     */
    public function __construct(ProductService $productService)
    {
        $this->productService = $productService;
    }

    public function index($page)
    {
        if ($page == 'All Products') {
            $productByCate = $this->productService->getListProductByCategory(null);
        }
        else {
            $productByCate = $this->productService->getListProductByCategory($page);
        }
        return view('user.categories.index', compact('productByCate', 'page'));
    }

    public function list($page)
    {
        if ($page == 'allProduct') {
            $products = $this->productService->getListProductByCategory(null);
        }
        else {
            $products = $this->productService->getListProductByCategory($page);
        }
        return view('user.categories.list', compact('products'));
    }
}
