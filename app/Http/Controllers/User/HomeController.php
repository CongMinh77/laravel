<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Services\ProductService;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;

class HomeController extends Controller
{
    protected ProductService $productService;

    /**
     * @param ProductService $productService
     */
    public function __construct(ProductService $productService)
    {
        $this->productService = $productService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|View
     */
    public function index()
    {
        $products = $this->productService->getLimitedProduct();
        $slideProducts = $this->productService->getLimitedNumberProduct(5);
        $visitProducts = $this->productService->getLimitedNumberProduct(6);
        return view('user.home.index', compact('products', 'slideProducts', 'visitProducts'));
    }

}
