<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Services\CategoryService;
use App\Services\ProductService;
use App\Services\StockService;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    protected ProductService $productService;
    protected CategoryService $categoryService;
    protected StockService $stockService;

    public function __construct(
        ProductService $productService,
        CategoryService $categoryService,
        StockService $stockService
    ) {
        $this->productService = $productService;
        $this->categoryService = $categoryService;
        $this->stockService = $stockService;
    }

    public function detail($id)
    {
        $product = $this->productService->findById($id);
        $stock = $this->stockService->checkProductInStock($id);
        return view('user.product.detail', compact('product', 'stock'));
    }
}
