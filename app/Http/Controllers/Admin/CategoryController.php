<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Category\StoreCategoryRequest;
use App\Http\Requests\Category\UpdateCategoryRequest;
use App\Http\Resources\Category\CategoryResource;
use App\Services\CategoryService;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;

class CategoryController extends Controller
{
    protected CategoryService $categoryService;

    public function __construct(CategoryService $categoryService)
    {
        $this->categoryService = $categoryService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        return view('admin.categories.index');
    }

    public function list(Request $request)
    {
        $categoriesByPage = $this->categoryService->search($request);
        return view('admin.categories.list', compact('categoriesByPage'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreCategoryRequest $request
     * @return JsonResponse
     */
    public function store(StoreCategoryRequest $request)
    {
        $category = $this->categoryService->create($request);
        $categoryResource = new CategoryResource($category);

        return $this->sentSuccessResponse(
            $categoryResource,
            'Create category successfully!',
            Response::HTTP_OK
        );
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return JsonResponse
     */
    public function show(int $id)
    {
        $category = $this->categoryService->findById($id);
        $categoryResource = new CategoryResource($category);

        return $this->sentSuccessResponse(
            $categoryResource,
            'Success!',
            Response::HTTP_OK
        );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return JsonResponse
     */
    public function edit(int $id)
    {
        $category = $this->categoryService->findById($id);
        $categoryResource = new CategoryResource($category);

        return $this->sentSuccessResponse(
            $categoryResource,
            'Success!',
            Response::HTTP_OK
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateCategoryRequest $request
     * @param int $id
     * @return JsonResponse
     */
    public function update(UpdateCategoryRequest $request, int $id)
    {
        $category = $this->categoryService->update($request, $id);
        $categoryResource = new CategoryResource($category);
        return $this->sentSuccessResponse(
            $categoryResource,
            'Update category successfully!',
            Response::HTTP_OK
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return JsonResponse
     */
    public function destroy(int $id)
    {
        DB::beginTransaction();
            $this->categoryService->delete($id);
            $this->categoryService->deleteParent($id);
        DB::commit();
        return $this->sentSuccessResponse(
            '',
            'Delete category successfully!',
            Response::HTTP_OK
        );
    }
}
