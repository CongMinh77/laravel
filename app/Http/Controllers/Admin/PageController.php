<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Services\CategoryService;
use App\Services\ProductService;
use App\Services\RoleService;
use App\Services\UserService;
use Illuminate\View\View;

class PageController extends Controller
{
    protected RoleService $roleService;
    protected UserService $userService;
    protected ProductService $productService;
    protected CategoryService $categoryService;

    /**
     * Create a new controller instance.
     *
     * @param RoleService $roleService
     * @param UserService $userService
     * @param ProductService $productService
     * @param CategoryService $categoryService
     */
    public function __construct(
        RoleService     $roleService,
        UserService     $userService,
        ProductService  $productService,
        CategoryService $categoryService
    ) {
        $this->middleware('auth');

        $this->roleService = $roleService;
        $this->userService = $userService;
        $this->productService = $productService;
        $this->categoryService = $categoryService;
    }

    /**
     * Display all the static pages when authenticated
     *
     * @param string $page
     * @return View
     */
    public function index(string $page): View
    {
        if (view()->exists("admin.pages.{$page}")) {
            return view("admin.pages.{$page}");
        }
        return abort(404);
    }
}
