<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Stock\StoreStockRequest;
use App\Services\StockService;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class StockController extends Controller
{
    protected StockService $stockService;

    /**
     * @param StockService $stockService
     */
    public function __construct(StockService $stockService)
    {
        $this->stockService = $stockService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|View
     */
    public function index(Request $request)
    {
        $stocks = $this->stockService->search($request);
        return view('admin.stocks.index', compact('stocks'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Application|Factory|View
     */
    public function create()
    {
        return view('admin.stocks.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreStockRequest $request
     * @return RedirectResponse
     */
    public function store(StoreStockRequest $request)
    {
        $this->stockService->create($request);
        return redirect()->route('stocks.index')->with('message', 'Create Stock Success!');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Application|Factory|View
     */
    public function show(int $id)
    {
        $stock = $this->stockService->findWithRelationship($id);
        return view('admin.stocks.show', compact('stock'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Application|Factory|View
     */
    public function edit(int $id)
    {
        $stock = $this->stockService->findById($id);
        return view('admin.stocks.edit', compact('stock'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param StoreStockRequest $request
     * @param int $id
     * @return RedirectResponse
     */
    public function update(StoreStockRequest $request, int $id)
    {
        $this->stockService->update($request, $id);
        return redirect()->route('stocks.index')->with('message', 'Update Stock Success!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return RedirectResponse
     */
    public function destroy(int $id)
    {
        $this->stockService->softDelete($id);
        return redirect()->route('stocks.index')->with('message', 'Delete Stock Success!');
    }
}
