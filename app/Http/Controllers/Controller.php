<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\JsonResponse;
use Illuminate\Routing\Controller as BaseController;
use function response;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * sent success responce
     * @param string $message
     * @param  $resource
     * @param int $status
     * @return JsonResponse
     */
    public function sentSuccessResponse($resource, string $message = 'success', int $status = 200): JsonResponse
    {
        return response()->json([
            'status' => $status,
            'message' => $message,
            'category' => $resource
        ], $status);
    }

    /**
     * @param $resource
     * @param string $message
     * @param int $status
     * @return JsonResponse
     */
    public function sentResponseAjax($resource, string $message = 'success', int $status = 200): JsonResponse
    {
        return response()->json([
            'status' => $status,
            'message' => $message,
            'data' => $resource
        ], $status);
    }
}
