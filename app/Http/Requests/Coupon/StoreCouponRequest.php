<?php

namespace App\Http\Requests\Coupon;

use Illuminate\Foundation\Http\FormRequest;

class StoreCouponRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|regex:/^\S*$/u|unique:coupons,name',
            'value' => 'required',
            'type' => 'required',
            'expiration_date' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'name.regex' => 'Name field cannot contain special characters'
        ];
    }
}
