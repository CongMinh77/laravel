<?php

namespace App\Http\Requests\Order;

use App\Rules\CheckPhoneNumberRule;
use Illuminate\Foundation\Http\FormRequest;

class StoreOrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'total' => 'required',
            'ship' => 'required',
            'customer_name' => 'required',
            'customer_email' => 'required|email',
            'customer_phone' => ['required', new CheckPhoneNumberRule()],
            'customer_address' => 'required',
        ];
    }

    public function messages()
    {
        return [

        ];
    }
}
