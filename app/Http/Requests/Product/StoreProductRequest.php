<?php

namespace App\Http\Requests\Product;

use Illuminate\Foundation\Http\FormRequest;

class StoreProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:100',
            'price' => 'required',
            'image' => 'image|nullable|mimes:jpeg,png,jpg,gif',
            'category_id' => 'required',
            'description' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'category_id.required' => 'The category field is required',
        ];
    }
}
