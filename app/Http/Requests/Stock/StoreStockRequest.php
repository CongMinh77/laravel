<?php

namespace App\Http\Requests\Stock;

use App\Rules\CheckExportProductRule;
use Illuminate\Foundation\Http\FormRequest;

class StoreStockRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'product_id' => 'required',
            'amount' => ['required', new CheckExportProductRule],
            'status' => 'required|in:1,2',
        ];
    }
}
