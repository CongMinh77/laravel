<?php

namespace App\Http\Middleware;

use App\Services\CartService;
use Closure;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class CanNotCheckOutIfEmptyCart
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return Response|RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $cart = app(CartService::class)->countCarts();

        if (array_sum($cart->toArray()) > 0 || session('cart')) {
            return $next($request);
        }
        else {
            return abort('403');
        }
    }
}
