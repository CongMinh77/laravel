<?php

namespace App\Rules;

use App\Services\StockService;
use Illuminate\Contracts\Validation\Rule;

class CheckProductInStock implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $product_id = request()->product_id;
        $amount = app(StockService::class)->checkProductInStock($product_id);
        if ($amount >= $value)
            return true;
        else
            return false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        $product_id = request()->product_id;
        $amount = app(StockService::class)->checkProductInStock($product_id);
        return 'Quantity of product must be less than or equal ' . $amount;
    }
}
