<?php

namespace App\Rules;

use App\Models\Stock;
use Illuminate\Contracts\Validation\Rule;

class CheckExportProductRule implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $product_id = request()->product_id;
        $status = request()->status;
        $amountProductsImport = Stock::where('product_id', $product_id)
            ->where('status', config('constants.stock.status.import.key'))
            ->pluck('amount')->toArray();
        $amountProductsExport = Stock::where('product_id', $product_id)
            ->where('status', config('constants.stock.status.export.key'))
            ->pluck('amount')->toArray();

        if ($status == config('constants.stock.status.import.key')) {
            return true;
        }
        else {
            if ($value > array_sum($amountProductsImport) - array_sum($amountProductsExport) || $value < 0) {
                return false;
            }
            else {
                return true;
            }
        }
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        $product_id = request()->product_id;
        $amountProductsImport = Stock::where('product_id', $product_id)
            ->where('status', config('constants.stock.status.import.key'))
            ->pluck('amount')->toArray();
        $amountProductsExport = Stock::where('product_id', $product_id)
            ->where('status', config('constants.stock.status.export.key'))
            ->pluck('amount')->toArray();
        $sum = array_sum($amountProductsImport) - array_sum($amountProductsExport);
        return trans('validation.stock.amount.export-product') . $sum;
    }
}
