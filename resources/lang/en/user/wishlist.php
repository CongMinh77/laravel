<?php

return [
    'title' => 'Wishlist',
    'no_product' => 'No products in the wishlist'
];
