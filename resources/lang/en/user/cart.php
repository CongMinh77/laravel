<?php

return [
    'title' => 'Cart',
    'no_product' => 'No products in the cart'
];
