<?php

return [
    'back_to_the_store' => 'Back to the store',
    'total_payment' => 'Total payment',
    'voucher' => 'Voucher',
    'checkout' => 'Checkout'
];
