<!DOCTYPE html>
<html>
<head>
    <title>403 Forbidden</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css"
          integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
</head>
<body style="background-image: url('https://mdbootstrap.com/img/Photos/Others/images/76.jpg'); background-size: cover;">
    <div class="container justify-content-center mt-5 pt-5">
        <div class="container justify-content-center p-5">
            <h1 class="text-center text-white" style="font-size: 8em">
                403 Unauthorized
            </h1>
            <h5 class="text-white text-center">
                You don't have permission to access
            </h5>
            <h5 class="text-center mt-3 pt-5">
                <a class="text-dark" href="{{route('home')}}">
                    <i class="fas fa-chevron-left"></i>
                    Return Dashboard
                </a>
            </h5>
        </div>
    </div>
</body>
</html>
