<nav class="navbar navbar-expand-lg navbar-light bg-light sticky-top">
    <div class="container-fluid p-0 max-width-1200">
        <div class="navbar-brand font-weight-bold">Furniture</div>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav flex-grow-1 justify-content-around d-flex w-100%">
                <li class="nav-item {{ request()->segment(1) ?? 'active' }} font-weight-bold">
                    <a class="nav-link" href="{{ route('home') }}">
                        Home
                    </a>
                </li>
                @foreach($categoryParent as $category)
                    <li class="nav-item {{ request()->segment(1) == $category->name ? 'active' : '' }} font-weight-bold">
                        <a class="nav-link" href="{{ route('page.index', $category->name) }}">
                            {{ $category->name }}
                        </a>
                    </li>
                @endforeach
            </ul>
            <div class="col-2 d-flex justify-content-end pr-0">
{{--                <button class="btn btn-light rounded-circle btn-header mr-1">--}}
{{--                    <i class="fa-solid fa-magnifying-glass icon fa-lg"></i>--}}
{{--                </button>--}}
                @auth()
                    @include('user.cart.cart')
                @endauth
                @guest()
                    @include('user.cart.cart-guest')
                @endguest
                <div class="dropdown">
                    <button class="btn btn-light rounded-circle btn-header {{ auth()->check() ? 'image-avatar ml-2' : '' }}" id="dropdownUser"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        @guest
                            <i class="fa-solid fa-user icon fa-lg"></i>
                        @endguest
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownUser">
                        @auth
                            <a class="dropdown-item d-flex align-items-center w-100" href="{{ route('wishlist.list') }}">
                                <div class="flex-grow-1">
                                    Wish list
                                </div>
                                <span class="badge badge-danger justify-content-end" id="amount-wish-list"></span>
                            </a>
                        @endauth
                        @auth
                            <a class="dropdown-item d-flex align-items-center w-100" href="{{ route('order.show') }}">
                                <div class="flex-grow-1">
                                    Purchase
                                </div>
                            </a>
                        @endauth
                        <a class="dropdown-item d-flex align-items-center w-100" href="{{ route('profile.edit') }}">
                            <div class="flex-grow-1">
                                Profile
                            </div>
                        </a>
                        @guest
                            <a class="dropdown-item d-flex align-items-center" href="{{ route('login') }}">
                                <div class="flex-grow-1">
                                    Log In
                                </div>
                                <i class="fa-solid fa-right-to-bracket"></i>
                            </a>
                        @endguest
                        @auth()
                            <form class="dropdown-item d-none" action="{{ route('logout') }}"
                                  id="formLogOut" method="POST">
                                @csrf
                            </form>
                            <a class="dropdown-item d-flex align-items-center"
                               onclick="document.getElementById('formLogOut').submit();">
                                <div class="flex-grow-1">
                                    Log Out
                                </div>
                                <i class="fa-solid fa-right-from-bracket"></i>
                            </a>
                        @endauth
                    </div>
                </div>
            </div>
        </div>
    </div>
</nav>
