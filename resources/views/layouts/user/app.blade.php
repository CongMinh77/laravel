<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <link rel="icon" type="image/png" href="{{ asset('paper/img/NoNameee-01.png') }}">

    <title>
        @yield('title') | {{ config('app.name') }}
    </title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no'
          name='viewport' />
    @yield('meta-tag')

    <!-- Fonts and icons -->
    <link href="//fonts.googleapis.com/css?family=Quicksand&display=swap" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css"
          integrity="sha512-KfkfwYDsLkIlwQp6LFnl8zNdLGxu9YAA1QvwINks4PhcElQSvqcyVLLD9aMhXd13uQjoXtEKNosOWaZqXgel0g==" crossorigin="anonymous" referrerpolicy="no-referrer" />

    <!-- CSS Files -->
    <link href="{{ asset('paper/css/bootstrap.min.css') }}" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="{{ asset('paper/css/user/slick.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('paper/css/user/slick-theme.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('paper/css/jquery.toast.css') }}"/>

    <!-- Sweet Alert2 -->
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10.15.7/dist/sweetalert2.all.min.js"></script>
    <!-- Custom css -->
    <link href="{{ asset('paper/css/user/layout.css') }}" rel="stylesheet" />

    @yield('style')
    @stack('css')

</head>

<body class="position-relative">
    @include('layouts.user.header')
    <div id="wrapper-content">
        <main>
            <div id="content">
                @yield('content')
            </div>
        </main>
        <div id="other-content">
            @yield('other-content')
        </div>
    </div>
    @include('layouts.user.footer')

    <button id="scrollToTop" class="btn btn-outline-dark rounded-circle btn-scroll-to-top">
        <i class="fa fa-angle-double-up"></i>
    </button>

    @if(in_array(config('constants.admin.admin_role'), $roleIds) ||
        in_array(config('constants.admin.staff_role'), $roleIds) ||
        in_array(config('constants.admin.super_admin'), $roleIds))
        <div class="bg-secondary w-10 position-fixed bg-btn-admin">
            <a href="{{ route('admin.home') }}" target="_blank" class="btn btn-outline-light rounded-circle btn-to-admin">
                <i class="fa-solid fa-screwdriver-wrench"></i>
            </a>
        </div>
    @endif
    <!--   Core JS Files   -->
    <script>
        const userId = @json(auth()->check() ? auth()->user()->id : 0);
        const urlGetWishList = @json(route('wishlist.index'));
    </script>
    <script src="{{ asset('paper/js/core/jquery.min.js') }}"></script>
    <script src="{{ asset('paper/js/core/popper.min.js') }}"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
    <script src="{{ asset('paper/js/user/common.js') }}"></script>
    <script src="{{ asset('paper/js/plugins/jquery.toast.js') }}"></script>
    <script type="text/javascript" charset="utf-8">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
        });
    </script>
    @yield('scripts')
    @stack('js')
</body>

</html>
