<div class="wrapper-footer container-fluid bg-light p-0">
    <div class="row max-width-1200 pb-3 pt-3">
        <div class="col-12 p-0">
            <div class="navbar-brand-footer font-weight-bold">Furniture</div>
        </div>
        <div class="col-3 pl-0">
            <ul class="nav flex-column">
                <li class="nav-item font-weight-bold d-flex align-items-center mt-3">
                    <i class="fa-solid fa-location-dot icon-footer"></i>
                    <div class="text-dark ml-1">
                        46/4 Ba Trieu, Nguyen Trai,<br> Ha Dong, Ha Noi
                    </div>
                </li>
                <li class="nav-item font-weight-bold d-flex align-items-center mt-3">
                    <i class="fa-solid fa-phone icon-footer"></i>
                    <a class="ml-1 text-dark" href="tel:0949389572">0949389572</a>
                </li>
                <li class="nav-item font-weight-bold d-flex align-items-center mt-3">
                    <i class="fa-solid fa-envelope icon-footer"></i>
                    <a class="ml-1 text-dark" href="mailto:info@example.com">info@example.com</a>
                </li>
            </ul>
        </div>
        <div class="col-3">
            <div class="navbar-brand font-weight-bold text-uppercase border-bottom">Information</div>
            <ul class="nav flex-column mt-3">
                <li class="nav-item font-weight-bold d-flex align-items-center mt-1">
                    <a href="#" class="text-dark">
                        @lang('user/footer.about-us')
                    </a>
                </li>
                <li class="nav-item font-weight-bold d-flex align-items-center mt-3">
                    <a href="#" class="text-dark">Career</a>
                </li>
            </ul>
        </div>
        <div class="col-3">
            <div class="navbar-brand font-weight-bold text-uppercase border-bottom">My Account</div>
            <ul class="nav flex-column mt-3">
                <li class="nav-item font-weight-bold d-flex align-items-center mt-1">
                    <a href="{{ route('profile.edit') }}" class="text-dark">
                        My account
                    </a>
                </li>
                <li class="nav-item font-weight-bold d-flex align-items-center mt-3">
                    <a href="{{ route('cart.index') }}" class="text-dark">Cart</a>
                </li>
                <li class="nav-item font-weight-bold d-flex align-items-center mt-3">
                    <a href="{{ route('wishlist.list') }}" class="text-dark">Wishlist</a>
                </li>
                <li class="nav-item font-weight-bold d-flex align-items-center mt-3">
                    <a href="{{ route('order.show') }}" class="text-dark">Order history</a>
                </li>
            </ul>
        </div>
        <div class="col-3">
            <div class="navbar-brand font-weight-bold text-uppercase border-bottom">Help & Support</div>
            <ul class="nav flex-column mt-3">
                <li class="nav-item font-weight-bold d-flex align-items-center mt-1">
                    <a class="text-dark">
                        How to shop
                    </a>
                </li>
                <li class="nav-item font-weight-bold d-flex align-items-center mt-3">
                    <a href="#" class="text-dark">Payment</a>
                </li>
                <li class="nav-item font-weight-bold d-flex align-items-center mt-3">
                    <a href="#" class="text-dark">Services</a>
                </li>
            </ul>
        </div>
    </div>
    <div class="border-middle"></div>
    <div class="row max-width-1200 pb-4 pt-4">
        <div class="flex-grow-1 align-items-center d-flex">
            © Copyright&nbsp;
            <a href="https://www.facebook.com/NCM.SE7EN" target="_blank" class="text-dark">NCM</a>.
            Designed and Developed by&nbsp;
            <a href="https://www.facebook.com/NCM.SE7EN" target="_blank" class="text-dark">NCM</a>
        </div>
        <div class="d-flex align-items-center">
            <span class="m-0 font-weight-bold">
                @lang('user/footer.follow-us-on')
            </span>
            <ul class="nav ml-2">
                <li class="nav-item mr-2">
                    <a href="#" class="text-dark">
                        <i class="fa-brands fa-facebook fa-lg"></i>
                    </a>
                </li>
                <li class="nav-item mr-2">
                    <a href="#" class="text-dark">
                        <i class="fa-brands fa-instagram fa-lg"></i>
                    </a>
                </li>
                <li class="nav-item mr-2">
                    <a href="#" class="text-dark">
                        <i class="fa-brands fa-twitter fa-lg"></i>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="#" class="text-dark">
                        <i class="fa-brands fa-youtube fa-lg"></i>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>
