<div class="sidebar" data-color="black" data-active-color="danger">
    <div class="logo">
        <a href="{{ route('admin.home') }}" class="simple-text logo-mini">
            <div class="logo-image-small">
                <img class="rounded" src="{{ asset('paper/img/NoName-02.png') }}" alt="Logo">
            </div>
        </a>
        <a href="{{ route('admin.home') }}" class="simple-text logo-normal">
            {{__('Furniture')}}
        </a>
    </div>
    <div class="sidebar-wrapper">
        <ul class="nav">
            <li class="{{ $elementActive == 'dashboard' ? 'active' : '' }}">
                <a href="{{ route('admin.home') }}">
                    <i class="nc-icon nc-bank"></i>
                    <p>{{ __('Dashboard') }}</p>
                </a>
            </li>
            <li class="{{ $elementActive == 'users' ? 'active' : '' }}">
                @hasPermission('user_read')
                    <a href="{{ route('users.index') }}">
                        <i class="nc-icon nc-single-02"></i>
                        <p>{{ __(' User Management ') }}</p>
                    </a>
                @endhasPermission
            </li>
            <li class="{{ $elementActive == 'roles' ? 'active' : '' }}">
                @hasPermission('role_read')
                    <a href="{{ route('roles.index') }}">
                        <i class="nc-icon nc-badge"></i>
                        <p>{{ __(' Role Management ') }}</p>
                    </a>
                @endhasPermission
            </li>
            <li class="{{ $elementActive == 'orders' ? 'active' : '' }}">
                @hasPermission('order_read')
                <a href="{{ route('orders.index') }}">
                    <i class="far fa-clipboard-list-check"></i>
                    <p>{{ __(' Order Management ') }}</p>
                </a>
                @endhasPermission
            </li>
            <li class="{{ $elementActive == 'categories' ? 'active' : '' }}">
                @hasPermission('category_read')
                    <a href="{{ route('categories.index') }}">
                        <i class="nc-icon nc-bullet-list-67"></i>
                        <p>{{ __(' Category Management ') }}</p>
                    </a>
                @endhasPermission
            </li>
            <li class="{{ $elementActive == 'products' ? 'active' : '' }}">
                @hasPermission('product_read')
                    <a href="{{ route('products.index') }}">
                        <i class="nc-icon nc-basket"></i>
                        <p>{{ __(' Product Management ') }}</p>
                    </a>
                @endhasPermission
            </li>
            <li class="{{ $elementActive == 'stocks' ? 'active' : '' }}">
                @hasPermission('stock_read')
                    <a href="{{ route('stocks.index') }}">
                        <i class="nc-icon nc-box-2"></i>
                        <p>{{ __(' Stock Management ') }}</p>
                    </a>
                @endhasPermission
            </li>
            <li class="{{ $elementActive == 'coupons' ? 'active' : '' }}">
                @hasPermission('coupon_read')
                <a href="{{ route('coupons.index') }}">
                    <i class="fas fa-ticket-alt"></i>
                    <p>{{ __('Coupon Management') }}</p>
                </a>
                @endhasPermission
            </li>
            <li class="{{ $elementActive == 'emails' ? 'active' : '' }}">
                <a href="{{ route('emails.index') }}">
                    <i class="fal fa-envelope"></i>
                    <p>{{ __('Email Management') }}</p>
                </a>
            </li>
            <li class="{{ $elementActive == 'ships' ? 'active' : '' }}">
                @hasPermission('ship_read')
                <a href="{{ route('ships.index') }}">
                    <i class="fas fa-truck"></i>
                    <p>{{ __('Shipping Management') }}</p>
                </a>
                @endhasPermission
            </li>
        </ul>
    </div>
</div>
