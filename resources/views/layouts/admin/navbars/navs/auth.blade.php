<nav class="navbar navbar-expand-lg navbar-absolute fixed-top navbar-transparent mt-0 border-bottom">
    <div class="container-fluid">
        <div class="navbar-wrapper">
            <h3 class="m-0 text-dark">
                {{ ucfirst(request()->segment(2) ?? request()->segment(1)) }}
            </h3>
        </div>
        <div class="collapse navbar-collapse justify-content-end" id="navigation">
            <div class="logo d-flex align-items-center">
                <i class="nc-icon nc-single-02"></i>
                <a href="{{ route('profile.edit') }}" class="pl-1 text-decoration-none">
                    {{ ucfirst(auth()->user()->name) }}
                </a>
            </div>
            <ul class="navbar-nav">
                <li class="nav-item btn-rotate dropdown">
                    <a class="nav-link dropdown-toggle text-dark" id="navbarDropdownMenuLink2"
                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="nc-icon nc-settings-gear-65 text-dark"></i>
                    </a>
                    <div aria-labelledby="navbarDropdownMenuLink2">
                        <form class="dropdown-item" action="{{ route('logout') }}" id="formLogOut" method="POST" style="display: none;">
                            @csrf
                        </form>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                            <a href="{{ route('profile.edit') }}" class="dropdown-item">
                                {{ __('Profile') }}
                            </a>
                            <a class="dropdown-item"
                               onclick="document.getElementById('formLogOut').submit();">{{ __('Log out') }}
                            </a>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</nav>
