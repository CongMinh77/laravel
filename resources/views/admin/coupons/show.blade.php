@extends('layouts.admin.app', [
    'class' => '',
    'elementActive' => 'coupons'
])

@section('content')
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card shadow">
                    <div class="card-body">
                        <div class="row">
                            <div>
                                {{ $coupon->name }}
                                {{ $coupon->type }}
                                {{ $coupon->value }}
                                {{ $coupon->expiration_date }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
