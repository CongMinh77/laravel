@extends('layouts.admin.app', [
    'class' => '',
    'elementActive' => 'coupons'
])
@section('content')
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card shadow">
                    <div class="card-header">
                        <h2 class="mb-0 text-dark text-center">
                            Create Coupon
                        </h2>
                    </div>
                    <div class="card-body">
                        <form enctype="multipart/form-data" action="{{ route('coupons.store') }}" method="POST">
                            @csrf
                            <div class="row">
                                <div class="form-group col">
                                    <label class="text-dark">
                                        <h6 class="m-0">Name coupon <span class="text-danger">*</span></h6>
                                    </label>
                                    <input type="text" class="form-control text-uppercase" name="name"
                                           value="{{ old('name') }}"
                                           placeholder="Name">
                                    @error('name')
                                    <span class="alert-danger" role="alert">
                                    {{$message}}
                                </span>
                                    @enderror
                                </div>
                                <div class="form-group col">
                                    <label class="text-dark">
                                        <h6 class="m-0">Type coupon <span class="text-danger">*</span></h6>
                                    </label>
                                    <select class="form-control" name="type">
                                        <option selected>--Type--</option>
                                        @foreach(config('constants.coupon.type') as $value)
                                            <option {{ old('type') == $value ? 'selected' : '' }}
                                                    value="{{ $value }}">
                                                {{ ucfirst($value) }}
                                            </option>
                                        @endforeach
                                    </select>
                                    @error('type')
                                    <span class="alert-danger" role="alert">
                                    {{$message}}
                                </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="row mt-2">
                                <div class="form-group mb-3 col-6">
                                    <label class="text-dark">
                                        <h6 class="m-0">Value <span class="text-danger">*</span></h6>
                                    </label>
                                    <input type="number" class="form-control" name="value"
                                           value="{{ old('value') }}"
                                           placeholder="Value">
                                    @error('value')
                                    <span class="alert-danger" role="alert">
                                        {{$message}}
                                    </span>
                                    @enderror
                                </div>
                                <div class="form-group col-6">
                                    <label class="text-dark">
                                        <h6 class="m-0">Expiration Date <span class="text-danger">*</span></h6>
                                    </label>
                                    <input type="date" class="form-control" name="expiration_date"
                                           value="{{ old('expiration_date') }}"
                                           placeholder="Expiration Date">
                                    @error('expiration_date')
                                    <span class="alert-danger" role="alert">
                                            {{$message}}
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div>
                                <a href="{{ route('coupons.index') }}" class="btn btn-danger">Back</a>
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </form>
                    </div>
                    <div class="card-footer">
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        $('#select-product').select2({
            width: '100%',
            placeholder: "--Select Product--",
        });
    </script>
@endpush
