@extends('layouts.admin.app', [
    'class' => '',
    'elementActive' => 'roles'
])
@section('content')
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card shadow">
                    <div class="card-header">
                        <h2 class="mb-0 text-dark text-center">
                            Create Role
                        </h2>
                    </div>
                    <div class="card-body">
                        <form action="{{ route('roles.store') }}" method="POST">
                            @csrf
                            <div class="form-group mb-3">
                                <label class="text-dark">
                                    <h6 class="m-0">Name <span class="text-danger">*</span></h6>
                                </label>
                                <input type="text" class="form-control" name="display_name"
                                       value="{{ old('display_name') }}" placeholder="Display name ...">
                                @error('display_name')
                                    <span class="alert-danger" role="alert">
                                        {{$message}}
                                    </span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label class="text-dark mb-2">
                                    <h6 class="m-0">Permission</h6>
                                </label>
                                <div class="row">
                                    @foreach($permissions as $group => $permissionGroup)
                                        <div class="col-3">
                                            <div class="container-fluid">
                                                <div class="text-dark font-weight-bold">{{ ucfirst($group) }}</div>
                                                @foreach($permissionGroup as $permission)
                                                    <div class="container pl-0">
                                                        <div class="form-check">
                                                            <label class="form-check-label text-dark font-weight-bold">
                                                                <input class="form-check-input" type="checkbox"
                                                                       name="permission_id[]"
                                                                       id="{{$permission->group_name}}"
                                                                       value="{{$permission->id}}">
                                                                {{$permission->display_name}}
                                                                <span class="form-check-sign">
                                                                <span class="check"></span>
                                                            </span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                            <div>
                                <a href="{{ route('roles.index') }}" class="btn btn-danger">Back</a>
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </form>
                    </div>
                    <div class="card-footer">

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
