@extends('layouts.admin.app', [
    'class' => '',
    'elementActive' => 'roles'
])
@section('content')
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                @if (session('message'))
                    <div id="notification" class="alert alert-success font-weight-bold">
                        {{ session('message') }}
                    </div>
                @endif
                <div class="card shadow">
                    <div class="card-header border-0 pb-2">
                        <div class="row align-items-center">
                            <form method="GET" class="col-11 d-flex justify-content-end">
                                <div class="input-group no-border w-75 m-0">
                                    <input type="text" name="search" value="{{ request('search') ?? '' }}"
                                        class="form-control" placeholder="Search...">
                                    <button class="btn m-0" type="submit">
                                        <div class="input-group-append">
                                            <i class="nc-icon nc-zoom-split"></i>
                                        </div>
                                    </button>
                                </div>
                            </form>
                            <div class="col-1">
                                @hasPermission('role_create')
                                    <a href="{{ route('roles.create') }}"
                                       class="btn btn-sm btn-primary">
                                        <i class="fas fa-plus fa-2x"></i>
                                    </a>
                                @endhasPermission
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table align-items-center table-flush">
                            <thead class="thead-light">
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col" class="w-160">Name</th>
                                <th scope="col" class="text-center">Permission</th>
                                <th scope="col" class="text-center w-160">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($roles as $role)
                                @if ($role->name != 'super-admin')
                                <tr>
                                    <td>{{ $loop->iteration + $roles->firstItem() - 1 }}</td>
                                    <td>{{ $role->display_name }}</td>
                                    <td>
                                        @foreach($role->permissions as $item)
                                            {{ $item->display_name }}{{ $loop->last ? '.' : ',' }}
                                        @endforeach
                                    </td>
                                    <td>
                                        <div class="container">
                                            <div class="d-flex justify-content-center">
                                                <form action="{{ route('roles.destroy', $role->id) }}"
                                                      method="POST" id="form-{{ $role->id }}">
                                                    @csrf
                                                    @method('DELETE')
                                                </form>
                                                @hasPermission('role_delete')
                                                    <button class="btn btn-danger p-2 w-icon"
                                                            onclick="deleteRole({{ $role->id }})">
                                                        <i class="far fa-trash-alt"></i>
                                                    </button>
                                                @endhasPermission
                                                @hasPermission('role_update')
                                                    <a href="{{ route('roles.edit', $role->id) }}"
                                                       class="btn btn-warning ml-2 p-2 w-icon">
                                                        <i class="far fa-edit text-white"></i>
                                                    </a>
                                                @endhasPermission
                                                @hasPermission('role_read')
                                                    <a href="{{ route('roles.show', $role->id) }}"
                                                       class="btn btn-info ml-2 p-2 w-icon">
                                                        <i class="fal fa-info-square"></i>
                                                    </a>
                                                @endhasPermission
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                @endif
                            @endforeach
                            </tbody>
                        </table>
                        <div class="container justify-content-center">
                            {{$roles->appends(request()->all())->links()}}
                        </div>
                    </div>
                    <div class="card-footer py-3">
                        <nav class="d-flex justify-content-end" aria-label="...">
                            @if(count($roles) <= 0)
                                <h6 class="container text-center font-weight-bold mb-0">-- NOT FOUND DATA --</h6>
                            @endif
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        function deleteRole(id) {
            const swalWithBootstrapButtons = Swal.mixin({
                customClass: {
                    confirmButton: 'btn btn-success',
                    cancelButton: 'btn btn-danger mr-3'
                },
                buttonsStyling: false
            })
            swalWithBootstrapButtons.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: 'No, cancel!',
                reverseButtons: true,
            }).then((result) => {
                if (result.isConfirmed) {
                    event.preventDefault();
                    document.getElementById('form-' + id).submit();
                    swalWithBootstrapButtons.fire(
                        'Deleted!',
                        'Your file has been deleted.',
                        'success',
                    )
                } else if (
                    result.dismiss === Swal.DismissReason.cancel
                ) {
                    swalWithBootstrapButtons.fire(
                        'Cancelled',
                        'Your role file is safe :)',
                        'error'
                    )
                }
            })
        }
    </script>
@endpush
