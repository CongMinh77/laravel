@extends('layouts.admin.app', [
    'class' => '',
    'elementActive' => 'roles'
])

@section('content')
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <h3 class="card-title">
                            Name: {{$role->display_name}}
                        </h3>
                        <h6 class="card-collapse ml-4">Permission:</h6>
                        <p class="card-text ml-4">
                            @foreach($role->permissions as $permission)
                                {{$permission->display_name}},
                            @endforeach
                        </p>
                        <div>
                            <a href="{{ route('roles.index') }}" class="btn btn-danger">Back</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
