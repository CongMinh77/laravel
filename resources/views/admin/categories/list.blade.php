<div id="list" data-action="{{ route('categories.list') }}">
    <div class="table-responsive">
        <table class="table align-items-center table-flush">
            <thead class="thead-light">
            <tr>
                <th scope="col">#</th>
                <th scope="col">Name</th>
                <th scope="col">Parent name</th>
                <th scope="col" class="text-center w-160">Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach($categoriesByPage as $category)
                <tr>
                    <td>{{ $loop->iteration + $categoriesByPage->firstItem() - 1 }}</td>
                    <td>{{ $category->name }}</td>
                    <td>
                        @if($category->ParentName != null)
                            {{ $category->ParentName }}
                        @else
                            <h6 class="mb-0 badge badge-danger p-1">NULL</h6>
                        @endif
                    </td>
                    <td>
                        <div class="container">
                            <div class="d-flex justify-content-center">
                                @hasPermission('category_delete')
                                    <button class="btn btn-danger p-2 w-icon btn-delete-category"
                                            id="btn-delete"
                                            data-action="{{ route('categories.destroy', $category->id) }}">
                                        <i class="far fa-trash-alt"></i>
                                    </button>
                                @endhasPermission
                                @hasPermission('category_update')
                                    <a class="btn btn-warning ml-2 p-2 w-icon" data-bs-toggle="modal" id="btn-edit"
                                       data-toggle="modal" data-target="#EditCategory"
                                       data-action="{{ route('categories.edit',$category->id) }}"
                                       data-update="{{ route('categories.update',$category->id) }}">
                                        <i class="far fa-edit text-white"></i>
                                    </a>
                                @endhasPermission
                                @hasPermission('category_read')
                                    <a data-action="{{ route('categories.show', $category->id) }}"
                                       data-toggle="modal" data-target="#ShowCategory" data-bs-toggle="modal"
                                       class="btn btn-info ml-2 p-2 w-icon" id="btn-show">
                                        <i class="fal fa-info-square text-white"></i>
                                    </a>
                                @endhasPermission
                            </div>
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <div class="container justify-content-center">
            {{ $categoriesByPage->appends(request()->all())->links() }}
        </div>
    </div>
    <div class="card-footer py-3">
        <nav class="d-flex justify-content-end" aria-label="...">
            @if( count($categories) <= 0 )
                <h6 class="container text-center font-weight-bold">-- NOT FOUND DATA --</h6>
            @endif
        </nav>
    </div>
</div>
