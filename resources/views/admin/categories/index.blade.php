@extends('layouts.admin.app', [
    'class' => '',
    'elementActive' => 'categories'
])
@section('content')
{{--Create modal--}}
<div class="modal fade" id="CreateCategory" tabindex="-1" role="dialog"
 aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Create category</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="CreateCategory" enctype="multipart/form-data" method="POST">
                @csrf
                <div class="modal-body">
                    <div class="card shadow">
                        <div class="card-body">
                            <div class="form-group">
                                <label class="text-dark">
                                    <h6 class="m-0">Name Category</h6>
                                </label>
                                <input type="text" class="form-control" id="nameCategory"
                                       name="name" placeholder="Name ...">
                            </div>
                            <span class="text-danger name_error_create" id="error"></span>
                            <div class="form-group">
                                <label class="text-dark">
                                    <h6 class="m-0">Parent name</h6>
                                </label>
                                <select class="form-control" id="parentIdCategory" name="parent_id">
                                    <option value="">-- Null --</option>
                                    @foreach($categories as $category)
                                        <option value="{{ $category->id }}">{{ $category->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary mr-3" data-dismiss="modal">
                    Close
                </button>
                <button type="submit" id="btn-create" class="btn btn-primary"
                        data-action="{{ route('categories.store') }}">
                    Submit
                </button>
            </div>
        </div>
    </div>
</div>
{{--Edit Modal--}}
<div class="modal fade" id="EditCategory" tabindex="-1" role="dialog"
 aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Edit category</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="EditCategory" enctype="multipart/form-data" method="POST">
                @csrf
                @method("PUT")
                <div class="modal-body">
                    <div class="card shadow">
                        <div class="card-body">
                            <div class="form-group">
                                <label class="text-dark">
                                    <h6 class="m-0">Name Category</h6>
                                </label>
                                <input type="text" class="form-control" id="nameUpdate"
                                       name="name" placeholder="Name ...">
                            </div>
                            <span class="text-danger name_error_update" id="error"></span>
                            <div class="form-group">
                                <label class="text-dark">
                                    <h6 class="m-0">Parent name</h6>
                                </label>
                                <select class="form-control form-control-lg rounded" id="parentIdUpdate" name="parent_id">
                                    <option value="">-- Null --</option>
                                    @foreach($categories as $category)
                                        <option value="{{ $category->id }}">{{ $category->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary mr-3" data-dismiss="modal">
                    Close
                </button>
                <button type="button" id="btn-update" class="btn btn-primary" autofocus>
                    Save changes
                </button>
            </div>
        </div>
    </div>
</div>
{{--Show Modal--}}
<div class="modal fade" id="ShowCategory" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Show category</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
                <div class="modal-body">
                    <div class="card shadow">
                        <div class="card-body">
                            <div class="form-group row align-items-center">
                                <label class="text-dark col-4 mb-0">
                                    <h6 class="m-0">Name Category</h6>
                                </label>
                                <h4 id="nameShow" class="col-8 p-0 m-0"></h4>
                            </div>
                            <div class="form-group row align-items-center">
                                <label class="text-dark col-4 mb-0 pr-0">
                                    <h6 class="m-0">Parent name</h6>
                                </label>
                                <select disabled class="form-control form-control-lg col-7 mr-2 rounded" id="parentIdShow" name="parent_id">
                                    <option value="">-- Null --</option>
                                    @foreach($categories as $category)
                                        <option value="{{ $category->id }}">{{ $category->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary mr-3" data-dismiss="modal">
                    Close
                </button>
            </div>
        </div>
    </div>
</div>
<div class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card shadow">
                <div class="card-header border-0 pb-2">
                    <div id="success_message" class="m-0 rounded"></div>
                    <div class="row align-items-center">
                        <form method="POST" class="col-11 d-flex justify-content-end">
                            <div class="input-group no-border m-0 w-75">
                                <input type="search" name="search" id="search"
                                       class="form-control" placeholder="Search..."
                                       value="{{ request('search') }}">
                                <button class="btn m-0" id="btn-search">
                                    <div class="input-group-append">
                                        <i class="nc-icon nc-zoom-split"></i>
                                    </div>
                                </button>
                            </div>
                        </form>
                        <div class="col-1 text-right">
                            <a data-bs-toggle="modal" data-toggle="modal"
                               data-target="#CreateCategory" class="btn btn-sm btn-primary">
                                <i class="fas fa-plus text-white fa-2x"></i>
                            </a>
                        </div>
                    </div>
                </div>
                <div id="list" data-action="{{ route('categories.list') }}">
                </div>
            </div>
        </div>
    </div>

</div>
@endsection

@push('scripts')
    <script src="{{ asset('paper/js/admin/category.js') }}"></script>
@endpush

