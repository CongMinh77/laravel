@extends('layouts.admin.app', [
    'class' => '',
    'elementActive' => 'products'
])
@section('content')
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card shadow">
                    <div class="card-header">
                        <h2 class="mb-0 text-dark text-center">
                            Create Product
                        </h2>
                    </div>
                    <div class="card-body">
                        <form enctype="multipart/form-data" action="{{ route('products.store') }}" method="POST">
                            @csrf
                            <div class="row">
                                <div class="form-group col-8">
                                    <label class="text-dark">
                                        <h6 class="m-0">Name product <span class="text-danger">*</span></h6>
                                    </label>
                                    <input type="text" class="form-control" name="name"
                                           value="{{ old('name') }}"
                                           placeholder="Name">
                                    @error('name')
                                    <span class="alert-danger" role="alert">
                                        {{$message}}
                                    </span>
                                    @enderror
                                </div>
                                <div class="form-group mb-3 col-4">
                                    <label class="text-dark">
                                        <h6 class="m-0">Price <span class="text-danger">*</span></h6>
                                    </label>
                                    <input type="number" class="form-control" name="price" min="1"
                                           value="{{ old('price') }}"
                                           placeholder="Price">
                                    @error('price')
                                    <span class="alert-danger" role="alert">
                                        {{$message}}
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="text-dark">
                                    <h6 class="m-0">Category <span class="text-danger">*</span></h6>
                                </label>
                                @if(old('category_id'))

                                @endif
                                <select class="form-control form-control-lg rounded" id="select-category"
                                        name="category_id[]" multiple>
                                    @foreach($categories as $category)
                                        <option
                                            @if(old('category_id'))
                                                @foreach(old('category_id') as $categoryId)
                                                    {{ $categoryId == $category->id ? 'selected' : '' }}
                                                @endforeach
                                            @endif
                                                class="text-dark" value="{{ $category->id }}">
                                            {{ $category->name }}
                                        </option>
                                    @endforeach
                                </select>
                                @error('category_id')
                                    <span class="alert-danger" role="alert">
                                        {{$message}}
                                    </span>
                                @enderror
                            </div>
                            <div class="form-group mb-3">
                                <label class="text-dark">
                                    <h6 class="m-0">Description <span class="text-danger">*</span></h6>
                                </label>
                                <textarea class="form-control" rows="3" name="description">
                                    {!! old('description') !!}
                                </textarea>
                                @error('description')
                                    <span class="alert-danger" role="alert">
                                        {{$message}}
                                    </span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <div class="form-group">
                                    <label class="text-dark">
                                        <h6 class="m-0">Image</h6>
                                    </label>
                                    <input type="file" class="form-control" id="image"
                                           value="{{ old('image') }}" name="image">
                                        Choose file
                                </div>
                                <div class="form-group">
                                    <div style="width: 10rem; height: 10rem">
                                        <img id="showImage" alt="" style="width: 10rem; height: 10rem">
                                    </div>
                                </div>
                            </div>
                            <div>
                                <a href="{{ route('products.index') }}" class="btn btn-danger">Back</a>
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </form>
                    </div>
                    <div class="card-footer">
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script type="text/javascript">
        $('#select-category').select2({
            width: '100%',
            placeholder: "--Select Category--",
        });

        $(document).ready(function () {
            $('#image').change(function (e) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#showImage').attr('src', e.target.result);
                }
                reader.readAsDataURL(e.target.files['0']);
            })
        })
    </script>
@endpush
