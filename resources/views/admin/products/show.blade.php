@extends('layouts.admin.app', [
    'class' => '',
    'elementActive' => 'products'
])

@section('content')
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card shadow">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-6">
                                <img class="border border-secondary"
                                     src="{{ asset($product->image) }}" alt="{{ $product->image }}">
                            </div>
                            <div class="col-6">
                                <h3 class="mb-0">{{ $product->name }}</h3>
                                <h4 class="m-0 pt-2">${{ number_format($product->price, 0, '', '.') }}</h4>
                                <div class="container-fluid bg-light p-0" style="height: 1px;"></div>
                                <p class="description pt-2 text-dark">{{ $product->description }}</p>
                                <div>
                                    Quantity: {{ $amountInStock }}
                                </div>
                            </div>
                            <div class="col-12">
                                <a href="{{ route('products.index') }}" class="btn btn-danger">Back</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
