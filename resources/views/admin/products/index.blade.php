@extends('layouts.admin.app', [
    'class' => '',
    'elementActive' => 'products'
])
@section('content')
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                @if (session('message'))
                    <div id="notification" class="alert alert-success font-weight-bold">
                        {{ session('message') }}
                    </div>
                @endif
                <div class="card shadow">
                    <div class="card-header border-0 pb-2">
                        <div class="row align-items-center">
                            <form method="GET" class="col-11 d-flex justify-content-end">
                                <div class="w-75 d-flex">
                                    <select name="category" class=" form-control col-md-3 pl-1 mr-2">
                                        <option value="">All Categories</option>
                                        @foreach($categories as $category)
                                            <option {{ $category->name == request('category') ? 'selected' : '' }}
                                                    value="{{ old('category') ?? $category->name }}">
                                                {{ $category->name }}
                                            </option>
                                        @endforeach
                                    </select>
                                    <div class="input-group no-border m-0">
                                        <input type="text" name="search" value="{{ request('search') ?? '' }}"
                                               class="form-control"
                                               placeholder="Search...">
                                        <button class="btn m-0" type="submit">
                                            <div class="input-group-append">
                                                <i class="nc-icon nc-zoom-split"></i>
                                            </div>
                                        </button>
                                    </div>
                                </div>
                            </form>
                            <div class="col-1 text-right">
                                @hasPermission('product_create')
                                    <a href="{{ route('products.create') }}" class="btn btn-sm btn-primary">
                                        <i class="fas fa-plus fa-2x"></i>
                                    </a>
                                @endhasPermission
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table align-items-center table-flush">
                            <thead class="thead-light">
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Name</th>
                                <th scope="col">Price</th>
                                <th scope="col">Category</th>
                                <th scope="col">Description</th>
                                <th scope="col" class="text-center w-160">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($products as $product)
                                <tr>
                                    <td>{{ $loop->iteration + $products->firstItem() - 1 }}</td>
                                    <td class="w-160">{{ $product->name }}</td>
                                    <td>
                                        <h6>
                                            {{ number_format($product->price, 0, '', '.') }}<sup>₫</sup>
                                        </h6>
                                    </td>
                                    <td>
                                        @foreach($product->categories as $category_item)
                                            <small class="text-danger font-weight-bold">
                                                {{ ucfirst($category_item->name) }}
                                            </small>
                                            {{ $loop->last ? '.' : ',' }}
                                        @endforeach
                                    </td>
                                    <td class="text-break-word">
                                        {!! Str::limit(strip_tags($product->description) , $limit = 100, $end = '...') !!}
                                    </td>
                                    <td>
                                        <div class="container">
                                            <div class="d-flex justify-content-center">
                                                <form action="{{ route('products.destroy', $product->id) }}"
                                                      method="POST" id="form-{{ $product->id }}">
                                                    @csrf
                                                    @method('DELETE')
                                                </form>
                                                @hasPermission('product_delete')
                                                <button class="btn btn-danger p-2 w-icon"
                                                        onclick="deleteProduct({{ $product->id }})">
                                                    <i class="far fa-trash-alt"></i>
                                                </button>
                                                @endhasPermission
                                                @hasPermission('product_update')
                                                    <a href="{{ route('products.edit', $product->id) }}"
                                                       class="btn btn-warning ml-2 p-2 w-icon">
                                                        <i class="far fa-edit text-white"></i>
                                                    </a>
                                                @endhasPermission
                                                @hasPermission('product_read')
                                                    <a href="{{ route('products.show', $product->id) }}"
                                                       class="btn btn-info ml-2 p-2 w-icon">
                                                        <i class="fal fa-info-square"></i>
                                                    </a>
                                                @endhasPermission
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <div class="container justify-content-center">
                            {{ $products->appends(request()->all())->links() }}
                        </div>
                    </div>
                    <div class="card-footer py-3">
                        <nav class="d-flex justify-content-end" aria-label="...">
                            @if( count($products) <= 0 )
                                <h6 class="container text-center font-weight-bold mb-0">-- NOT FOUND DATA --</h6>
                            @endif
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        function deleteProduct(id) {
            const swalWithBootstrapButtons = Swal.mixin({
                customClass: {
                    confirmButton: 'btn btn-success',
                    cancelButton: 'btn btn-danger mr-3'
                },
                buttonsStyling: false
            })
            swalWithBootstrapButtons.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: 'No, cancel!',
                reverseButtons: true,
            }).then((result) => {
                if (result.isConfirmed) {
                    event.preventDefault();
                    document.getElementById('form-' + id).submit();
                    swalWithBootstrapButtons.fire(
                        'Deleted!',
                        'Your file has been deleted.',
                        'success',
                    )
                } else if (
                    result.dismiss === Swal.DismissReason.cancel
                ) {
                    swalWithBootstrapButtons.fire(
                        'Cancelled',
                        'Your product file is safe :)',
                        'error'
                    )
                }
            })
        }
    </script>
@endpush

