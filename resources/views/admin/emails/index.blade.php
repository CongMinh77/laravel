@extends('layouts.admin.app', [
    'class' => '',
    'elementActive' => 'emails'
])
@section('content')
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                @if (session('message'))
                    <div id="notification" class="alert alert-success font-weight-bold">
                        {{ session('message') }}
                    </div>
                @endif
                <div class="card shadow">
                    <div class="card-header border-0 pb-3">
                        <div class="row align-items-center">
                            <form method="GET" class="col-12 d-flex justify-content-end">
                                <div class="w-75 d-flex">
                                    <div class="input-group no-border m-0">
                                        <input type="text" name="search" value="{{ request('search') ?? '' }}"
                                               class="form-control"
                                               placeholder="Search...">
                                        <button class="btn m-0" type="submit">
                                            <div class="input-group-append">
                                                <i class="nc-icon nc-zoom-split"></i>
                                            </div>
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table align-items-center table-flush">
                            <thead class="thead-light">
                            <tr>
                                <th scope="col" class="w-70">#</th>
                                <th scope="col" class="text-center">Email</th>
                                <th scope="col" class="text-center w-160">Status</th>
                                <th scope="col" class="text-center w-160">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($emails as $email)
                                <tr>
                                    <td>{{ $loop->iteration + $emails->firstItem() - 1 }}</td>
                                    <td class="">{{ $email->email }}</td>
                                    <td class="text-center w-160">
                                        <span class="badge {{ $email->status == 1 ? 'badge-success' : 'badge-danger' }} p-2">
                                            {{ $email->status == 1 ? 'Registered' : 'Cancel' }}
                                        </span>
                                    </td>
                                    <td>
                                        <div class="container">
                                            <div class="d-flex justify-content-center">
                                                <form action="{{ route('emails.destroy', $email->id) }}"
                                                      method="POST" id="form-{{ $email->id }}">
                                                    @csrf
                                                    @method('DELETE')
                                                </form>
                                                @hasPermission('stock_delete')
                                                <button class="btn btn-danger p-2 w-icon" id="btn-delete"
                                                        onclick="deleteEmail({{ $email->id }})"
                                                        data-action="{{ route('emails.destroy', $email->id) }}">
                                                    <i class="far fa-trash-alt"></i>
                                                </button>
                                                @endhasPermission
{{--                                                @hasPermission('stock_update')--}}
{{--                                                    <a href="{{ route('emails.edit', $email->id) }}"--}}
{{--                                                       class="btn btn-warning ml-2 p-2 w-icon">--}}
{{--                                                        <i class="far fa-edit text-white"></i>--}}
{{--                                                    </a>--}}
{{--                                                @endhasPermission--}}
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <div class="container justify-content-center">
                             {{ $emails->appends(request()->all())->links() }}
                        </div>
                    </div>
                    <div class="card-footer py-3">
                        <nav class="d-flex justify-content-end" aria-label="...">
                            @if( count($emails) <= 0 )
                                <h6 class="container text-center font-weight-bold mb-0">-- NOT FOUND DATA --</h6>
                            @endif
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        function deleteEmail(id) {
            const swalWithBootstrapButtons = Swal.mixin({
                customClass: {
                    confirmButton: 'btn btn-success',
                    cancelButton: 'btn btn-danger mr-3'
                },
                buttonsStyling: false
            })
            swalWithBootstrapButtons.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: 'No, cancel!',
                reverseButtons: true,
            }).then((result) => {
                if (result.isConfirmed) {
                    event.preventDefault();
                    document.getElementById('form-' + id).submit();
                    swalWithBootstrapButtons.fire(
                        'Deleted!',
                        'Your file has been deleted.',
                        'success',
                    )
                } else if (
                    result.dismiss === Swal.DismissReason.cancel
                ) {
                    swalWithBootstrapButtons.fire(
                        'Cancelled',
                        'Your email file is safe :)',
                        'error'
                    )
                }
            })
        }
    </script>
@endpush

