@extends('layouts.admin.app', [
    'class' => '',
    'elementActive' => 'stocks'
])
@section('content')
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card shadow">
                    <div class="card-header">
                        <h2 class="mb-0 text-dark text-center">
                            Edit Stock
                        </h2>
                    </div>
                    <div class="card-body">
                        <form enctype="multipart/form-data" action="{{ route('stocks.update', $stock->id) }}" method="POST">
                            @csrf
                            @method('PUT')
                            <div class="form-group">
                                <div class="d-flex">
                                    <div class="form-check form-check-inline d-flex align-items-center">
                                        <input class="form-check-input" type="radio" name="status" checked
                                               id="inlineRadio1" value="{{ $stock->status == 1 ? config('constants.stock.status.import.key') : config('constants.stock.status.export.key')}}">
                                        <label class="text-dark m-0">
                                            <h6 class="m-0">
                                                {{ $stock->status == 1 ? config('constants.stock.status.import.name') : config('constants.stock.status.export.name')}}
                                            </h6>
                                        </label>
                                    </div>
                                </div>
                                @error('status')
                                    <span class="alert-danger" role="alert">
                                        {{$message}}
                                    </span>
                                @enderror
                            </div>
                            <div class="row mt-2">
                                <div class="form-group col-8">
                                    <label class="text-dark">
                                        <h6 class="m-0">Name product <span class="text-danger">*</span></h6>
                                    </label>
                                    <select name="product_id" class="form-control pl-1 mr-2" id="select-product">
                                        <option value=""></option>
                                        @foreach($products as $product)
                                            <option {{ old('product_id', $stock->product_id) == $product->id ? 'selected' : '' }}
                                                    value="{{ $product->id }}">
                                                {{ $product->name }}
                                            </option>
                                        @endforeach
                                    </select>
                                    @error('product_id')
                                        <span class="alert-danger" role="alert">
                                            {{$message}}
                                        </span>
                                    @enderror
                                </div>
                                <div class="form-group mb-3 col-4">
                                    <label class="text-dark">
                                        <h6 class="m-0">Amount <span class="text-danger">*</span></h6>
                                    </label>
                                    <input type="number" class="form-control" name="amount"
                                           value="{{ old('amount', $stock->amount) }}"
                                           placeholder="Amount">
                                    @error('amount')
                                    <span class="alert-danger" role="alert">
                                        {{$message}}
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group mb-3">
                                <label class="text-dark">
                                    <h6 class="m-0">Note</h6>
                                </label>
                                <textarea class="form-control" rows="3" name="note">{!! old('note', $stock->note) !!}</textarea>
                            </div>
                            <div>
                                <a href="{{ route('stocks.index') }}" class="btn btn-danger">Back</a>
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </form>
                    </div>
                    <div class="card-footer">
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        $('#select-product').select2({
            width: '100%',
            placeholder: "--Select Product--",
        });
    </script>
@endpush
