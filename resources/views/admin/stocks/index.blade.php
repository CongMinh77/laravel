@extends('layouts.admin.app', [
    'class' => '',
    'elementActive' => 'stocks'
])
@section('content')
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                @if (session('message'))
                    <div id="notification" class="alert alert-success font-weight-bold">
                        {{ session('message') }}
                    </div>
                @endif
                <div class="card shadow">
                    <div class="card-header border-0 pb-2">
                        <div class="row align-items-center">
                            <form method="GET" class="col-11 d-flex justify-content-end">
                                <div class="w-75 d-flex">
                                    <div class="input-group no-border m-0">
                                        <input type="text" name="search" value="{{ request('search') ?? '' }}"
                                               class="form-control"
                                               placeholder="Search...">
                                        <button class="btn m-0" type="submit">
                                            <div class="input-group-append">
                                                <i class="nc-icon nc-zoom-split"></i>
                                            </div>
                                        </button>
                                    </div>
                                </div>
                            </form>
                            <div class="col-1 text-right">
                                @hasPermission('stock_create')
                                    <a href="{{ route('stocks.create') }}" class="btn btn-sm btn-primary">
                                        <i class="fas fa-plus fa-2x"></i>
                                    </a>
                                @endhasPermission
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table align-items-center table-flush">
                            <thead class="thead-light">
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Product</th>
                                <th scope="col">Amount</th>
                                <th scope="col" class="text-center">Status</th>
                                <th scope="col" class="text-center">Time</th>
                                <th scope="col" class="text-center">Note</th>
                                <th scope="col" class="text-center w-160">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($stocks as $stock)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $stock->products->name }}</td>
                                    <td class="w-70">
                                        {{ number_format($stock->amount, 0, '', '.') }}
                                    </td>
                                    <td class="w-70 text-center">
                                        @foreach(config('constants.stock.status') as $key => $name)
                                            @if (config('constants.stock.status.'.$key.'.key') == $stock->status)
                                                <span class="badge {{ $stock->status == 1 ? 'badge-success' : 'badge-danger' }} p-2">
                                                    {{ config('constants.stock.status.'.$key.'.name') }}
                                                </span>
                                            @endif
                                        @endforeach
                                    </td>
                                    <td class="w-160 text-center">{{ $stock->created_at }}</td>
                                    <td>{{ $stock->note }}</td>
                                    <td>
                                        <div class="container">
                                            <div class="d-flex justify-content-center">
                                                <form action="{{ route('stocks.destroy', $stock->id) }}"
                                                      method="POST" id="form-{{ $stock->id }}">
                                                    @csrf
                                                    @method('DELETE')
                                                </form>
                                                @hasPermission('stock_delete')
                                                <button class="btn btn-danger p-2 w-icon"
                                                        onclick="deleteStock({{ $stock->id }})">
                                                    <i class="far fa-trash-alt"></i>
                                                </button>
                                                @endhasPermission
                                                @hasPermission('stock_update')
                                                    <a href="{{ route('stocks.edit', $stock->id) }}"
                                                       class="btn btn-warning ml-2 p-2 w-icon">
                                                        <i class="far fa-edit text-white"></i>
                                                    </a>
                                                @endhasPermission
                                                @hasPermission('stock_read')
                                                    <a href="{{ route('stocks.show', $stock->id) }}"
                                                       class="btn btn-info ml-2 p-2 w-icon">
                                                        <i class="fal fa-info-square"></i>
                                                    </a>
                                                @endhasPermission
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <div class="container justify-content-center">
                             {{ $stocks->appends(request()->all())->links() }}
                        </div>
                    </div>
                    <div class="card-footer py-3">
                        <nav class="d-flex justify-content-end" aria-label="...">
                            @if( count($stocks) <= 0 )
                                <h6 class="container text-center font-weight-bold mb-0">-- NOT FOUND DATA --</h6>
                            @endif
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        function deleteStock(id) {
            const swalWithBootstrapButtons = Swal.mixin({
                customClass: {
                    confirmButton: 'btn btn-success',
                    cancelButton: 'btn btn-danger mr-3'
                },
                buttonsStyling: false
            })
            swalWithBootstrapButtons.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: 'No, cancel!',
                reverseButtons: true,
            }).then((result) => {
                if (result.isConfirmed) {
                    event.preventDefault();
                    document.getElementById('form-' + id).submit();
                    swalWithBootstrapButtons.fire(
                        'Deleted!',
                        'Your file has been deleted.',
                        'success',
                    )
                } else if (
                    result.dismiss === Swal.DismissReason.cancel
                ) {
                    swalWithBootstrapButtons.fire(
                        'Cancelled',
                        'Your product file is safe :)',
                        'error'
                    )
                }
            })
        }
    </script>
@endpush

