@extends('layouts.admin.app', [
    'class' => '',
    'elementActive' => 'stocks'
])

@section('content')
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card shadow">
                    <div class="card-body">
                        <div class="row">
                            <div>
                                {{ $stock[0]->products->name }}
                                {{ $stock[0]->amount }}
                                {{ $stock[0]->note }}
                                {{ $stock[0]->status }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
