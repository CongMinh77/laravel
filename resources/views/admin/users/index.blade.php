@extends('layouts.admin.app', [
    'class' => '',
    'elementActive' => 'users'
])
@section('content')
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                @if (session('message'))
                    <div id="notification" class="alert alert-success font-weight-bold">
                        {{ session('message') }}
                    </div>
                @endif
                <div class="card shadow">
                    <div class="card-header border-0 pb-2">
                        <div class="row align-items-center">
                            <form method="GET" class="col-11 d-flex justify-content-end">
                                <div class="w-75 form-group d-flex m-0">
                                    <select name="role" class="form-control col-md-3 pl-1 mr-2">
                                        <option value=" " selected>All Roles</option>
                                        @foreach($rolesWithoutSuperAdmin as $role)
                                            <option {{ $role->name == request('role') ? 'selected' : '' }}
                                                    value="{{ $role->name }}">
                                                {{ $role->display_name }}
                                            </option>
                                        @endforeach
                                    </select>
                                    <div class="input-group no-border m-0">
                                        <input type="text" name="search" value="{{ request('search') ?? '' }}"
                                               class="form-control" placeholder="Search...">
                                        <button class="btn m-0" type="submit">
                                            <div class="input-group-append">
                                                <i class="nc-icon nc-zoom-split"></i>
                                            </div>
                                        </button>
                                    </div>
                                </div>
                            </form>
                            <div class="col-1">
                                @hasPermission('user_create')
                                    <a href="{{route('users.create')}}" class="btn btn-sm btn-primary">
                                        <i class="fas fa-user-plus fa-2x"></i>
                                    </a>
                                @endhasPermission
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table align-items-center table-flush">
                            <thead class="thead-light">
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col" class="ml-1">Name</th>
                                    <th scope="col">Email</th>
                                    <th scope="col">Address</th>
                                    <th scope="col" class="text-center">Role</th>
                                    <th scope="col" class="text-center w-160">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($users as $user)
                                    <tr>
                                        <td>{{ $loop->iteration + $users->firstItem() - 1 }}</td>
                                        <td>{{ $user->name }}</td>
                                        <td>{{ $user->email }}</td>
                                        <td>{{ $user->address }}</td>
                                        <td>
                                            @foreach($user->roles as $role)
                                                <small class="text-success font-weight-bold">
                                                    {{$role->display_name}}
                                                </small>
                                                {{ $loop->last ? '' : ',' }}
                                            @endforeach
                                        </td>
                                        <td>
                                            <div class="container">
                                                <div class="d-flex justify-content-center">
                                                    <form action="{{ route('users.destroy', $user->id) }}"
                                                          method="POST" id="form-{{ $user->id }}">
                                                        @csrf
                                                        @method('DELETE')
                                                    </form>
                                                    @hasPermission('user_delete')
                                                        <button class="btn btn-danger p-2 w-icon"
                                                                onclick="deleteUser({{ $user->id }})">
                                                            <i class="far fa-trash-alt"></i>
                                                        </button>
                                                    @endhasPermission
                                                    @hasPermission('user_update')
                                                        <a href="{{ route('users.edit', $user->id) }}"
                                                           class="btn btn-warning ml-2 p-2 w-icon">
                                                            <i class="far fa-edit"></i>
                                                        </a>
                                                    @endhasPermission
                                                    @hasPermission('user_read')
                                                        <a href="{{ route('users.show', $user->id) }}"
                                                           class="btn btn-info ml-2 p-2 w-icon">
                                                            <i class="fal fa-info-square"></i>
                                                        </a>
                                                    @endhasPermission
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <div class="container justify-content-center">
                            {{$users->appends(request()->all())->links()}}
                        </div>
                    </div>
                    <div class="card-footer py-3">
                        <nav class="d-flex justify-content-end" aria-label="...">
                            @if( count($users) <= 0 )
                                <h6 class="container text-center font-weight-bold mb-0">-- NOT FOUND DATA --</h6>
                            @endif
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        function deleteUser(id) {
            const swalWithBootstrapButtons = Swal.mixin({
                customClass: {
                    confirmButton: 'btn btn-success',
                    cancelButton: 'btn btn-danger mr-3'
                },
                buttonsStyling: false
            })
            swalWithBootstrapButtons.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: 'No, cancel!',
                reverseButtons: true,
            }).then((result) => {
                if (result.isConfirmed) {
                    event.preventDefault();
                    document.getElementById('form-' + id).submit();
                    swalWithBootstrapButtons.fire(
                        'Deleted!',
                        'Your file has been deleted.',
                        'success',
                    )
                } else if (
                    result.dismiss === Swal.DismissReason.cancel
                ) {
                    swalWithBootstrapButtons.fire(
                        'Cancelled',
                        'Your user file is safe :)',
                        'error'
                    )
                }
            })
        }
    </script>
@endpush
