@extends('layouts.admin.app', [
    'class' => '',
    'elementActive' => 'users'
])
@section('content')
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card shadow">
                    <div class="card-header">
                        <h2 class="mb-0 text-dark text-center">
                            Edit User
                        </h2>
                    </div>
                    <div class="card-body">
                        <form method="POST" action="{{ route('users.update', $user->id) }}">
                            @csrf
                            @method("PUT")
                            <div class="container-fluid p-0">
                                <div class="row">
                                    <div class="form-group col">
                                        <label class="text-dark">
                                            <h6 class="mb-0">Full name <span class="text-danger">*</span></h6>
                                        </label>
                                        <input type="text" class="form-control" name="name" placeholder="Name"
                                               value="{{ old('name', $user->name) }}">
                                        @error('name')
                                        <span class="alert-danger" role="alert">
                                                {{$message}}
                                            </span>
                                        @enderror
                                    </div>
                                    <div class="form-group col">
                                        <label class="text-dark">
                                            <h6 class="mb-0">Phone <span class="text-danger">*</span></h6>
                                        </label>
                                        <input type="text" class="form-control" name="phone"
                                               value="{{ old('phone', $user->phone) }}" placeholder="Phone">
                                        @error('phone')
                                        <span class="alert-danger" role="alert">
                                                {{$message}}
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="container p-0">
                                <div class="row">
                                    <div class="form-group col">
                                        <label class="text-dark">
                                            <h6 class="mb-0">Email <span class="text-danger">*</span></h6>
                                        </label>
                                        <input type="email" class="form-control" value="{{ $user->email }}" name="email" placeholder="Email ...">
                                        @error('email')
                                        <span class="alert-danger" role="alert">
                                    {{$message}}
                                </span>
                                        @enderror
                                    </div>
                                    <div class="form-group col">
                                        <label class="text-dark">
                                            <h6 class="mb-0">Address <span class="text-danger">*</span></h6>
                                        </label>
                                        <input type="text" class="form-control" value="{{ $user->address }}" name="address" placeholder="Address">
                                        @error('address')
                                        <span class="alert-danger" role="alert">
                                    {{$message}}
                                </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="text-dark">
                                    <h6 class="mb-0">Role</h6>
                                </label>
                                <div class="row row-cols-3 pl-3 pr-3">
                                    @foreach($rolesWithoutSuperAdmin as $role)
                                        <div class="form-check col-4">
                                            <label class="form-check-label text-dark font-weight-bold">
                                                <input class="form-check-input"
                                                       {{ $roleIds->contains($role->id) ? 'checked' : '' }}
                                                       type="checkbox"
                                                       name="role_id[]" value="{{$role->id}}">
                                                {{$role->display_name}}
                                                <span class="form-check-sign">
                                                    <span class="check"></span>
                                                </span>
                                            </label>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                            <div>
                                <a href="{{ route('users.index') }}" class="btn btn-danger">Back</a>
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </form>
                    </div>
                    <div class="card-footer">

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
