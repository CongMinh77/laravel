@extends('layouts.admin.app', [
    'class' => '',
    'elementActive' => 'users'
])

@section('content')
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-user">
                    <div class="image">
                        <img src="{{ asset('paper/img/damir-bosnjak.jpg') }}" alt="...">
                    </div>
                    <div class="card-body">
                        <div class="author">
                            <a href="{{ route('users.edit', $user->id) }}">
                                <img class="avatar border border-white" src="{{ asset('paper/img/default-avatar.png') }}"
                                     alt="{{ $user->name }}">
                                <h5 class="title">{{ $user->name }}</h5>
                            </a>
                        </div>
                        <div class="container card">
                            <div class="d-flex justify-content-center">
                                <div class="container ml-5">
                                    <div class="form-group d-flex align-items-center">
                                        <h6 class="description pr-2 m-0 text-dark">
                                            Email:
                                        </h6>
                                        <p class="m-0">
                                            {{ $user->email }}
                                        </p>
                                    </div>
                                    <div class="form-group d-flex align-items-center">
                                        <h6 class="description pr-2 m-0 text-dark">
                                            Address:
                                        </h6>
                                        <p class="m-0">{{ $user->address }}</p>
                                    </div>
                                    <div class="form-group d-flex align-items-center">
                                        <h6 class="description pr-2 m-0 text-dark">
                                            Phone:
                                        </h6>
                                        <p class="m-0">{{ $user->phone }}</p>
                                    </div>
                                </div>
                                <div class="container d-flex">
                                    <p class="description text-dark font-italic">
                                        "{{ __('I like the way you work it') }}
                                        <br> {{ __('No diggity') }}
                                        <br> {{ __('I wanna bag it up') }}"
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div>
                            <a href="{{ route('users.index') }}" class="btn btn-danger">Back</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
