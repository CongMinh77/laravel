@extends('layouts.admin.app', [
    'class' => '',
    'elementActive' => 'ships'
])
@section('content')
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                @if (session('message'))
                    <div id="notification" class="alert alert-success font-weight-bold">
                        {{ session('message') }}
                    </div>
                @endif
                <div class="card shadow">
                    <div class="card-header border-0 pb-2">
                        <div class="row align-items-center">
                            <form method="GET" class="col-11 d-flex justify-content-end">
                                <div class="w-75 d-flex">
                                    <div class="input-group no-border m-0">
                                        <input type="text" name="search" value="{{ request('search') ?? '' }}"
                                               class="form-control"
                                               placeholder="Search...">
                                        <button class="btn m-0" type="submit">
                                            <div class="input-group-append">
                                                <i class="nc-icon nc-zoom-split"></i>
                                            </div>
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table align-items-center table-flush">
                            <thead class="thead-light">
                            <tr>
                                <th scope="col">#</th>
                            </tr>
                            </thead>
                            <tbody>
{{--                            @foreach($coupons as $coupon)--}}
{{--                                <tr>--}}
{{--                                    <td>{{ $loop->iteration + $coupons->firstItem() - 1 }}</td>--}}
{{--                                    <td class="text-uppercase">{{ $coupon->name }}</td>--}}
{{--                                    <td class="w-70 w-160">--}}
{{--                                        {{ number_format($coupon->value, 0, '', '.') }}--}}
{{--                                    </td>--}}
{{--                                    <td class="text-center">{{ ucfirst($coupon->type) }}</td>--}}
{{--                                    <td class="text-center w-160">{{ date('Y-m-d', strtotime($coupon->expiration_date)) }}</td>--}}
{{--                                    <td>--}}
{{--                                        <div class="container">--}}
{{--                                            <div class="d-flex justify-content-center">--}}
{{--                                                <form action="{{ route('coupons.destroy', $coupon->id) }}"--}}
{{--                                                      method="POST" id="form-{{ $coupon->id }}">--}}
{{--                                                    @csrf--}}
{{--                                                    @method('DELETE')--}}
{{--                                                </form>--}}
{{--                                                @hasPermission('stock_delete')--}}
{{--                                                <button class="btn btn-danger p-2 w-icon" id="btn-delete"--}}
{{--                                                        onclick="deleteCoupon({{ $coupon->id }})"--}}
{{--                                                        data-action="{{ route('coupons.destroy', $coupon->id) }}">--}}
{{--                                                    <i class="far fa-trash-alt"></i>--}}
{{--                                                </button>--}}
{{--                                                @endhasPermission--}}
{{--                                                @hasPermission('stock_update')--}}
{{--                                                    <a href="{{ route('coupons.edit', $coupon->id) }}"--}}
{{--                                                       class="btn btn-warning ml-2 p-2 w-icon">--}}
{{--                                                        <i class="far fa-edit text-white"></i>--}}
{{--                                                    </a>--}}
{{--                                                @endhasPermission--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                    </td>--}}
{{--                                </tr>--}}
{{--                            @endforeach--}}
                            </tbody>
                        </table>
                        <div class="container justify-content-center">
{{--                             {{ $coupons->appends(request()->all())->links() }}--}}
                        </div>
                    </div>
                    <div class="card-footer py-3">
                        <nav class="d-flex justify-content-end" aria-label="...">
{{--                            @if( count($coupons) <= 0 )--}}
                                <h6 class="container text-center font-weight-bold mb-0">-- NOT FOUND DATA --</h6>
{{--                            @endif--}}
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        function deleteCoupon(id) {
            const swalWithBootstrapButtons = Swal.mixin({
                customClass: {
                    confirmButton: 'btn btn-success',
                    cancelButton: 'btn btn-danger mr-3'
                },
                buttonsStyling: false
            })
            swalWithBootstrapButtons.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: 'No, cancel!',
                reverseButtons: true,
            }).then((result) => {
                if (result.isConfirmed) {
                    event.preventDefault();
                    document.getElementById('form-' + id).submit();
                    swalWithBootstrapButtons.fire(
                        'Deleted!',
                        'Your file has been deleted.',
                        'success',
                    )
                } else if (
                    result.dismiss === Swal.DismissReason.cancel
                ) {
                    swalWithBootstrapButtons.fire(
                        'Cancelled',
                        'Your product file is safe :)',
                        'error'
                    )
                }
            })
        }
    </script>
@endpush

