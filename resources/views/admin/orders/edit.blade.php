@extends('layouts.admin.app', [
    'class' => '',
    'elementActive' => 'orders'
])
@section('content')
    <div class="content">
        <div class="card shadow">
            <div class="container mt-2 mb-2">
                <form action="{{ route('orders.update', $order->id) }}" class="row" method="POST">
                    @csrf
                    @method('PUT')
                    <div class="col-7 card-user">
                        <div class="image">
                            <img src="{{ asset('paper/img/damir-bosnjak.jpg') }}" alt="...">
                        </div>
                        <div class="card-body">
                            <div class="author">
                                <a>
                                    <img class="avatar border border-white" src="{{ asset('paper/img/default-avatar.png') }}"
                                         alt="{{ $order->customer_name }}">
                                    <h5 class="title">{{ $order->customer_name }}</h5>
                                </a>
                            </div>
                            <div class="container card">
                                <div class="d-flex justify-content-center">
                                    <div class="container">
                                        <div class="form-group d-flex align-items-center">
                                            <h6 class="description pr-2 m-0 text-dark">
                                                Email:
                                            </h6>
                                            <p class="m-0">
                                                {{ $order->customer_email }}
                                            </p>
                                        </div>
                                        <div class="form-group d-flex align-items-center">
                                            <h6 class="description pr-2 m-0 text-dark">
                                                Address:
                                            </h6>
                                            <p class="m-0">{{ $order->customer_address }}</p>
                                        </div>
                                    </div>
                                    <div class="container">
                                        <div class="form-group d-flex align-items-center">
                                            <h6 class="description pr-2 m-0 text-dark">
                                                Phone:
                                            </h6>
                                            <p class="m-0">
                                                {{ $order->customer_phone }}
                                            </p>
                                        </div>
                                        <div class="form-group d-flex align-items-center">
                                            <h6 class="description pr-2 m-0 text-dark">
                                                Note:
                                            </h6>
                                            <p class="m-0">{{ $order->customer_note }}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-5">
                        <h3 class="mb-1">
                            Customer Order
                        </h3>
                        @foreach($orderDetail as $orderProduct)
                            <div class="col-12 d-flex align-items-center rounded p-2 element-product-cart border-bottom">
                                <input type="hidden" name="product_id[]" value="{{ $orderProduct->products->id }}">
                                <img class="img-cart" src="{{ asset($orderProduct->products->image) }}" alt="product">
                                <div class="pl-2 pr-2 text-break-word flex-grow-1">
                                    {{ $orderProduct->products->name }}
                                </div>
                                <div class="mr-2">
                                    <small>x</small> {{ $orderProduct->quantity }}
                                    <input type="hidden" name="quantity[]" value="{{ $orderProduct->quantity }}">
                                </div>
                            </div>
                        @endforeach
                        <select class="form-control mt-3" name="status">
                            @foreach(config('constants.order') as $key => $stage)
                                <option value="{{ config('constants.order.'.$key) }}"
                                    {{ $order->status == config('constants.order.'.$key) ? 'selected' : '' }}>
                                    {{ ucfirst($key) }}
                                </option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-12">
                        <a href="{{ route('orders.index') }}" class="btn btn-danger">Back</a>
                        <button class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
