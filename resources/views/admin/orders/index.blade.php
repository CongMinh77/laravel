@extends('layouts.admin.app', [
    'class' => '',
    'elementActive' => 'orders'
])
@section('content')
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                @if (session('message'))
                    <div id="notification" class="alert alert-success font-weight-bold">
                        {{ session('message') }}
                    </div>
                @endif
                <div class="card shadow">
                    <div class="card-header border-0 pb-2">
                        <div class="row align-items-center">
                            <form method="GET" class="col-11 d-flex justify-content-end">
                                <div class="w-75 d-flex">
                                    <div class="input-group no-border m-0">
                                        <input type="text" name="search" value="{{ request('search') ?? '' }}"
                                               class="form-control"
                                               placeholder="Search...">
                                        <button class="btn m-0" type="submit">
                                            <div class="input-group-append">
                                                <i class="nc-icon nc-zoom-split"></i>
                                            </div>
                                        </button>
                                    </div>
                                </div>
                            </form>
                            <div class="col-1 text-right">
{{--                                @hasPermission('stock_create')--}}
{{--                                    <a href="{{ route('stocks.create') }}" class="btn btn-sm btn-primary">--}}
{{--                                        <i class="fas fa-plus fa-2x"></i>--}}
{{--                                    </a>--}}
{{--                                @endhasPermission--}}
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table align-items-center table-flush">
                            <thead class="thead-light">
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col" class="text-center">Status</th>
                                <th scope="col" class="text-center">Total</th>
                                <th scope="col" class="text-center">Name</th>
                                <th scope="col" class="text-center w-120">Phone</th>
                                <th scope="col" class="text-center">Address</th>
                                <th scope="col" class="text-center w-120">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($orders as $order)
                                <tr>
                                    <td>{{ $loop->iteration + $orders->firstItem() - 1 }}</td>
                                    <td class="w-120 text-center">
                                        @foreach(config('constants.order') as $key => $status)
                                            @if (config('constants.order.'.$key) == $order->status)
                                                <div class="{{ config('constants.order.'.$key) == 0 || config('constants.order.'.$key) == 3 ? 'bg-danger' : 'bg-success' }} text-white rounded font-weight-bold">
                                                    {{ ucfirst($key) }}
                                                </div>
                                            @endif
                                        @endforeach
                                    </td>
                                    <td class="w-120 font-weight-bold">
                                        {{ number_format($order->total, 0, '', '.') }}<sup>₫</sup>
                                    </td>
                                    <td>{{ $order->customer_name }}</td>
                                    <td>{{ $order->customer_phone }}</td>
                                    <td>{{ $order->customer_address }}</td>
                                    <td>
                                        <div class="container">
                                            <div class="d-flex
                                                {{ $order->status == 0 ? 'justify-content-center' : 'justify-content-end'}}">
                                                <form action="{{ route('orders.destroy', $order->id) }}"
                                                      method="POST" id="form-{{ $order->id }}">
                                                    @csrf
                                                    @method('DELETE')
                                                </form>
                                                @hasPermission('order_delete')
                                                @if($order->status == 0  || $order->status == 3)
                                                    <button class="btn btn-danger p-2 w-icon"
                                                            onclick="deleteOrder({{ $order->id }})">
                                                        <i class="far fa-trash-alt"></i>
                                                    </button>
                                                @endif
                                                @endhasPermission
                                                @hasPermission('order_update')
                                                <a href="{{ route('orders.edit', $order->id) }}"
                                                   class="btn btn-warning ml-2 p-2 w-icon">
                                                    <i class="far fa-edit text-white"></i>
                                                </a>
                                                @endhasPermission
                                                @hasPermission('order_read')
                                                    <a href="{{ route('orders.show', $order->id) }}"
                                                       class="btn btn-info ml-2 p-2 w-icon">
                                                        <i class="fal fa-info-square"></i>
                                                    </a>
                                                @endhasPermission
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <div class="container justify-content-center">
                             {{ $orders->appends(request()->all())->links() }}
                        </div>
                    </div>
                    <div class="card-footer py-3">
                        <nav class="d-flex justify-content-end" aria-label="...">
                            @if( count($orders) <= 0 )
                                <h6 class="container text-center font-weight-bold mb-0">-- NOT FOUND DATA --</h6>
                            @endif
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        function deleteOrder(id) {
            const swalWithBootstrapButtons = Swal.mixin({
                customClass: {
                    confirmButton: 'btn btn-success',
                    cancelButton: 'btn btn-danger mr-3'
                },
                buttonsStyling: false
            })
            swalWithBootstrapButtons.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: 'No, cancel!',
                reverseButtons: true,
            }).then((result) => {
                if (result.isConfirmed) {
                    event.preventDefault();
                    document.getElementById('form-' + id).submit();
                    swalWithBootstrapButtons.fire(
                        'Deleted!',
                        'Your file has been deleted.',
                        'success',
                    )
                } else if (
                    result.dismiss === Swal.DismissReason.cancel
                ) {
                    swalWithBootstrapButtons.fire(
                        'Cancelled',
                        'Your order file is safe :)',
                        'error'
                    )
                }
            })
        }
    </script>
@endpush
