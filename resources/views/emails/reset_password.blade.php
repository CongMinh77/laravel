<!doctype html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{ trans('Expired Account') }}</title>
    <style>
    </style>
</head>

<body>
<div class="container">
    <div class="header">
        <div class="container">
            <div class="header" style="margin-bottom: 20px;">
                <div>
                    <h1>{{ trans('Hello!') }}</h1>
                    <p>{{ trans('You are receiving this email because we received a password reset request for your account.') }}
                    </p>
                    <a href="{{ $url }}">{{ trans('Reset Password') }}</a><br>
                    <p>{{ trans('This password reset link will expire in :count hour.', ['count' => '24']) }}</p>
                    <p>{{ trans('If you did not request a password reset, no further action is required.') }}</p>
                    <p>{{ trans('Regards') }}</p>
                </div>
            </div>
            <div class="footer">
            </div>
        </div>
    </div>
</div>
</body>

</html>
