<div id="category-list">
    <div class="row">
        @if(count($products) > 0)
        @foreach($products as $product)
            <div class="col-md-3 mt-4">
                <div class="element-product">
                    <img src="{{ asset($product->image) }}" class="image-product" alt="product">
                </div>
                <div class="element-name-product mt-2 pl-2 pr-3">
                    <a href="{{ route('product.detail', $product->id) }}" class="text-dark">
                        {{$product->name}}
                    </a>
                </div>
                <div class="d-flex align-items-center mt-1 pl-2">
                    <div class="flex-grow-1 element-price-product">
                        {{ number_format($product->price, 0, '', '.') }}<sup>₫</sup>
                    </div>
                    @auth()
                        <button class="btn {{ auth()->user()->hasLiked($product) ? 'btn-danger': 'btn-light' }} rounded-circle btn-add-cart mr-1"
                                id="btn-like-product" data-product="{{ $product->id }}"
                                data-like="{{ auth()->user()->hasLiked($product) ? 1 : 0 }}"
                                data-action="{{ route('wishlist.like') }}">
                            <i class="fa-regular fa-heart {{ auth()->user()->hasLiked($product) && auth()->check() ? '': 'icon' }}"></i>
                        </button>
                    @endauth
                    {{--                            <div class="loader rounded-circle"></div>--}}
                    {{--                                <button class="btn btn-light rounded-circle btn-add-cart"--}}
                    {{--                                        id="btn-add-cart" data-product-cart="{{ $product->id }}"--}}
                    {{--                                        data-action="">--}}
                    {{--                                    <i class="fa-solid fa-plus icon"></i>--}}
                    {{--                                </button>--}}
                </div>
            </div>
        @endforeach
        @else
            <div class="col-md-12">
                <h5 class="text-center pb-3">
                    @lang('user/home.no_data')
                </h5>
            </div>
        @endif
    </div>
</div>
