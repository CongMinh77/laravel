@extends('layouts.user.app')

@section('title') {{ $product->name }} @endsection

@section('style')
    <link rel="stylesheet" type="text/css" href="{{ asset('paper/css/user/product.css') }}"/>
@endsection
@section('content')
    <div class="content">
        <div class="col-md-12 d-flex align-items-center justify-content-center p-3">
            <img src="{{ asset('paper/img/info-product.png') }}" alt="logo-shopping" class="icon-title">
            <h2 class="m-0 pl-2 font-weight-bold">
                Detail Product
            </h2>
        </div>
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb bg-light">
                <li class="breadcrumb-item font-weight-bold"><a href="{{ route('home') }}">Home</a></li>
                <li class="breadcrumb-item font-weight-bold active" aria-current="page">@lang('user/cart.title')</li>
            </ol>
        </nav>
        <div class="container-fluid d-flex pt-4 pb-4 pr-0 pl-0">
            <div class="col-5 pl-0">
                <div class="card">
                    <div class="card-body">
                        <img class="" src="{{ asset($product->image) }}" alt="{{ $product->image }}">
                    </div>
                </div>
            </div>
            <div class="col-7 pr-0 position-relative">
                <form action="{{ route('cart.add', $product->id) }}" method="POST">
                    @csrf
                    <h3 style="">{{ $product->name }}</h3>
                    <div class="bg-light w-100 rounded p-2">
                        <h4 class="d-flex align-items-center pl-2 h-100 m-0" style="color: #ff4f04;">
                            {{ number_format($product->price, 0, '', '.') }}<sup>₫</sup>
                        </h4>
                    </div>
                    <div class="container-fluid pl-0 pr-0 pt-3 pb-3">
                        <div class="container-fluid bg-light p-0 border-top"></div>
                        <ul class="list-group pt-4">
                            {{ $product->description }}
                        </ul>
                    </div>
                    <div class="row">
                        @if ($stock > 0)
                        <div class="form-group mb-3 col-3">
                            <div class="d-flex align-items-center">
                                <label class="text-dark mr-2 mb-0">
                                    <h6 class="m-0">Quantity: </h6>
                                </label>
                                <input type="number" class="form-control w-70" name="quantity"
                                       value="{{ old('quantity', 1) }}">
                            </div>
                        </div>
                        @endif
                        <div class="col-6 mb-3 d-flex align-items-center font-weight-bold">
                            {{ number_format($stock, 0, '', '.') }} products left
                        </div>
                        <div class="col-12 mb-2">
                            @error('quantity')
                                <span class="alert-danger" role="alert">
                                    {{$message}}
                                </span>
                            @enderror
                        </div>
                    </div>
                    <div class="container-fluid p-0 add-to-cart">
                        @if ($stock > 0)
                            <div class="row">
                                <div class="col-6">
                                    <button class="btn btn-primary rounded w-100" type="submit">
                                        <i class="fa-solid fa-cart-plus"></i> Add to cart
                                    </button>
                                    <input type="hidden" name="product_id" value="{{ $product->id }}">
                                </div>
                            </div>
                        @else
                            <h6 class="text-center bg-danger text-white p-2 rounded mt-3">
                                <i class="fa-solid fa-box-open"></i> Out of stock
                            </h6>
                        @endif
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    {{--    <script src="{{ asset('paper/js/user/cart.js') }}"></script>--}}
@endsection
