@extends('layouts.user.app')

@section('title') @lang('user/wishlist.title') @endsection
@section('style')
    <link rel="stylesheet" type="text/css" href="{{ asset('paper/css/user/wishlist.css') }}"/>
@endsection

@section('content')
    @include('user.wishlist.list')
@endsection

@section('scripts')
    <script>
        $(document).ready(function(){
            $("#search-like").on("keyup", function() {
                let value = $(this).val().toLowerCase();
                $("#table-like tr").filter(function() {
                    $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
                });
            });
        });
    </script>
    <script src="{{ asset('paper/js/user/home.js') }}"></script>
@endsection
