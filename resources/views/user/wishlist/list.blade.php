<div class="content" id="like-product">
    <div class="container-fluid p-0">
        <div class="col-md-12 d-flex align-items-center justify-content-center p-3">
            <img src="{{ asset('paper/img/wishlist.png') }}" alt="logo-shopping" class="icon-title">
            <h2 class="m-0 pl-2 font-weight-bold">
                @lang('user/wishlist.title')
            </h2>
        </div>
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb bg-light">
                <li class="breadcrumb-item font-weight-bold"><a href="{{ route('home') }}">Home</a></li>
                <li class="breadcrumb-item font-weight-bold active" aria-current="page">@lang('user/wishlist.title')</li>
            </ol>
        </nav>
        @if ( count($likes) > 0 )
            <section class="mb-3">
                <div class="d-flex mt-2">
                    <div class="d-flex align-items-center w-160">
                        <a href="{{ route('home') }}" class="text-dark font-weight-bold">
                            <i class="fa-solid fa-angle-left"></i>
                            @lang('label.back_to_the_store')
                        </a>
                    </div>
                    <div class="container-fluid d-flex justify-content-end p-0">
                        <form class="form-inline">
                            <input class="form-control" type="search" placeholder="Search" id="search-like">
                        </form>
                    </div>
                </div>
                <div class="container-fluid p-0 mt-3" id="myDIV">
                    <table class="table table-hover">
                        <thead class="table-light bg-light">
                        <tr>
                            <th scope="col" colspan="2" style="width: 10rem">Product</th>
                            <th scope="col" class="text-center">Price ₫</th>
                            <th scope="col" style="width:3rem"></th>
                        </tr>
                        </thead>
                        <tbody id="table-like">
                        @foreach($likes as $productLike)
                            <tr>
                                <th scope="col" class="w-90">
                                    <img src="{{ asset($productLike->likeable->image) }}" class="rounded"
                                         style="width: 2rem" alt="product">
                                </th>
                                <th scope="col">
                                    <a href="{{ route('product.detail', $productLike->likeable->id) }}"
                                       class="text-dark">
                                        {{ $productLike->likeable->name }}
                                    </a>
                                </th>
                                <th scope="col" class="text-center">
                                    {{ number_format($productLike->likeable->price, 0, '', '.') }}<sup>₫</sup>
                                </th>
                                <th scope="col">
                                    <button class="border-0 btn-dark rounded remove-like" id="btn-like-product"
                                            data-product="{{ $productLike->likeable->id }}"
                                            data-action="{{ route('wishlist.like') }}">
                                        <i class="fas fa-times"></i>
                                    </button>
                                </th>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </section>
        @else
            <div class="d-flex flex-column align-items-center justify-content-center container-fluid p-3 mb-4">
                <h5 class="text-center pb-3">
                    @lang('user/wishlist.no_product')
                </h5>
                <div>
                    <a href="{{ route('home') }}" class="btn btn-dark">
                        @lang('label.back_to_the_store')
                    </a>
                </div>
            </div>
        @endif
    </div>
</div>
