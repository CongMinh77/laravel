@if(count($orderUser) > 0)
    @foreach($orderUser as $order)
        <tr>
            <td>{{ $loop->iteration + $orderUser->firstItem() - 1 }}</td>
            <td class="w-160 text-center">
                <select class="form-control" data-order="{{ $order->id }}"
                        id="select-status" data-action="{{ route('order.update') }}">
                    @foreach(config('constants.order') as $key => $status)
                        @if ($key !== 'done')
                            @if ($key !== 'accept')
                                <option value="{{ config('constants.order.'.$key) }}"
                                    {{ config('constants.order.'.$key) == $order->status ? 'selected' : '' }}>
                                    {{ ucfirst($key) }}
                                </option>
                            @endif
                        @endif
                    @endforeach
                </select>
            </td>
            <td class="w-120 font-weight-bold">
                {{ number_format($order->total, 0, '', '.') }}<sup>₫</sup>
            </td>
            <td>{{ $order->customer_name }}</td>
            <td>{{ $order->customer_phone }}</td>
            <td>{{ $order->customer_address }}</td>
            <td>
                <div class="container">
                    <div class="d-flex
                        {{ $order->status == 0 ? 'justify-content-center' : 'justify-content-end'}}">
                        <form action="{{ route('order.destroy', $order->id) }}"
                              method="POST" id="form-{{ $order->id }}">
                            @csrf
                            @method('DELETE')
                        </form>
                        @if($order->status == 3)
                            <button class="btn btn-danger"
                                    onclick="deleteOrder({{ $order->id }})">
                                <i class="far fa-trash-alt"></i>
                            </button>
                        @endif
{{--                        <a href="{{ route('order.showDetail', $order->id) }}"--}}
{{--                           class="btn btn-info ml-2">--}}
{{--                            <i class="fa-solid fa-info"></i>--}}
{{--                        </a>--}}
                    </div>
                </div>
            </td>
        </tr>
    @endforeach
@endif
