@if(session('cart'))
    @php
        $total = 0;
        $countCart = 0;
    @endphp
    @foreach((array) session('cart') as $id => $details)
        @php
            $total += $details['price'] * $details['quantity'];
            $countCart += $details['quantity'];
        @endphp
    @endforeach
    @foreach(session('cart') as $id => $cart)
        <div class="col-12 d-flex align-items-center rounded p-2 element-product-cart">
            <input type="hidden" name="product_id[]" value="{{ $id }}">
            <img class="img-cart" src="{{ asset($cart['image']) }}" alt="product">
            <div class="pl-2 pr-2 text-break-word flex-grow-1">
                {{ $cart['name'] }}
            </div>
            <div class="mr-2">
                <small>x</small>{{ $cart['quantity'] }}
                <input type="hidden" name="quantity[]" value="{{ $cart['quantity'] }}">
            </div>
            <div>{{ number_format($cart['quantity'] * $cart['price'], 0, '', '.') }}<sup>₫</sup></div>
        </div>
    @endforeach
    <div class="container-fluid p-0 pb-3 border-bottom">
        <div class="d-flex col-12 pl-2 pr-2 font-weight-bold mt-3">
            <div class="flex-grow-1">
                Subtotal:
            </div>
            <div>
                {{ number_format($total, 0, '', '.') }}<sup>₫</sup>
            </div>
        </div>
        <div class="d-flex col-12 pl-2 pr-2 font-weight-bold pt-2">
            <div class="flex-grow-1">
                Ship:
            </div>
            <div>
                {{ number_format(200000, 0, '', '.') }}<sup>₫</sup>
                <input type="hidden" name="ship" value="{{ 200000 }}">
            </div>
        </div>
    </div>
    <div class="container-fluid p-0">
        <div class="d-flex col-12 pl-2 pr-2 font-weight-bold mt-3">
            <h3 class="flex-grow-1">
                Total:
            </h3>
            <div>
                {{ number_format($total + 200000, 0, '', '.') }}<sup>₫</sup>
                <input type="hidden" name="total" value="{{ $total + 200000 }}">
            </div>
        </div>
    </div>
@else
@endif
