@extends('layouts.user.app')

@section('title') Order @endsection
@section('style')
    <link rel="stylesheet" type="text/css" href="{{ asset('paper/css/user/order.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('paper/css/user/cart.css') }}"/>
@endsection

@section('content')
    <div class="content">
        <div class="col-md-12 d-flex align-items-center justify-content-center p-3">
            <img src="{{ asset('paper/img/order.png') }}" alt="logo-shopping" class="icon-title">
            <h2 class="m-0 pl-2 font-weight-bold">
                Purchase
            </h2>
        </div>
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb bg-light">
                <li class="breadcrumb-item font-weight-bold"><a href="{{ route('home') }}">Home</a></li>
                <li class="breadcrumb-item font-weight-bold active" aria-current="page">Purchase</li>
            </ol>
        </nav>
        @auth()
            <div class="table-responsive">
                <table class="table align-items-center table-flush">
                    <thead class="thead-light">
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col" class="text-center">Status</th>
                        <th scope="col" class="text-center">Total</th>
                        <th scope="col" class="text-center">Name</th>
                        <th scope="col" class="text-center w-120">Phone</th>
                        <th scope="col" class="text-center">Address</th>
                        <th scope="col" class="text-center w-120">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                        @include('user.order.table-show')
                    </tbody>
                </table>
                <div class="container justify-content-center">
                    {{ $orderUser->appends(request()->all())->links() }}
                </div>
            </div>
        @endauth
        @guest()
            @include('user.order.table-show-guest')
        @endguest
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('paper/js/user/order.js') }}"></script>
@endsection
