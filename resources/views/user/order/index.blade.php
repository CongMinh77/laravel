@extends('layouts.user.app')

@section('title') Order @endsection
@section('style')
    <link rel="stylesheet" type="text/css" href="{{ asset('paper/css/user/order.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('paper/css/user/cart.css') }}"/>
@endsection

@section('content')
    <div class="content">
        <div class="col-md-12 d-flex align-items-center justify-content-center p-3">
            <img src="{{ asset('paper/img/order.png') }}" alt="logo-shopping" class="icon-title">
            <h2 class="m-0 pl-2 font-weight-bold">
                Order
            </h2>
        </div>
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb bg-light">
                <li class="breadcrumb-item font-weight-bold"><a href="{{ route('home') }}">Home</a></li>
                <li class="breadcrumb-item font-weight-bold">
                    <a href="{{ route('cart.index') }}">@lang('user/cart.title')</a>
                </li>
                <li class="breadcrumb-item font-weight-bold active" aria-current="page">Order</li>
            </ol>
        </nav>
        <div class="container-fluid">
            <form class="row mb-3" method="POST" action="{{ route('order.store') }}">
                @csrf
                <div class="col-7">
                    <div class="form-group">
                        <label>Name <span class="text-danger">*</span></label>
                        <input type="text" class="form-control" name="customer_name"
                               value="{{ old('customer_name') }}"
                               placeholder="Name">
                        @error('customer_name')
                            <span class="alert-danger" role="alert">
                                {{$message}}
                            </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label>Email <span class="text-danger">*</span></label>
                        <input type="text" class="form-control" name="customer_email"
                               value="{{ old('customer_email') }}"
                               placeholder="Email">
                        @error('customer_email')
                            <span class="alert-danger" role="alert">
                                {{$message}}
                            </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label>Phone <span class="text-danger">*</span></label>
                        <input type="text" class="form-control" name="customer_phone"
                               value="{{ old('customer_phone') }}"
                               placeholder="Phone">
                        @error('customer_phone')
                            <span class="alert-danger" role="alert">
                                {{$message}}
                            </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label>Address <span class="text-danger">*</span></label>
                        <input type="text" class="form-control" name="customer_address"
                               value="{{ old('customer_address') }}"
                               placeholder="Address">
                        @error('customer_address')
                            <span class="alert-danger" role="alert">
                                {{$message}}
                            </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label>Note</label>
                        <textarea class="form-control" name="note" rows="3">{{ old('note') }}</textarea>
                    </div>
                </div>
                <div class="col-5 align-items-end">
                    <div class="card mb-3">
                        <div class="card-body ">
                            <div class="row">
                                <div class="col-12 font-weight-bold border-bottom">
                                    <h3 class="mb-0 pt-2 pb-2">
                                        Your order
                                    </h3>
                                </div>
                            </div>
                            <div class="row">
                                @auth()
                                    @include('user.order.product-order')
                                @endauth
                                @guest()
                                    @include('user.order.product-order-guest')
                                @endguest
                            </div>
                        </div>
                    </div>
                    <div class="container-fluid d-flex justify-content-center p-1">
                        <a href="{{ route('cart.index') }}" class="col-6 btn btn-secondary m-1">Back</a>
                        <button type="submit" class="col-6 btn btn-primary m-1">Check out</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
