@if ( count($carts) > 0 )
    <section>
        <div class="d-flex mt-2">
            <div class="d-flex align-items-center w-160">
                <a href="{{ route('home') }}" class="text-dark font-weight-bold">
                    <i class="fa-solid fa-angle-left"></i>
                    @lang('label.back_to_the_store')
                </a>
            </div>
            <div class="container-fluid d-flex justify-content-end p-0">
                <form class="form-inline">
                    <input class="form-control mr-sm-2" type="search" placeholder="Search" id="search-cart">
                </form>
            </div>
        </div>
        <div class="container-fluid p-0 mt-3" id="myDIV">
            <table class="table table-hover">
                <thead class="table-light bg-light">
                <tr>
                    <th scope="col" colspan="2" style="width: 10rem">Product</th>
                    <th scope="col" class="w-120">Quantity</th>
                    <th scope="col" class="text-center">Price ₫</th>
                    <th scope="col" height="" class="text-center">Total</th>
                    <th scope="col" style="width:3rem"></th>
                </tr>
                </thead>
                <tbody id="table-cart">
                @foreach($carts as $cart)
                    @if($cart->quantity > 0)
                        <tr id="row-{{ $cart->id }}">
                        <th scope="col" class="w-90">
                            <img src="{{ asset($cart->products->image) }}" class="rounded"
                                 style="width: 2rem" alt="product">
                        </th>
                        <th scope="col">{{ $cart->products->name }}</th>
                        <th scope="col">
                            <input type="number" class="form-control w-70"
                                   data-action="{{ route('cart.update') }}"
                                   data-product-cart="{{ $cart->id }}"
                                   data-action-delete="{{ route('cart.remove.from.cart') }}"
                                   id="quantity-product" name="quantity"
                                   value="{{ $cart->quantity }}">
                        </th>
                        <th scope="col" class="text-center">
                            {{ number_format($cart->products->price, 0, '', '.') }}<sup>₫</sup>
                        </th>
                        <th scope="col" class="text-center">
                            {{ number_format($cart->quantity * $cart->products->price, 0, '', '.') }}<sup>₫</sup>
                        </th>
                        <th scope="col">
                            <button class="border-0 btn-dark rounded remove-cart" id="btn-remove-from-cart"
                                    data-product-cart="{{ $cart->id }}"
                                    data-action="{{ route('cart.remove.from.cart') }}">
                                <i class="fas fa-times"></i>
                            </button>
                        </th>
                    </tr>
                    @endif
                @endforeach
                </tbody>
            </table>
        </div>
    </section>
    <div class="container-fluid p-0 pb-4 pt-3">
        <div class="d-flex">
            <div class="col-8 pl-0">
                <div class="input-group mb-3">
                    <div class="input-group-prepend" >
                        <span class="input-group-text">
                            <i class="fa-solid fa-ticket pr-2"></i>
                            @lang('label.voucher')
                        </span>
                    </div>
                    <input type="text" class="form-control" placeholder="Your voucher" name="coupon" aria-describedby="basic-addon1">
                </div>
                <h4>
                    @if($totalPayment)
                        @lang('label.total_payment'):
                            <span>
                                {{ number_format($totalPayment, 0, '', '.') }}
                            </span>
                        <sup>₫</sup>
                    @endif
                </h4>
            </div>
            <div class="col-4 d-flex align-items-end pr-0">
                <a href="{{ route('order.index') }}" class="btn btn-dark w-100 font-weight-bold">
                    @lang('label.checkout')
                </a>
            </div>
        </div>
    </div>
@else
    <div class="d-flex flex-column align-items-center justify-content-center container-fluid p-3 mb-4">
        <h5 class="text-center pb-3">
            @lang('user/cart.no_product')
        </h5>
        <div>
            <a href="{{ route('home') }}" class="btn btn-dark">
                @lang('label.back_to_the_store')
            </a>
        </div>
    </div>
@endif
