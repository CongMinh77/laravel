@extends('layouts.user.app')

@section('title') @lang('user/cart.title') @endsection
@section('style')
    <link rel="stylesheet" type="text/css" href="{{ asset('paper/css/user/cart.css') }}"/>
@endsection

@section('content')
    <div class="content">
        <div class="container-fluid p-0">
            <div class="col-md-12 d-flex align-items-center justify-content-center p-3">
                <img src="{{ asset('paper/img/shopping-cart.png') }}" alt="logo-shopping" class="icon-title">
                <h2 class="m-0 pl-2 font-weight-bold">
                    @lang('user/cart.title')
                </h2>
            </div>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb bg-light">
                    <li class="breadcrumb-item font-weight-bold"><a href="{{ route('home') }}">Home</a></li>
                    <li class="breadcrumb-item font-weight-bold active" aria-current="page">@lang('user/cart.title')</li>
                </ol>
            </nav>
            <div id="table-cart">
                @auth()
                    @include('user.cart.list')
                @endauth
                @guest()
                    @include('user.cart.list-guest')
                @endguest
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(document).ready(function(){
            $("#search-cart").on("keyup", function() {
                let value = $(this).val().toLowerCase();
                $("#table-cart tr").filter(function() {
                    $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
                });
            });
        });
    </script>
    <script src="{{ asset('paper/js/user/cart.js') }}"></script>
@endsection
