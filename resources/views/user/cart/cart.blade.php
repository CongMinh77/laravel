<div class="dropdown-hover position-relative dropdownCart">
    <a href="{{ route('cart.index') }}" class="btn btn-light rounded-circle btn-header mr-1 position-relative"
            id="dropdownCart">
        <i class="fa-solid fa-cart-shopping icon fa-lg"></i>
        <span class="badge badge-danger rounded-circle position-absolute" id="amount-cart">{{ $countCart }}</span>
    </a>
    <div class="cart-list">
        <div class="container-fluid">
            <h5 class="text-start pt-2 font-weight-bold">Cart</h5>
            @if(count($carts) > 0)
            @foreach($carts as $cart)
                <a href="{{ route('product.detail', $cart->product_id) }}" class="text-dark">
                    <div class="d-flex align-items-center rounded p-2 element-product-cart">
                        <img class="img-cart" src="{{ asset($cart->products->image) }}" alt="product-cart">
                        <div class="pl-2 pr-2 text-break-word flex-grow-1">
                            {{ $cart->products->name }}
                        </div>
                        <div class="mr-2">
                            {{ $cart->quantity }} <small>x</small>
                        </div>
                        <div>{{ number_format($cart->quantity * $cart->products->price, 0, '', '.') }}<sup>₫</sup></div>
                    </div>
                </a>
            @endforeach
            @else
                <div class="d-flex align-items-center justify-content-center rounded p-2 bg-light font-weight-bold">
                    @lang('user/cart.no_product')
                </div>
            @endif
            <div class="d-flex justify-content-end mt-3 mb-3">
                <a href="{{ route('cart.index') }}" class="btn btn-info font-weight-bold">
                    See cart
                </a>
            </div>
        </div>
    </div>
</div>
