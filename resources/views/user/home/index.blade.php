@extends('layouts.user.app')

@section('title') @lang('user/home.title') @endsection
@section('style')
    <link rel="stylesheet" type="text/css" href="{{ asset('paper/css/user/home.css') }}"/>
@endsection

@section('content')
    <div class="content">
        <section class="wrapper-product-hot mt-3">
            <div>
                <h1 class="text-center font-weight-bold">Make Your Interior More Minimalistic</h1>
            </div>
            <div class="slide-product-hot mt-2 row mt-4">
                @foreach($slideProducts as $product)
                    <div class="col-4 bg-black text-center">
                        <a href="{{ route('product.detail', $product->id) }}">
                            <div class="wrapper-img-slide">
                                <img src="{{ asset($product->image) }}" class="img-slide" alt="img-policy">
                            </div>
                        </a>
                    </div>
                @endforeach
            </div>
        </section>

        {{-- All product --}}
        <section class="wrapper-all-product mt-5">
            <div class="element-description-title text-center font-weight-bold">
                A Collection of Furniture Products for Your Interior
            </div>
            <div class="row justify-content-around mt-3">
                <ul class="nav d-flex justify-content-around w-75">
                    <li class="nav-item active font-weight-bold btn-light element-nav-product">
                        <a class="nav-link text-dark" id="btn-category"
                           data-action="/allProduct/list">
                            All products
                        </a>
                    </li>
                    @foreach($categoryParent as $category)
                        <li class="nav-item font-weight-bold btn-light element-nav-product">
                            <a class="nav-link text-dark" id="btn-category"
                               data-action="/{{ $category->name}}/list">
                                {{ $category->name }}
                            </a>
                        </li>
                    @endforeach
                </ul>
            </div>
            <div class="container-fluid mt-4">
                @include('user.categories.list')
            </div>
            <div class="container-fluid text-center mt-4">
                <a href="/All Products" class="text-dark font-weight-bold">View all product</a>
            </div>
        </section>

        {{-- Policy --}}
        <section class="wrapper-policy mt-4">
            <div class="row">
                <div class="col-7 d-flex align-items-center">
                    <div class="row">
                        <div class="col-6">
                            <i class="text-primary fa-solid fa-truck-fast"></i>
                            <h4 class="font-weight-bold">Fast & Free Shipping</h4>
                            <div>Sign up to premier delivery USA
                                for unlimited free shipping</div>
                        </div>
                        <div class="col-6">
                            <i class="text-success fa-solid fa-arrow-right-arrow-left"></i>
                            <h4 class="font-weight-bold">Hassle Free Return</h4>
                            <div>Free returns means the seller
                                must accept returns for</div>
                        </div>
                        <div class="col-6 mt-3">
                            <i class="text-danger fa-solid fa-headset"></i>
                            <h4 class="font-weight-bold">Dedicated Customer Support</h4>
                            <div>24/7 support iJ a kind of support
                                that is available throughout</div>
                        </div>
                        <div class="col-6 mt-3">
                            <i class="text-info fa-solid fa-money-bill-1-wave"></i>
                            <h4 class="font-weight-bold">Your Best Price Matching</h4>
                            <div>Ask to speak to a manager, show him the competitor's lower price</div>
                        </div>
                    </div>
                </div>
                <div class="col-5 overflow-hidden pl-0">
                    <img src="{{ asset('paper/img/1640135997bg5.jpg') }}" class="img-poster" alt="img-policy">
                </div>
            </div>
        </section>

        {{-- Join Us --}}
        <section class="wrapper-sign-up-information mt-5">
            <div class="container-fluid">
                <div class="row justify-content-center d-flex flex-column">
                    <div class="text-center element-join-us-title">
                        We worked with almost organization in Viet Nam
                    </div>
                    <div class="container-fluid d-flex justify-content-center mt-2 mb-2">
                        <div class="d-flex wrapper-join-us-input form-join-us">
                            <input type="email" name="subscriber" placeholder="Email Address"
                                   class="form-control border-0" id="email-register">
                            <button class="btn btn-danger font-weight-bold btn-join-us m-1"
                                    id="btn-register-email" data-action="{{ route('email-notification.store') }}">
                                @lang('user/home.join-us')
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        {{-- Other social --}}
        <section class="wrapper-other-social mt-4">
            <div class="container-fluid">
                <div class="row flex-column">
                    <div class="element-other-social-title">
                        Visit our instagram gallery
                    </div>
                    <div class="container-fluid mt-3 mb-5">
                        <div class="row">
                            @foreach($visitProducts as $product)
                                <div class="col-2 pl-0">
                                    <img src="{{ asset($product->image) }}" class="image-product rounded" alt="product">
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('paper/js/user/home.js') }}"></script>
@endsection
