<?php

namespace Database\Seeders;

use App\Models\Permission;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;

class PermissionRoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $allPermission = Permission::all();
        $dataInsert = [];
        foreach ($allPermission as $key=> $value) {
            $dataInsert[] = [
                'permission_id' => $value->id,
                'role_id' => 1
            ];
        }
        DB::table('permission_role')->insert($dataInsert);
    }
}
