<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('permissions')->insert([
            ['name' => 'user_create', 'display_name' => 'User Create', 'group_name' => 'user'],
            ['name' => 'user_read', 'display_name' => 'User Read', 'group_name' => 'user'],
            ['name' => 'user_update', 'display_name' => 'User Update', 'group_name' => 'user'],
            ['name' => 'user_delete', 'display_name' => 'User Delete', 'group_name' => 'user'],

            ['name' => 'role_create', 'display_name' => 'Role Create', 'group_name' => 'role'],
            ['name' => 'role_read', 'display_name' => 'Role Read', 'group_name' => 'role'],
            ['name' => 'role_update', 'display_name' => 'Role Update', 'group_name' => 'role'],
            ['name' => 'role_delete', 'display_name' => 'Role Delete', 'group_name' => 'role'],

            ['name' => 'product_create', 'display_name' => 'Product Create', 'group_name' => 'product'],
            ['name' => 'product_read', 'display_name' => 'Product Read', 'group_name' => 'product'],
            ['name' => 'product_update', 'display_name' => 'Product Update', 'group_name' => 'product'],
            ['name' => 'product_delete', 'display_name' => 'Product Delete', 'group_name' => 'product'],

            ['name' => 'category_create', 'display_name' => 'Category Create', 'group_name' => 'category'],
            ['name' => 'category_read', 'display_name' => 'Category Read', 'group_name' => 'category'],
            ['name' => 'category_update', 'display_name' => 'Category Update', 'group_name' => 'category'],
            ['name' => 'category_delete', 'display_name' => 'Category Delete', 'group_name' => 'category'],

            ['name' => 'stock_create', 'display_name' => 'Stock Create', 'group_name' => 'stock'],
            ['name' => 'stock_read', 'display_name' => 'Stock Read', 'group_name' => 'stock'],
            ['name' => 'stock_update', 'display_name' => 'Stock Update', 'group_name' => 'stock'],
            ['name' => 'stock_delete', 'display_name' => 'Stock Delete', 'group_name' => 'stock'],

            ['name' => 'order_create', 'display_name' => 'Order Create', 'group_name' => 'order'],
            ['name' => 'order_read', 'display_name' => 'Order Read', 'group_name' => 'order'],
            ['name' => 'order_update', 'display_name' => 'Order Update', 'group_name' => 'order'],
            ['name' => 'order_delete', 'display_name' => 'Order Delete', 'group_name' => 'order'],

            ['name' => 'ship_create', 'display_name' => 'Ship Create', 'group_name' => 'ship'],
            ['name' => 'ship_read', 'display_name' => 'Ship Read', 'group_name' => 'ship'],
            ['name' => 'ship_update', 'display_name' => 'Ship Update', 'group_name' => 'ship'],
            ['name' => 'ship_delete', 'display_name' => 'Ship Delete', 'group_name' => 'ship'],

            ['name' => 'coupon_create', 'display_name' => 'Coupon Create', 'group_name' => 'coupon'],
            ['name' => 'coupon_read', 'display_name' => 'Coupon Read', 'group_name' => 'coupon'],
            ['name' => 'coupon_update', 'display_name' => 'Coupon Update', 'group_name' => 'coupon'],
            ['name' => 'coupon_delete', 'display_name' => 'Coupon Delete', 'group_name' => 'coupon'],
        ]);
    }
}
