<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

Furniture v1.1.0

### Common set up
```
cd laravel

cp .env.example .env

composer install

npm install && npm run dev

php artisan key:generate
```

##### Set up database

```
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=your_port
DB_DATABASE=your_database_name
DB_USERNAME=your_username
DB_PASSWORD=your_password
```

### Way 1: Set up environment

```
php artisan serve
```

### Way 2: Run project by Docker - Set up laradock

```
git clone https://github.com/Laradock/laradock.git

cp .env.example .env

docker-compose up -d nginx mysql phpmyadmin
```

##### Set up .env laradock

```
APP_CODE_PATH_HOST=../your_folder_project
```
