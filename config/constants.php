<?php

return [
    'admin' => [
        'pagination' => 5,
        'admin_role' => 4,
        'staff_role' => 2,
        'super_admin' => 1,
    ],

    'user' => [
        'product_user' => [
            'like' => 1,
            'add_to_cart' => 2
        ],
        'pagination' => 8,
    ],

    'stock' => [
        'status' => [
            'import' => [
                'key' => 1,
                'name' => 'Import'
            ],
            'export' => [
                'key' => 2,
                'name' => 'Export'
            ]
        ],
    ],

    'order' => [
        'pending' => 0,
        'accept' => 1,
        'done' => 2,
        'cancel' => 3,
    ],

    'coupon' => [
        'type' => [
            'money',
            'percent'
        ],
    ],

    'category' => [
        'Living room' => 'livingroom.png',
        'Bathroom' => 'bathroom.png',
        'Bedroom' => 'bedroom.png',
        'Dining' => 'dining.png',
        'All Products' => 'allProduct.png'
    ],
];
