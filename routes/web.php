<?php

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\HomeController as AdminHomeController;
use App\Http\Controllers\Admin\UserController as AdminUserController;
use App\Http\Controllers\Admin\RoleController as AdminRoleController;
use App\Http\Controllers\Admin\CategoryController as AdminCategoryController;
use App\Http\Controllers\Admin\ProductController as AdminProductController;
use App\Http\Controllers\Admin\StockController as AdminStockController;
use App\Http\Controllers\Admin\CouponController as AdminCouponController;
use App\Http\Controllers\Admin\EmailNotificationController as AdminEmailNotificationController;
use App\Http\Controllers\Admin\OrderController as AdminOrderController;
use App\Http\Controllers\Admin\ShippingController as AdminShippingController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\User\HomeController;
use App\Http\Controllers\User\CartController;
use App\Http\Controllers\User\WishlistController;
use App\Http\Controllers\User\EmailNotificationController;
use App\Http\Controllers\User\ProductController;
use App\Http\Controllers\User\OrderController;
use App\Http\Controllers\User\CategoryController;

if (App::environment('production')) {
    URL::forceScheme('https');
}


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/**
 * Route Admin
 */

Route::middleware(['auth'])->prefix('admin')->group(function () {
    Route::get('/', [AdminHomeController::class, 'index'])->name('admin.home');

    Route::name('roles.')->prefix('roles')->group(function () {
        Route::get('/', [AdminRoleController::class, 'index'])
            ->name('index')
            ->middleware('permission:role_read');

        Route::get('/create', [AdminRoleController::class, 'create'])
            ->name('create')
            ->middleware('permission:role_create');

        Route::post('/', [AdminRoleController::class, 'store'])
            ->name('store')
            ->middleware('permission:role_create');

        Route::put('/{id}', [AdminRoleController::class, 'update'])
            ->name('update')
            ->middleware('permission:role_update');

        Route::get('/{id}/edit', [AdminRoleController::class, 'edit'])
            ->name('edit')
            ->middleware('permission:role_update');

        Route::delete('/{id}', [AdminRoleController::class, 'destroy'])
            ->name('destroy')
            ->middleware('permission:role_delete');

        Route::get('/{id}', [AdminRoleController::class, 'show'])
            ->name('show')
            ->middleware('permission:role_read');
    });

    Route::name('users.')->prefix('users')->group(function () {
        Route::get('/', [AdminUserController::class, 'index'])
            ->name('index')
            ->middleware('permission:user_read');

        Route::get('/create', [AdminUserController::class, 'create'])
            ->name('create')
            ->middleware('permission:user_create');

        Route::get('/{id}', [AdminUserController::class, 'show'])
            ->name('show')
            ->middleware('permission:user_read');

        Route::post('/', [AdminUserController::class, 'store'])
            ->name('store')
            ->middleware('permission:user_create');

        Route::put('/{id}', [AdminUserController::class, 'update'])
            ->name('update')
            ->middleware('permission:user_update');

        Route::get('/{id}/edit', [AdminUserController::class, 'edit'])
            ->name('edit')
            ->middleware('permission:user_update');

        Route::delete('/{id}', [AdminUserController::class, 'destroy'])
            ->name('destroy')
            ->middleware('permission:user_delete', 'check.delete.user');
    });

    Route::name('categories.')->prefix('categories')->group(function () {
        Route::get('/', [AdminCategoryController::class, 'index'])
            ->name('index')
            ->middleware('permission:category_read');

        Route::get('/list', [AdminCategoryController::class, 'list'])
            ->name('list')
            ->middleware('permission:category_read');

        Route::post('/', [AdminCategoryController::class, 'store'])
            ->name('store')
            ->middleware('permission:category_create');

        Route::get('/create', [AdminCategoryController::class, 'create'])
            ->name('create')
            ->middleware('permission:category_create');

        Route::get('/{id}', [AdminCategoryController::class, 'show'])
            ->name('show')
            ->middleware('permission:category_read');

        Route::delete('/{id}', [AdminCategoryController::class, 'destroy'])
            ->name('destroy')
            ->middleware('permission:category_delete');

        Route::put('/{id}', [AdminCategoryController::class, 'update'])
            ->name('update')
            ->middleware('permission:category_update');

        Route::get('/{id}/edit', [AdminCategoryController::class, 'edit'])
            ->name('edit')
            ->middleware('permission:category_update');
    });

    Route::name('products.')->prefix('products')->group(function () {
        Route::get('/', [AdminProductController::class, 'index'])
            ->name('index')
            ->middleware('permission:product_read');

        Route::get('/create', [AdminProductController::class, 'create'])
            ->name('create')
            ->middleware('permission:product_create');

        Route::get('/{id}', [AdminProductController::class, 'show'])
            ->name('show')
            ->middleware('permission:product_read');

        Route::post('/', [AdminProductController::class, 'store'])
            ->name('store')
            ->middleware('permission:product_create');

        Route::put('/{id}', [AdminProductController::class, 'update'])
            ->name('update')
            ->middleware('permission:product_update');

        Route::get('/{id}/edit', [AdminProductController::class, 'edit'])
            ->name('edit')
            ->middleware('permission:product_update');

        Route::delete('/{id}', [AdminProductController::class, 'destroy'])
            ->name('destroy')
            ->middleware('permission:product_delete');
    });

    Route::name('stocks.')->prefix('stocks')->group(function (){
        Route::get('/', [AdminStockController::class, 'index'])
            ->name('index')
            ->middleware('permission:stock_read');
        Route::get('/create', [AdminStockController::class, 'create'])
            ->name('create')
            ->middleware('permission:stock_create');
        Route::post('/', [AdminStockController::class, 'store'])
            ->name('store')
            ->middleware('permission:stock_create');
        Route::put('/update/{id}', [AdminStockController::class, 'update'])
            ->name('update')
            ->middleware('permission:stock_update');
        Route::get('/{id}/edit', [AdminStockController::class, 'edit'])
            ->name('edit')
            ->middleware('permission:stock_update');
        Route::get('/{id}', [AdminStockController::class, 'show'])
            ->name('show')
            ->middleware('permission:stock_read');
        Route::delete('/{id}', [AdminStockController::class, 'destroy'])
            ->name('destroy')
            ->middleware('permission:stock_delete');
    });

    Route::name('coupons.')->prefix('coupons')->group(function () {
        Route::get('/', [AdminCouponController::class, 'index'])
            ->name('index')
            ->middleware('permission:coupon_read');
        Route::get('/create', [AdminCouponController::class, 'create'])
            ->name('create')
            ->middleware('permission:coupon_create');
        Route::post('/', [AdminCouponController::class, 'store'])
            ->name('store')
            ->middleware('permission:coupon_create');
        Route::put('/update/{id}', [AdminCouponController::class, 'update'])
            ->name('update')
            ->middleware('permission:coupon_update');
        Route::get('/{id}/edit', [AdminCouponController::class, 'edit'])
            ->name('edit')
            ->middleware('permission:coupon_update');
        Route::get('/{id}', [AdminCouponController::class, 'show'])
            ->name('show')
            ->middleware('permission:coupon_read');
        Route::delete('/{id}', [AdminCouponController::class, 'destroy'])
            ->name('destroy')
            ->middleware('permission:coupon_delete');
    });

    Route::name('emails.')->prefix('emails')->group(function () {
        Route::get('/', [AdminEmailNotificationController::class, 'index'])->name('index');
        Route::get('/create', [AdminEmailNotificationController::class, 'create'])->name('create');
        Route::put('/update/{id}', [AdminEmailNotificationController::class, 'update'])->name('update');
        Route::get('/{id}/edit', [AdminEmailNotificationController::class, 'edit'])->name('edit');
        Route::delete('/{id}', [AdminEmailNotificationController::class, 'destroy'])->name('destroy');
    });

    Route::name('orders.')->prefix('orders')->group(function () {
        Route::get('/', [AdminOrderController::class, 'index'])
            ->name('index')
            ->middleware('permission:order_read');
        Route::get('/create', [AdminOrderController::class, 'create'])
            ->name('create')
            ->middleware('permission:order_create');
        Route::post('/', [AdminOrderController::class, 'store'])
            ->name('store')
            ->middleware('permission:order_create');
        Route::put('/update/{id}', [AdminOrderController::class, 'update'])
            ->name('update')
            ->middleware('permission:order_update');
        Route::get('/{id}/edit', [AdminOrderController::class, 'edit'])
            ->name('edit')
            ->middleware('permission:order_update');
        Route::get('/{id}', [AdminOrderController::class, 'show'])
            ->name('show')
            ->middleware('permission:order_read');
        Route::delete('/{id}', [AdminOrderController::class, 'destroy'])
            ->name('destroy')
            ->middleware('permission:order_delete');
    });

    Route::name('ships.')->prefix('ships')->group(function () {
        Route::get('/', [AdminShippingController::class, 'index'])
            ->name('index')
            ->middleware('permission:ship_read');
        Route::get('/create', [AdminShippingController::class, 'create'])
            ->name('create')
            ->middleware('permission:ship_create');
        Route::post('/', [AdminShippingController::class, 'store'])
            ->name('store')
            ->middleware('permission:ship_create');
        Route::put('/update/{id}', [AdminShippingController::class, 'update'])
            ->name('update')
            ->middleware('permission:ship_update');
        Route::get('/{id}/edit', [AdminShippingController::class, 'edit'])
            ->name('edit')
            ->middleware('permission:ship_update');
        Route::get('/{id}', [AdminShippingController::class, 'show'])
            ->name('show')
            ->middleware('permission:ship_read');
        Route::delete('/{id}', [AdminShippingController::class, 'destroy'])
            ->name('destroy')
            ->middleware('permission:ship_delete');
    });

    Route::get('{page}', [
        'as' => 'page.index',
        'uses' => 'App\Http\Controllers\Admin\PageController@index'
    ]);

    Route::name('profile.')->prefix('profile')->group(function () {
        Route::get('/edit', [ProfileController::class, 'edit'])
            ->name('edit');

        Route::put('/', [ProfileController::class, 'update'])
            ->name('update');

        Route::put('/password', [ProfileController::class, 'password'])
            ->name('password');
    });
});

/**
 * Route User
 */
Route::get('/', [HomeController::class, 'index'])->name('home');

Route::middleware(['auth'])->group(function (){
    Route::name('wishlist.')->prefix('wishlist')->group(function () {
        Route::get('/', [WishlistController::class, 'index'])->name('index');
        Route::get('/list', [WishlistController::class, 'listLike'])->name('list');
        Route::get('/listProduct', [WishlistController::class, 'list'])->name('listProduct');
        Route::post('/like', [WishlistController::class, 'like'])->name('like');
    });
});

Route::resource('email-notification', EmailNotificationController::class)->only('store');

Route::name('cart.')->prefix('cart')->group(function () {
    Route::get('/', [CartController::class, 'index'])->name('index');
    Route::get('/list', [CartController::class, 'listCart'])->name('list');
    Route::post('/add-cart/{id}', [CartController::class, 'addToCart'])->name('add');
    Route::patch('update-cart', [CartController::class, 'update'])->name('update');
    Route::delete('remove-from-cart', [CartController::class, 'remove'])->name('remove.from.cart');
});

Route::name('product.')->prefix('product')->group(function () {
    Route::get('/{id}', [ProductController::class, 'detail'])->name('detail');
});

Route::name('order.')->prefix('order')->group(function () {
    Route::get('/', [OrderController::class, 'index'])->name('index')->middleware('check.cart');
    Route::post('/', [OrderController::class, 'store'])->name('store')->middleware('check.cart');
    Route::get('/purchase', [OrderController::class, 'show'])->name('show');
    Route::get('/{id}', [OrderController::class, 'showDetail'])->name('showDetail');
    Route::delete('/{id}', [OrderController::class, 'destroy'])->name('destroy');
    Route::put('/', [OrderController::class, 'updateStatus'])->name('update');
});

Auth::routes();

Route::prefix('/{page}')->name('page.')->group(function () {
    Route::get('/', [CategoryController::class, 'index'])->name('index');
    Route::get('/list', [CategoryController::class, 'list'])->name('list');
});
